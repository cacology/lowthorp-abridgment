---
title: "Handout"
author: "J. P. Ascher, jpa4q@virginia.edu"
date: 10 March 2023
citation-style: chicago-fullnote-no-bibliography.csl
bibliography: /Users/james/org/research.bib
Local IspellParsing: latex-mode
...
\input{standard-style.tex.include}

## Lowthorp’s scheme

\startcolumns[n=2]

{\bf I. Mathematics:}
    1. Geometry, algebra, arithmetic;
    2. Trigonometry, surveying;
    3. Optics;
    4. Astronomy;
    5. Mechanics, acoustics;
    6. Hydrostatics, hydraulics;
    7. Geography, navigation;
    8. Architecture, ship-building;
    9. Perspective, sculpture, painting;
    10. Music.
    
{\bf II. Physiology:}
    1. Meteorology, pneumatics;
    2. Hydrology;
    3. Mineralogy;
    4. Magnetics;
    5. Zoology.
    
{\bf III. Anatomy, physic, chemistry:}
    1. Anatomy, physic:
       {\it i. Structure, external parts, and common teguments};
       {\it ii. Head};
       {\it iii. Thorax};
       {\it iv. Abdomen};
       {\it v. Humors and general affections};
       {\it vi. Bones, joints, muscles};
       {\it vii. Monsters};
       {\it vii. Period of human life};
    2. Pharmacy, chemistry.

{\bf IV. Philology, miscellanies:}
    1. Philology, grammar;
    2. Chronology, history, antiquities;
    3. Voyages and travels;
    4. Miscellaneous.

\noindentation{}Jones’s continuation follows a similar outline, as do all the succeeding volumes:

{\bf 1. Mathematical papers:}
    1. Geometry, arithmetic, algebra, logarithmotechny;
    2. Optics;
    3. Astronomy;
    4. Mechanics, acoustics;
    5. Hydrostatics, hydraulics;
    6. Geography, navigation;
    7. Music.

{\bf 2. Physiological papers:}
    1. Physiology, meteorology, pneumatics;
    2. Hydrology;
    3. Mineralogy;
    4. Magnetics;
    5. Agriculture, botany.

{\bf 3. Zoology, &c. anatomy, physic, chymistry:}
    1. Zoology, and the anatomy of animals;
    2. Anatomy, diseases:
       {\it 1. General and skin};
       {\it 2. Head};
       {\it 3. Neck, thorax, heart};
       {\it 4. Abdomen};
       {\it 5. Humours and general affections of the body};
       {\it 6. Bones, joints, and muscles};
       {\it 7. Pharmacy, chymistry}.

{\bf 4. Philology, miscellanies:}
     1. Manuscripts, printing;
     2. Chronology, history, antiquities;
     3. Travels, voyages;
     4. Miscellaneous.

\stopcolumns

## Proposing the abridgment

Having established himself as a member of the Royal Society, Lowthorp eventually proposes two ambitious projects simultaneously.

\TranscriptionStart

[**Page 20:**]

[…]

[*centered:*] 5 May 1703.

[…]

[**Page 21:**]

[…]

 M^r^ Lowthorp’s written Proposalls for Instruments design’d for advancing Astro-\
nomy, & perfecting Navigation , & for Experiments of good Use in Mechanics, Optics, [*‘for’ inserted between ‘&’ and ‘Experiments’, i.e. ‘& ‸^for^ Experiments’*]\
& Physics.  He was desired by the Society, in the first place to prosecute those Ex-\
periments which relate to Telescopes, and to bring to the next Meeting, a List of\
such Experiments as are neceſsary thereunto.

 M^r^ Lowthorp also presented a Proposal for Printing an Abridgement of\
the Philosophical Transactions, together with a Printed Specimen of such an\
Abridgement.  His Specimen & this Design were approv’d by the Society, & he\
was desired to proceed therein.

[…][^25]

[^25]: UkLoRS JBO/11/30 (pp. 20–21)

\TranscriptionStop

# The Manuscript Proposal

The original manuscript proposal is kept within the Classified Papers
at the Royal Society, a series appropriately enough, reorganized in
1720s along the same classification scheme developed by Lowthorp.[^31]
The volume contains a number of book-related manuscripts and this
proposal is the 59th item.  It is written on a bifolium, 2^o^: χ^2^; 4
pages, [*unnumbered, pp. 1-4*], on paper watermarked with the London
coat of arms (Churchill 239–244) and countermarked “C T” (stock,
% 13 \| 17 \| 24 \| 25 \| 2 [ 23 \| 23 ] 1 \| 25 \| 24 \| 24 \| 24 \| 25
\| 21 [ 4 \| 6 ] 15 [ 3 \| 6 ] 19 \| 24 \| 23 \| 16 % );[^33] the leaf
measures 301 × 194 mm and has 33 lines (on p. 2), in cursive, with
heavier strokes at the ends of words and on descenders. Page 2
continues across the gutter onto p.3 at the ends of some lines,
suggesting this was written open to both pages.  It is dated ‘May 5:
1702.’  on p. 4 and ‘Apr. 12. 1703’ on p. 1.  It seems likely that 21
April 1703 reflects Lowthorp’s dating and 5 May 1702 is later, perhaps
written by the Secretary at the meeting where it was read.  The year,
1702, is almost certainly wrong.  The inner margin of p. 1 is
discolored with its, presumably, previous pasting in another guard
book.  There are inked fingerprints on p.1, possibly oil-based
printers’ ink by the spread and show-through.  Page 4 is faintly
discolored into four folded panels measuring 81 × 194 mm.  In the
lower left of p.1 a penciled cross, probably Dr. Stacks’s, indicates
this item has been counted.  Modern penciled numbers ‘175’ on p. 1 and
‘176’ on p. 3 refer to the foliation of the present volume.

[^33]: This notation uses the pipe to indicate a chainline, square brackets to indicate a watermark, and a percent sign to indicate the deckle edge.  See @vandermeulen84:paper


[^31]: CMO/3/84

\TranscriptionStart

[*Next seven lines, centered:*] Apr. 12. 1703

Propoſalls for Printing\
The Philosophicall\
Transactions and Collections [*pen skip after ‘Transactions’*]\
To the End of the Year j700.\
Abridg’d and Diſpoſ\kern1pt{}’d under Generall Heads\
in **III.** Volumes.

By J. Lowthorp. **M.A.**&**F.R.S.** [*pen skip before ‘M.A.’*]

The Philoſophicall Transactions are well known to be an\
Excellent Regiſter of many Valuable Experiments, and a Cu⹀\
⹀rious Collection of such Diſcourſes , as being too ſhort to be
Printed\
alone, were in great Danger of being loſt: And have been\
publiſ\kern1pt{}hed almoſt every Month (some few Years of Interruption\
excepted) since the begining of the Year i665, by the particular\
Encouragement of the Royall Society .  But the generall Appro\
⹀bation they have met with, both at home and abroad , has long\
since made them so scarce , that a compleat Set is not to be pro⹀\
⹀cur’d without much trouble ,and not to be purchaſ\kern1pt{}’d under 15.£.\
And yet the vaſt Bulk of 22. thin Volumes in 4to. to which\
they are swell’d, and that want of Connection which could —\
not be avoided in this manner of Communicating such a Mul⹀\
⹀titude of independent Papers , make it altogether unadviſeable\
to Reprint them in their preſent Form .  The Author therefore\
of this Abridgement , conceives it may be some Service to the\
Publique , to Contract them into a leſs Compaſs; and to Range\
and Diſpoſe them in a better order, under Generall Heads:\
In the Execution whereof, He (hath for the moſt part ) confin’d\
himſelf to theſe Rules.

 i. He has divided the whole Work into 3. Volumes; The First\
contains all the Mathematicall Papers; The Second the —\
Phyſiologicall; and the First part of the 3d thoſe that\
are Medicall, the later part of it being reſerved for the [**Page 2:**]\
the Ph\rlap{\bf i}{y}lologicall and such Miſcellaneous Papers as cannot [*‘Ph\rlap{\bf i}{y}lologicall’ corrected from ‘Phylologicall’ by overwriting*]\
properly be reduc’d to any of the former Claſſes.  Each of theſe\
Volumes are subdivided into Chapters , and Generall Heads ,\
under which each Paper is inſerted in the beſt Order their —\
great Variety will admit of.

 2. He does not think it neceſſary in this Abridgement to\
retain the Accounts of Books which are added to the End of —\
each of the Tranſactions , since the Books themſelves are either\
long ago common in the Hands of such Readers as have Occaſion [*tail on ‘n’ extends to next page*]\
to uſe them , or so near loſt that few will be at the trouble to\
search for them . Yet that the Inquiſitive may have some Notice\
of them , He ha\rlap{\bfa s}{\kern2pt{}\tfx{}d} added to the End of each Chapter a
Catalogue [*‘ha\rlap{\bfa s}{\kern2pt{}\tfx{}d}’ corrected from ‘had’ by overwriting*]\
of such of them as Treat profeſtly and principally of that Subject [*blot over ‘at’ in ‘that’*]\
of that Chapter.

 3. The Additions , Emendations, and Refutations , of Books He does\
not think neceſſary to be retained here, becauſse such remarks can\
be only serviceable to thoſe who have the Books .  And therefore He\
contents himſelf with Directing his Readers to the Tranſac\rlap{\bfa t}{c}ions [*pen skip over ‘e’ in ‘Directing’; ‘Tranſac\rlap{\bfa t}{c}ions’ corrected from ‘Tranſaccions’ by overwriting*]\
themſelves for such Particulars .

 4. There are many Papers which were very Curious at the times [*tail of ‘s’ extends to next page*]\
of their Publication\rlap{\bfa (}{,}as the Calculations of Eclipſes , Lunar [*opening parenthesis overwrites comma*]\
Appulſes , Satellite Eclipſes , Tide Tables , and some others of that\
kind\rlap{\bfa )}{,} which are now uſeleſs .  Yet to do Juſtice to their Authors [*closing parenthesis overwrites comma; tail of ‘s’ extends to next page*]\
the Titles are inſerted in their proper places.

 5. There are another sort of Papers here Omitted which are [*dot over first ‘h’ in ‘which’; tail of ‘e’ extends to next page*]\
alſo very Curious viz. Inquiries and Directions , upon severall\
Subjects , and for places seldom viſited , drawn up and diſperſed\
by Order of the Royall Society · But there being very few who\
have any Oportunity of satiſfying either themſelves or others\
in such Particulars , The Author thinks that the Anſwers\
already given to many of the Enquiries , and the other Diſcourſes\
upon the same or the like Subjects here retained , will sufficiently\
supply this want . And therefore he chuſes to recommend it to [**Page
3:**]\
the Gentlemen of the Royall Society themſelves to Reprint thoſe\
Ingenious Papers, and to put them into such Hands as may probably\
have an Oportunity to make them suitable Returns , rather then to
swell\
theſe Volumes which are deſigned for more \rlap{g}{\bf G}enerall uſe. In the [*‘\rlap{g}{\bf G}enerall’ corrected from ‘generall’ by overwriting*]\
mean time He has not fail’d to inſert the Titles of theſe ,
as well [*pen skip between ‘has’ and ‘not’*]\
as of all other Papers Omitted, at the end of each Chapter to which\
they properly belong.

 6. He has been very Carefull , in Abridging the remaining Papers,\
to preſerve the Sence of their Authors entire, and to relate the\
Hiſtories of Facts with all the Materiall Circumſtances and uſefull\
Reaſonings thereon . But many of thoſe Papers being writ in the\
form of Letters and often interſperſed with perſonall Addreſſes;
[*second d in ‘Addreſſes’ inserted, Ad‸^d^reſſes*]\
And others of them being frequently interrupted with long Excurſi⹀\
⹀ons and Citations of Books, which for the moſt part rather Amuſe\
the Reader than either Inſtruct him or Illuſtrate the Subject; This\
Author humbly hopes that none will think themſelves Injured\
whilſt He takes a modeſt Liberty of Pruning such Luxuriances .\

 7. To cloſe in some Meaſure one of the Chaſmes in the\
Tranſactions , viz. between the Years i678 and i683. He hath inſe\rlap{—}{rt} [*dash overwrites ‘rt’ in inſe\rlap{—}{rt}’*]\
⹀ed , in their proper places , Dr Hook’s Philoſophicall Collections,\
being a Work of the same kind and Publiſh’d by him during that\
Intervall of time.

 8. To make the whole Work more compleat He will add such Ind⹀\
⹀dexes both of Authors and Subjects as Shall be thought of any [*‘⹀dexes’ corrected by canceling an apostrophe from ‘⹀dexe’s’*]\
uſe or Advantage to the Readers.


In short the Author of this Abridgement profeſſes that\
He hath every where deſign’d (according to the beſt of his Under⹀\
⹀ſtanding) to Omit nothing that would be of any Service to the
greateſt\
part of the Readers , and to Burthen them with nothing that could\
be spar’d without manifeſt Prejudice to the Sence of the severall\
Ingenious Authors : And that he hath carefully endeavoured that\
none of their Papers ſ\kern1pt{}hould suffer by paſſing through his Hands.

\TranscriptionStop

## Bibliographical argument

\BiblHangingIndentsStart

**Vol. 1:** 4^o^: [*\**]^4^ 2\*^2^ a–n^2^ B–3M^4^ 4A–4U^4^ 4X^2^ [\$2 (−2\* a–n 4X2) signed; d1 signed D1]; 342 leaves, pp. [*32*] 1–454 543–708[=620] (misnumbering 200 as 100); 1–7 engraved plates (pl. 6 as ‘7’), pl. 1 facing pp. 54 (H3^v^), 2: 190 (2B3^v^), 3: 388 (3D2^v^), 4: 556 (4B2^v^), 5: 626 (4L1^v^), ‘7’ [=6]: 660 (4P2^v^), 7: 698 (4U1^v^)\

\BiblHangingIndentsStop

To summarize a great deal of research, it is probable based on signing
and pagination that 4A-4N were printed first; it is probable based on
typographical style that 4A-4N were printed first; it is probable
based on type damage, catchwords, and text that 4A-4N were printed
first; and it is probable that 4A-4N were printed first based on the
paper evidence.  Based on these independent probabilities, I’d say
it’s most likely that 4A-4N were printed first.  Perhaps B-C were
begun along the way, but 4A must have been the very first sheet of the
volume printed.
