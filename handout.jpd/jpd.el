;;; jpd.el --- Functions for JP document format in Emacs  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  J. P. Ascher

;; Author: J. P. Ascher <jpsa@protonmail.com>
;; Keywords: files

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(defcustom jpd-doc-directory "/Users/james/src/jp-doc"
  "The default directory for the JP Document format to use with `jpd-new-and-edit'" :type '(string) :group 'editing)

(defun jpd-new-and-edit (new-file)
    "Creates a new jpd and edits the enclosed doc.md."
  (interactive "FNew JP document:")
  (let* ((target-jpd-dir (concat new-file ".jpd"))
        (target-md-file (concat target-jpd-dir "/doc.md")))
  (copy-directory jpd-doc-directory target-jpd-dir)
  (delete-directory (concat target-jpd-dir "/.git") t)  ; this is the .git for the template
  (find-file-existing target-md-file)
  ))

(global-set-key (kbd "C-x j") 'jpd-new-and-edit)

(provide 'jpd)
;;; jpd.el ends here
