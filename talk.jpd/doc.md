---
title: "Talk for “Roundtable: Formats, Numbers, Parts, Periodicals, and Serials”"
author: "J. P. Ascher, jpa4q@virginia.edu"
date: 10 March 2023
citation-style: chicago-fullnote-no-bibliography.csl
bibliography: /Users/james/org/research.bib
Local IspellParsing: latex-mode
...
\input{standard-style.tex.include}

# Paper title

\noindentation{}John Lowthorp’s 1705 Abridgment of the *Philosophical Transactions*

**Sentences in bold are read.** Those not are additional background.


## Setting the stage

**I will be drawing your attention to John Lowthorp’s 1705 abridgment of
the *Philosophical Transactions* not just because of its interest in
itself, but as a different kind of the paper laboratory discussed by
Doina-Cristina Rusu and Christoph Lüthy in examining Francis Bacon’s
*Sylva Sylvarum*.  The paper convinces me that William Rawley
published the raw notes Bacon had been assembling.  Bacon meant his
notes as a private paper laboratory from which to draw more works, so
Rawley produced a disordered book from his paper laboratory.**

This kind of paper laboratory contrasts with more deliberate attempts
to render collections of notes into published works.  The tradition of
commenting on Pliny, arguably begun by Pliny himself in his proems, is
continued with Ibn Sina’s commentary on Aristotle that in Albertus
Magnus’s paraphrasing analysis becomes its own source work, eventually
becoming something to comment on in its turn.

**Similarly, and later, we see Pierre Bayle’s notes and *Nouvelles de la
république des lettres* of 1684 to 1687 maturing into his 1697
*Historical and Critical Dictionary* that describes not the things and
people of its headnotes, but the errors others have made.  It is also
a sort of paper laboratory describing the errors in other books, but
public.  Over time, the necessary headnotes on which he hangs his
voluminous footnotes of errors become the established explanations for
phenomenon.**

**I think a useful way to think of the paper laboratory phenomenon,
public or private, is as a “literary technology” in the sense of a
technique for solving a writing and editing problem.  One must begin
writing on some topic, continue in some order, and finish at some
point.  Particular ways of doing this form a literary technology.  Paper
laboratories are just one kind.**

**I contrast literary technology with an “intellectual technology,”
which appears to describe phenomenon on their intellectual basis.  The
longstanding debates on Aristotle’s *Categories* indulge in just this:
what is the intellectual aim, completeness, or method for producing
these?**

**Said in these terms, Rusu and Lüthy’s point is that Bacon’s literary
technology was dressed up as an intellectual technology by Rawley, and
accepted as such.  The interesting point here is not that it started
as a literary technology, which any large project must have, but that
Bacon *did not intend to present this particular system as an
intellectual technology.***

**This point interests me because I think it requires a definitive
negative: Rusu and Lüthy’s argument works because they convince me
that Bacon would *not* have wanted the book made public.  This
contrasts with other paper laboratories, or literary technologies,
that became public by design.  Their literary origins are frequently
lost as the sheer labor of reproducing encyclopedic work means that
its practically more effective to accept an intellectually imperfect
literary technology than produce a fresh effort.**

**But, let me be specific by explaining how Lowthorp managed to produce his classification of science.**

# Lowthorp’s Abridgement and Classification

**John Lowthorp abridged and classified the topics of the earliest
scientific journal, the *Philosophical Transactions of the Royal
Society of London*.  Covering the *Transactions* from 1665 to 1700,
his work received the imprimatur of the Royal Society on 12 May 1705.**
The Licensing Act of 1662 had required such imprimaturs.  But the act
lapsed in 1695, so it is a bit unusual for such an imprimatur to
appear in a book this late.  It is even more unusual that **Henry
Jones’s continuation of Lowthorp’s abridgment, covering the
*Transactions* from 1700 to 1720, received the Society’s
imprimatur on 27 October 1721.**  The Copyright Act of 1710
fundamentally altered printing authority in England, so an imprimatur
would have been weirdly out of date.

Yet **the work of these two abridgments become increasingly prominent.
Lowthorp’s abridgment achieves five, separate, numbered editions
between 1705 and 1749.^[This doesn’t count nonce editions of
collections of the abridgments which might include volumes from any of
the five editions 1st, ESTC T103703; 2nd, ESTC T103702; 3rd, ESTC
N12579; 4th, ESTC T153711; 5th, ESTC T199443.]  Jones’s continuation
achieves three, separate, numbered editions between 1721 and
1749^[1st, ESTC T103708; 2nd, ESTC T103707; 3rd, T199438.] even though
it had a competing abridgment by Benjamin Motte sponsored by the
printers to the Royal Society.^[ESTC T103704.]**  These continuations
begin a series of abridgments covering the dated numbers of the
*Transactions* until 1750.^[1719–33, ESTC T103705; 1732–44, ESTC
T103701; 1743–50, ESTC T103700.] The last volume of he series,
abridged by John Martyn, Professor of Botany at Cambridge, is dated
1756 in the imprint.  Several other competing abridgments appeared
between 1705 and 1756, but those that achieved multiple editions were
either authorized by the society or written by Fellows of the society
previously authorized.  Thus these authorized abridgments become the
standard abridgments of record.  In 1753, the society takes over the
publication of the *Transactions*, which had been a special project
for the succeeding Secretaries, and this series of variously
authorized abridgments ceases.  The next abridgment of 1809 describes
the *Transactions* from 1665 to 1800 anew and presents it in a
chronological order, rather than classifying it.[^2]

[^2]: @hutton09:pt.

Lowthorp’s classification scheme, however, persists.  Paul Henry
Maty’s *General Index to the Philosophical Transactions* of 1787
provides an alphabetical list of topics keyed to the volumes of the
series begun by Lowthorp and continued in the authorized abridgments
to 1756.^[ESTC T108310] While he does not mention the classification
scheme provided by Lowthorp, we know he must have considered it for
his classification because he references books within it.
Furthermore, readers of that index might have had recourse to their
abridgments rather than the originals, so would have seen the original
scheme too.

\page

### Lowthorp’s scheme is as follows:

\startcolumns[n=2]

{\bf I. Mathematics:}
    1. Geometry, algebra, arithmetic;
    2. Trigonometry, surveying;
    3. Optics;
    4. Astronomy;
    5. Mechanics, acoustics;
    6. Hydrostatics, hydraulics;
    7. Geography, navigation;
    8. Architecture, ship-building;
    9. Perspective, sculpture, painting;
    10. Music.
    
{\bf II. Physiology:}
    1. Meteorology, pneumatics;
    2. Hydrology;
    3. Mineralogy;
    4. Magnetics;
    5. Zoology.
    
{\bf III. Anatomy, physic, chemistry:}
    1. Anatomy, physic:
       {\it i. Structure, external parts, and common teguments};
       {\it ii. Head};
       {\it iii. Thorax};
       {\it iv. Abdomen};
       {\it v. Humors and general affections};
       {\it vi. Bones, joints, muscles};
       {\it vii. Monsters};
       {\it vii. Period of human life};
    2. Pharmacy, chemistry.

{\bf IV. Philology, miscellanies:}
    1. Philology, grammar;
    2. Chronology, history, antiquities;
    3. Voyages and travels;
    4. Miscellaneous.

\noindentation{}Jones’s continuation follows a similar outline, as do all the succeeding volumes:

{\bf 1. Mathematical papers:}
    1. Geometry, arithmetic, algebra, logarithmotechny;
    2. Optics;
    3. Astronomy;
    4. Mechanics, acoustics;
    5. Hydrostatics, hydraulics;
    6. Geography, navigation;
    7. Music.

{\bf 2. Physiological papers:}
    1. Physiology, meteorology, pneumatics;
    2. Hydrology;
    3. Mineralogy;
    4. Magnetics;
    5. Agriculture, botany.

{\bf 3. Zoology, &c. anatomy, physic, chymistry:}
    1. Zoology, and the anatomy of animals;
    2. Anatomy, diseases:
       {\it 1. General and skin};
       {\it 2. Head};
       {\it 3. Neck, thorax, heart};
       {\it 4. Abdomen};
       {\it 5. Humours and general affections of the body};
       {\it 6. Bones, joints, and muscles};
       {\it 7. Pharmacy, chymistry}.

{\bf 4. Philology, miscellanies:}
     1. Manuscripts, printing;
     2. Chronology, history, antiquities;
     3. Travels, voyages;
     4. Miscellaneous.

\stopcolumns

## Proposing the abridgment

**Having established himself as a member of the Royal Society, Lowthorp eventually proposes two ambitious projects simultaneously.**

\TranscriptionStart

[**Page 20:**]

[…]

[*centered:*] 5 May 1703.

[…]

[**Page 21:**]

[…]

 M^r^ Lowthorp’s written Proposalls for Instruments design’d for advancing Astro-\
nomy, & perfecting Navigation , & for Experiments of good Use in Mechanics, Optics, [*‘for’ inserted between ‘&’ and ‘Experiments’, i.e. ‘& ‸^for^ Experiments’*]\
& Physics.  He was desired by the Society, in the first place to prosecute those Ex-\
periments which relate to Telescopes, and to bring to the next Meeting, a List of\
such Experiments as are neceſsary thereunto.

 **M^r^ Lowthorp also presented a Proposal for Printing an Abridgement of\
the Philosophical Transactions, together with a Printed Specimen of such an\
Abridgement.  His Specimen & this Design were approv’d by the Society, & he\
was desired to proceed therein.**

[…][^25]

[^25]: UkLoRS JBO/11/30 (pp. 20–21)

\TranscriptionStop

\noindentation{}**The length of these shortest effective minutes attest to the
complexity of his proposals.  The first one is the topic for another
paper, but the second concerns us here.  Note that a great deal is
happening.  He reads a proposal and he presents a specimen of the
abridgment to show the Society what it would look like when completed
years later.  The Society approves not just his design, but his
specimen, and desires him to proceed.  There are three actions here
and three responses:** **he solicits feedback on the intellectual plan for
his work (his “design”), he solicits feedback on the typographical design
for his work (his “specimen”), and he implicitly seeks administrative
approval to proceed with the project.  The minutes do not record the
discussion, but he presumably got feedback on the intellectual plan
and the typographical design as part of the approval process.  His
manuscript proposal differs from the later printed one, which must
reflect changes that he decided on based on this meeting.  The Society
could have merely approved of the idea, or the look, and not
encouraged him to proceed, but they ask him to produce the work.  It
wasn’t unusual to present proposals for new works to the Society; it
was, however, unusual for the Society to respond as a whole with
encouragement.**

**Let’s pause at this meeting, almost unknowable, and think about what
Lowthorp must have accomplished to propose his project.  Beyond his
election to membership, a specimen means that he has completed a
portion of the work, settled on a printing bookseller who could set
mathematical type, identified entrepreneurial booksellers to finance
the project, and found a mechanism for distribution.  The Society
doesn’t typically fund projects, so if he got approval—as he does—he
must have been prepared to carry out the work.  The specimen itself
suggests that he had already begun negotiations with the booksellers
around the work, giving them some of the copy text for the abridgment
and deciding on the style for headings, notes, and possibly the paper.**
**The apparent casualness of “also presented a proposal” covers up the
significant scale, cost, and effort in planning a major, three volume
reference work.  Why does he propose the abridgment after proposing a
sequence of his more typical work on instruments?  He make very little
progress on the instruments it seems.  Did he mean to give himself two
large projects or did he propose the first as cover for the second?
It’s likely he profited from his abridgment, so if he was willing to
sacrifice one project it seems like it would have been the first.  Did
he propose the second after the first, easier yes, like a good
salesman?**

**Much of the social work around succeeding at this proposal is almost
invisible to us now.  We don’t know the meetings he had in advance, if
any.  We don’t know if he recruited allies to support his proposal or
not.  We don’t know if he had enemies to overcome in proposing this
project.  However, it is a far bigger project than this short minute
suggests and involves a great deal of revenue to accomplish.**

### The Legal Context

The Society came into being with a series of administrative documents,
first a grant of incorporation in 1661, a first letters patent in
1662, revised letters patent in 1663, and a third revision in 1669.
Both the first and second letters patent give the Society special
privileges beyond a normal corporation.  The Licensing of the Press
Act of 1662 provided strong controls for printing and required that
works to be printed by reviewed by a censor to the press, or licensor.
Thus normal corporations could not decide on what documents to print,
nor send them to their own printer.  The Society’s second
letters patent grants both deciding power and the right to a printer,

> And further, of our more ample special grace and of our certain
> knowledge and mere motion, we have given and granted, and by these
> presents for us, our heirs, and successors do give and grant to the
> aforesaid President, Council, and Fellows of the aforesaid Royal
> Society, and to their successors for ever, or to any twenty-one or
> more of them (of whom we will the President for the time being, or
> his Deputy, to be always one), or to the major part of the aforesaid
> twenty-one or more, full power and authority from time to time to
> elect, nominate, and appoint one or more Typographers or Printers,
> and Chalcographers or Engravers, and to grant to him or them, by a
> writing sealed with the Common Seal of the aforesaid Royal Society,
> and signed by the hand of the President for the time being, faculty
> to print such things, matters, and affairs touching or concerning
> the aforesaid Royal Society, as shall have been committed to the
> aforesaid Typographer or Printer, Chalcographer or Engraver, or
> Typographers or Printers, Chalcographers or Engravers, from time to
> time, by the President and Council of the aforesaid Royal Society,
> or any seven or more of them (of whom we will the President for the
> time being, or his Deputy, to be one), or by the major part of the
> aforesaid seven or more; their corporal oaths first to be taken,
> before they be admitted to exercise their offices, before the
> President and Council for the time being, or any seven or more of
> them in the form and effect last specified; to which the same
> President and Council, or to any seven or more of them, we do give
> and grant by these presents full power and authority to administer
> the oaths aforesaid.[^26]

[^26]: (11) of pdf at https://royalsociety.org/about-us/governance/charters/  “Translation of Second Charter, A.D. 1663” 

\noindentation{}The careful wording outlines that the Society has the
right—for all time—to bind printers and engravers to the Society by
having them swear loyalty.  They can then send materials to those
printers and engravers to be printed.  This is accomplished by a
sealed order from “President and Council … or any seven or more of
them …, or by the major part.”  The passage outlines a flexible
procedure where the Council and President can identify particular
printers, write up terms of their work, deliver an oath, and send them
materials to be printed.  These materials are “things, matters, and
affairs touching or concerning the aforesaid Royal Society.”  Early
on, the Society asks a lawyer to flesh this out into their statues,
which describe the particular procedure for printing as well,

> Chap. XIII - Of the Printer to the Society
>
> I. The office of the Printer shall be to take care for the printing and
> vending such books, besides catalogues, and such other things, as
> shall be committed to him by order of the Society or Council ; in
> the doing of which he shall from time to time observe, and submit
> unto the directions and orders of the Council, both as to the
> correctness of the edition, the number of copies to be printed, the
> form or volume, the goodness of the paper, character, figures and
> diagrams, as likewise the price at which such books are to be sold ;
> nor shall he reprint any of the said books, or print them in any
> translation or epitome, without particular leave from the
> Council. [...][^27]

[^27]: cite the Records book- you have this in your directories.

\noindentation{}Thus the Society establishes the basic procedure to
work with their printer.  After following their letters patent, having
the printer swear their loyalty to the project, they then could send
orders from the Society or the Council.  Those bodies could specify
corrections, edition size, form, paper, and the typography, along with
the price for the books.  This procedure remains in place until the
mid ninteenth century.  The last sentence is particularly important to
Lowthorp’s abridgment, the Society’s printer could not reprint or
print an epitome without the Council’s authorization.

While the letters patent and the statues do not say that the Council
owns the copy to their own work, it does prevent the printer from
reprinting it without explicit authorization, effectively functioning
as copy.  Moreover, unlike the terms of the Copyright Act of 1710, the
Council controls translations and epitomes as well.  Generally,
abridgments and translations generated new copy, and thus a new right,
but here the older letters patent does not allow this.  These statutes
remained in force after the lapse of the Licensing Act in 1695, so
while printers themselves no longer faced restriction, the Society
possessed a special right to their own copy.

The *Transactions* had long been published using this right.  As we’ve
seen, it had become typical for material to be brought to the
Society’s meeting, read, and for the majority to vote to authorize its
printing.  Technically, the *Transactions* were authored by the
Secretary, but they required the license of the Council or Society.
Early on, the proces was more formal.  Henry Oldenburg would bring the
actual copy to the Council, who would appoint two members to review
it, and then authorize printing with the seal of the Society.
However, at this point the basic authority still existed and required
an order.  This is one of the important functions of the minutes where
the Society orders the printing of something.

**Lowthorp couldn’t actually abridge the *Transactions* without
authority from the Society. Yet there was no particular law in
place restricting the reprinting of anything.  To deal with this issue,
booksellers formed a conger of major sellers that respected each
other’s copy.  Their enforcement mechanism was to blacklist any
distributing bookseller who sold what they would consider pirated copy
and since their conger included major booksellers of the day, this
would have cost the distributing bookseller a great deal of
business.[^29]** Lowthorp, thus, needed to satisfy two separate
regulatory mechanisms: he needed to get the Council to approve the
reprint and he needed to convince a member of the bookselling conger
to take the project on.

[^28]: Careful readers will note that epitomes are mentioned in the
    letters patent, not abridgments; one wonders about a hypothetical
    legal case that attempted to distinguish the two concepts, but
    such a case did not occur here.

The reprints of Lowthorp’s abridgment don’t advertise revisions, nor
do they seem to be revised.  It seems that Lowthorp, after putting the
abridgment together, is no longer involved.  This would be in keeping
with copyright as it developed after 1710, when the printing of the
text awarded copyright to the entrepreneurial bookseller.  This also
dealt with a problem where President Broucnkner had licensed the
*Transactions* to John Martyn a bit earlier.  The entrepreneurial booksellers
considered the Stationers’ Register a record of copyright, so they
might not have accepted just any bookseller reprinting the
*Transactions*.  An abridgment, however, was a new work in their
tradition, but a reprint in the Society’s tradition.

**Lowthorp’s proposal then solves a number of problems: the
*Transactions* could be reprinted in a form that would satisfy the
legal claims of all the people involved.  As we will see, his
abridgment is much more like a reprint than it would appear at first.
More than proposing a new work, Lowthorp managed to propose a
just-new-enough work that fell between incompatible copyright
regulations.  His proposal carefully makes a near-reprint look new and
exciting.**

# The Manuscript Proposal

The original manuscript proposal is kept within the Classified Papers
at the Royal Society, a series appropriately enough, reorganized in
1720s along the same classification scheme developed by Lowthorp.[^31]
The volume contains a number of book-related manuscripts and this
proposal is the 59th item.  It is written on a bifolium, 2^o^: χ^2^; 4
pages, [*unnumbered, pp. 1-4*], on paper watermarked with the London
coat of arms (Churchill 239–244) [^32]and countermarked “C T” (stock,
% 13 \| 17 \| 24 \| 25 \| 2 [ 23 \| 23 ] 1 \| 25 \| 24 \| 24 \| 24 \| 25
\| 21 [ 4 \| 6 ] 15 [ 3 \| 6 ] 19 \| 24 \| 23 \| 16 % );[^33] the leaf
measures 301 × 194 mm and has 33 lines (on p. 2), in cursive, with
heavier strokes at the ends of words and on descenders. Page 2
continues across the gutter onto p.3 at the ends of some lines,
suggesting this was written open to both pages.  It is dated ‘May 5:
1702.’  on p. 4 and ‘Apr. 12. 1703’ on p. 1.  It seems likely that 21
April 1703 reflects Lowthorp’s dating and 5 May 1702 is later, perhaps
written by the Secretary at the meeting where it was read.  The year,
1702, is almost certainly wrong.  The inner margin of p. 1 is
discolored with its, presumably, previous pasting in another guard
book.  There are inked fingerprints on p.1, possibly oil-based
printers’ ink by the spread and show-through.  Page 4 is faintly
discolored into four folded panels measuring 81 × 194 mm.  In the
lower left of p.1 a penciled cross, probably Dr. Stacks’s, indicates
this item has been counted.  Modern penciled numbers ‘175’ on p. 1 and
‘176’ on p. 3 refer to the foliation of the present volume.

[^33]: This notation uses the pipe to indicate a chainline, square brackets to indicate a watermark, and a percent sign to indicate the deckle edge.  See @vandermeulen84:paper

[^32]: Cite Churchill

[^31]: Cite this!! CMO/3/84

Beyond the range of subscriptions and docketing on the item to record
its location and movement through the Society’s archive, the text is
Lowthorp’s.  I transcribe it at length below because it differs
significantly from the printed version and because evidence of
proposals for the production of large reference works in the
eighteenth century are scarce.

\TranscriptionStart

[*Next seven lines, centered:*] Apr. 12. 1703

Propoſalls for Printing\
The Philosophicall\
Transactions and Collections [*pen skip after ‘Transactions’*]\
To the End of the Year j700.\
Abridg’d and Diſpoſ\kern1pt{}’d under Generall Heads\
in **III.** Volumes.

By J. Lowthorp. **M.A.**&**F.R.S.** [*pen skip before ‘M.A.’*]

The Philoſophicall Transactions are well known to be an\
Excellent Regiſter of many Valuable Experiments, and a Cu⹀\
⹀rious Collection of such Diſcourſes , as being too ſhort to be
Printed\
alone, were in great Danger of being loſt: And have been\
publiſ\kern1pt{}hed almoſt every Month (some few Years of Interruption\
excepted) since the begining of the Year i665, by the particular\
Encouragement of the Royall Society .  But the generall Appro\
⹀bation they have met with, both at home and abroad , has long\
since made them so scarce , that a compleat Set is not to be pro⹀\
⹀cur’d without much trouble ,and not to be purchaſ\kern1pt{}’d under 15.£.\
And yet the vaſt Bulk of 22. thin Volumes in 4to. to which\
they are swell’d, and that want of Connection which could —\
not be avoided in this manner of Communicating such a Mul⹀\
⹀titude of independent Papers , make it altogether unadviſeable\
to Reprint them in their preſent Form .  The Author therefore\
of this Abridgement , conceives it may be some Service to the\
Publique , to Contract them into a leſs Compaſs; and to Range\
and Diſpoſe them in a better order, under Generall Heads:\
In the Execution whereof, He (hath for the moſt part ) confin’d\
himſelf to theſe Rules.

 i. He has divided the whole Work into 3. Volumes; The First\
contains all the Mathematicall Papers; The Second the —\
Phyſiologicall; and the First part of the 3d thoſe that\
are Medicall, the later part of it being reſerved for the [**Page 2:**]\
the Ph\rlap{\bf i}{y}lologicall and such Miſcellaneous Papers as cannot [*‘Ph\rlap{\bf i}{y}lologicall’ corrected from ‘Phylologicall’ by overwriting*]\
properly be reduc’d to any of the former Claſſes.  Each of theſe\
Volumes are subdivided into Chapters , and Generall Heads ,\
under which each Paper is inſerted in the beſt Order their —\
great Variety will admit of.

 2. He does not think it neceſſary in this Abridgement to\
retain the Accounts of Books which are added to the End of —\
each of the Tranſactions , since the Books themſelves are either\
long ago common in the Hands of such Readers as have Occaſion [*tail on ‘n’ extends to next page*]\
to uſe them , or so near loſt that few will be at the trouble to\
search for them . Yet that the Inquiſitive may have some Notice\
of them , He ha\rlap{\bfa s}{\kern2pt{}\tfx{}d} added to the End of each Chapter a
Catalogue [*‘ha\rlap{\bfa s}{\kern2pt{}\tfx{}d}’ corrected from ‘had’ by overwriting*]\
of such of them as Treat profeſtly and principally of that Subject [*blot over ‘at’ in ‘that’*]\
of that Chapter.

 3. The Additions , Emendations, and Refutations , of Books He does\
not think neceſſary to be retained here, becauſse such remarks can\
be only serviceable to thoſe who have the Books .  And therefore He\
contents himſelf with Directing his Readers to the Tranſac\rlap{\bfa t}{c}ions [*pen skip over ‘e’ in ‘Directing’; ‘Tranſac\rlap{\bfa t}{c}ions’ corrected from ‘Tranſaccions’ by overwriting*]\
themſelves for such Particulars .

 4. There are many Papers which were very Curious at the times [*tail of ‘s’ extends to next page*]\
of their Publication\rlap{\bfa (}{,}as the Calculations of Eclipſes , Lunar [*opening parenthesis overwrites comma*]\
Appulſes , Satellite Eclipſes , Tide Tables , and some others of that\
kind\rlap{\bfa )}{,} which are now uſeleſs .  Yet to do Juſtice to their Authors [*closing parenthesis overwrites comma; tail of ‘s’ extends to next page*]\
the Titles are inſerted in their proper places.

 5. There are another sort of Papers here Omitted which are [*dot over first ‘h’ in ‘which’; tail of ‘e’ extends to next page*]\
alſo very Curious viz. Inquiries and Directions , upon severall\
Subjects , and for places seldom viſited , drawn up and diſperſed\
by Order of the Royall Society · But there being very few who\
have any Oportunity of satiſfying either themſelves or others\
in such Particulars , The Author thinks that the Anſwers\
already given to many of the Enquiries , and the other Diſcourſes\
upon the same or the like Subjects here retained , will sufficiently\
supply this want . And therefore he chuſes to recommend it to [**Page
3:**]\
the Gentlemen of the Royall Society themſelves to Reprint thoſe\
Ingenious Papers, and to put them into such Hands as may probably\
have an Oportunity to make them suitable Returns , rather then to
swell\
theſe Volumes which are deſigned for more \rlap{g}{\bf G}enerall uſe. In the [*‘\rlap{g}{\bf G}enerall’ corrected from ‘generall’ by overwriting*]\
mean time He has not fail’d to inſert the Titles of theſe ,
as well [*pen skip between ‘has’ and ‘not’*]\
as of all other Papers Omitted, at the end of each Chapter to which\
they properly belong.

 6. He has been very Carefull , in Abridging the remaining Papers,\
to preſerve the Sence of their Authors entire, and to relate the\
Hiſtories of Facts with all the Materiall Circumſtances and uſefull\
Reaſonings thereon . But many of thoſe Papers being writ in the\
form of Letters and often interſperſed with perſonall Addreſſes;
[*second d in ‘Addreſſes’ inserted, Ad‸^d^reſſes*]\
And others of them being frequently interrupted with long Excurſi⹀\
⹀ons and Citations of Books, which for the moſt part rather Amuſe\
the Reader than either Inſtruct him or Illuſtrate the Subject; This\
Author humbly hopes that none will think themſelves Injured\
whilſt He takes a modeſt Liberty of Pruning such Luxuriances .\

 7. To cloſe in some Meaſure one of the Chaſmes in the\
Tranſactions , viz. between the Years i678 and i683. He hath inſe\rlap{—}{rt} [*dash overwrites ‘rt’ in inſe\rlap{—}{rt}’*]\
⹀ed , in their proper places , Dr Hook’s Philoſophicall Collections,\
being a Work of the same kind and Publiſh’d by him during that\
Intervall of time.

 8. To make the whole Work more compleat He will add such Ind⹀\
⹀dexes both of Authors and Subjects as Shall be thought of any [*‘⹀dexes’ corrected by canceling an apostrophe from ‘⹀dexe’s’*]\
uſe or Advantage to the Readers.


In short the Author of this Abridgement profeſſes that\
He hath every where deſign’d (according to the beſt of his Under⹀\
⹀ſtanding) to Omit nothing that would be of any Service to the
greateſt\
part of the Readers , and to Burthen them with nothing that could\
be spar’d without manifeſt Prejudice to the Sence of the severall\
Ingenious Authors : And that he hath carefully endeavoured that\
none of their Papers ſ\kern1pt{}hould suffer by paſſing through his Hands.

\TranscriptionStop

\noindentation{}The manuscript has corrections throughout, some which
do not reflect a change in pronunciation but a correction in spelling.
The presence of inky fingerprints suggests that this may have been
used to set the printed version of the proposal, but it differs
significantly as we will see, so it’s also possible this was copied.
The revisions, however, suggest that this final draft was presented
verbally.  The Society employed amanuenses to provide fair copies of
texts, to a text with this many corrections would have looked odd
against the more professional meeting minutes.  Since the document has
a function of persuasion, it seems unlikely that it would have been
presented in a less than perfect manner.

**The text carefully argues in favor of the abridgment, saying both that
the *Transacations* to 1700 are too small and thus might be lost, but
also very expensive, and also very large.  The phrase “vast Bulk of 22
thin volumes” seems to capture the rhetorical uncertainty of the
situation: Lowthorp needs to argue that the set is too big to reprint
as a whole, but that its so small that it might be lost.  He further
argues that the chronological order of the papers itself means they
shouldn’t be reprinted in their current form.  A classification scheme
would make them easier to read for both the Society and the public.
The proposal deftly switches from the subjunctive to the indicative,
from “conceives it may be some service” to “he hath.”  When proposing
the basic project, the proposal acts as though it has not begun, but
within the text we switch to the actual work, actually begun.**

The individual headings bear re-reading and follow the numbered and
interrelated form Lowthorp had used in his earlier *Letter* and would
use in producing this abridgments.  Each number follows logically from
the previous, but rather than presenting themselves as narrative
paragraphs, they present themselves as separate points.  The first
outlines the scale of the work along with the overall structure.
Lowthorp would have been particularly interested in mathematics, and
well suited because of his Latin and instrument work, and
physiological here means something closer to physics.  The last volume
ends up combining medicine, always of interest, with philology.  The
philological section seems to have been an afterthought since it
becomes miscellaneous and philological in the final version.

**Headings two through five describe what sorts of papers he will omit.**
He excludes the text of the accounts of books, but includes the
article.  In the final version, this has the look of a bibliography or
works cited at the end of sections.  Similarly, he plans to omit
calculations of calendrical phenomenon such as tides, eclipses, and
astronomical observations.  In the final version, many of these
remain, perhaps because astronomical researchers found historical data
useful and objected in this meeting.  The last category of paper
omitted are the queries the Society would propose, asking for the
details of foreign lands, trips, and observations.  His rationale is
that the Society no longer seeks the answers to most of these, but if
they still want them, they should reprint them separately and send
them to the interested people.  This change is worth noting: the
*Transactions* saw themselves as circulating among many people,
informing them and providing them with a way to contribute.  The
abridgment will see itself as transmitting information only in one
way.  Furthermore, the intellectual schemes of the queries provide
additional classification schemes.  Since Lowthorp aims to provide his
own classification scheme, it seems that their contribution to
categories of thought would no longer be needed.

**These exclusions mark a shift in how the project of the Society is
conceived.**  Early on, the *Transactions* were dialogues with readers
and Fellows.  People could be invited into the Society, contribute
their own thoughts, the Society reflected on current books, and the
numbered issues of *Transactions* might be broadly useful to all
classes of people.  The abridgment converts portable, stitched, issues
into heavy tomes that sit in a library.  Given Lowthorp’s reading, we
have a shift in how one imagines interacting with texts.  **Rather than
traveling the world, observing things, and reporting them to a central
office, Lowthorp imagines a gentleman in his study reviewing
scholarship and seeking after truth in libraries.**

**The sixth item reinforces this conceptual shift.  Calling the polite
greetings and editorials of the *Transactions* “luxuriances” he
proposes to cut them.  His rationale here is that his work no longer
seeks to “amuse the reader” but rather to “instruct him or illustrate
the subject.”  An apparently luxurious writer himself, at least in his
early work, it is curious he proposes to cut citations and the
evidence of the social interactions behind the activities recorded in
the *Transactions*.  Yet, in this item, lies another clever rhetorical
trick.**

**The final printed version of the abridgment gives the articles with
the “sense of their authors entire” by quoting their exact words.  The
abridgment merely removes whole sentences and phrases.**  An article
will typically have its greetings removed and the final sentences that
describe the moral of the experiment and what further experiments
might be conducted.  The general effect is to focus on facts, but the
practical effect is that Lowthorp needn’t write any new materials.  As
this proposal comes out in 1703 and he completes this major work by
1705, it seems that writing by marking out sections helped him to
achieve a high rate of composition.  We can even see his working copy
in the books sold after his death.

**In 1728, four years after his death, Lowthorp’s books were sold.**
Combining his library with that of Thomas Carew, the bookseller
Charles Davis sold the books “Cheap (the Price mark’d in each book)”
at his shop beginning Tuesday, 27 February 172$\frac{7}{8}$.[^34] **The
impressive collection attests to Lowthorp’s commitment as a scholar as
well as his financial success, but includes a description of what must
be the copy used for producing the abridgment under “Miscellanies, quarto,”**

[^34]: ESTC T26223, title page.

> **744 Philosophical Transactions, from 1665 to 1700. 21 vol. bound
> in 11. corrected by Mr. Lowthorpe.[^35]**

[^35]: ESTC T26223, p. 53

\noindentation{}Given Lowthorp’s career with the Society, there seem
to have been only two reasons for him to review the issues of the
*Transactions* up to 1700, either to produce this abridgment or to
study them on his own.  Individual study resulting in no product
certainly seems possible, but Lowthorp’s enthusiastic contributions
suggest that if he was systematically re-reading and correcting the
articles of the *Transactions*, we would have seen him present more
new ideas or contribute more ideas from other people.  Additionally,
the reported rebinding in eleven volumes suggests that something
happened to the original bindings.  Few other books in the catalog
have been rebound and there’s no evidence of Lowthorp creating
interleaved copies, so it seems unlikely these eleven volumes
represent heavy use.  Furthermore, there are other copies of the
*Transactions*, not rebound and not corrected, for sale from
Lowthorp’s library.  If Lowthorp had taken the print volumes and
marked out the passages to exclude, and disbound them to give the
printers the materials, then the resulting pages would look corrected.
They would have many passages marked out, some words added, and the
classification numbers marked on the pages.  Furthermore, the
compositors reading the marked-up copy against their printed text
would have made notes of *their* corrections to the abridgments on the
printed copy, giving the appearance of more corrections.

If the original volumes had been taken apart and marked up, circulated
in a printing house, then they would need to be rebound.  Since
Lowthorp must have been preserving these as a record of his work, why
preserve the logical volumes that are needed for reference?  Rebinding
it in eleven volumes makes finding a particular article more
difficult, but makes perfect sense if you have multiple copies of the
*Transactions* and this one is just the working copy for the
abridgment. Thus **I think it’s probable that this corrected copy is the
printers’ copy used to produce the abridgment.**

## Bibliographical argument

\BiblHangingIndentsStart

**Vol. 1:** 4^o^: [*\**]^4^ 2\*^2^ a–n^2^ B–3M^4^ 4A–4U^4^ 4X^2^ [\$2 (−2\* a–n 4X2) signed; d1 signed D1]; 342 leaves, pp. [*32*] 1–454 543–708[=620] (misnumbering 200 as 100); 1–7 engraved plates (pl. 6 as ‘7’), pl. 1 facing pp. 54 (H3^v^), 2: 190 (2B3^v^), 3: 388 (3D2^v^), 4: 556 (4B2^v^), 5: 626 (4L1^v^), ‘7’ [=6]: 660 (4P2^v^), 7: 698 (4U1^v^)\

\BiblHangingIndentsStop

**To summarize a great deal of research, it is probable based on signing
and pagination that 4A-4N were printed first; it is probable based on
typographical style that 4A-4N were printed first; it is probable
based on type damage, catchwords, and text that 4A-4N were printed
first; and it is probable that 4A-4N were printed first based on the
paper evidence.  Based on these independent probabilities, I’d say
it’s most likely that 4A-4N were printed first.**  Perhaps B-C were
begun along the way, but 4A must have been the very first sheet of the
volume printed.

**Knowing this is first helps with the question of the specimen.  Since
the specimen was probably an independently set example, the type could
have been reused.  Indeed, unless there was something wrong with the
type, reusing it would save time.  A good specimen should demonstrate
the headings and basic features of the book and, delightfully, 4A
begins the section on acoustics.  It might be too much of a
coincidence that Lowthorp was working as James Brydges’s librarian and
that Brydges was also Handel’s patron, but then again Lowthorp might
have been helping Brydges plan musical venues.**

The regularity of volumes 2 and 3, and the higher quality paper at the
beginning of both volumes, suggest that their beginnings were produced
in the same batch.  As the printed prospectus says the first volume is
in progress, and the copy nearly ready for the next two, it would make
sense for the entrepreneurial bookseller to begin to print faster by
working on volumes 2 and 3 at the same time, that way he could meet
the upcoming deadline.


