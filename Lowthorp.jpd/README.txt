JP DOCUMENT FORMAT
==================

This directory contains the files comprising one logical document.
The design follows four principles:

Archival:  Using the files requires only decades-old, stable, free
           software tools undergoing active development, my best guess
           as to what will be usable in the future.

Expert:    The file formats behave well with state-of-the-art
           tools used by sophisticated professional developers,
           e.g. Git, Emacs, Vim, VSCode, POSIX, etc.

Adaptable: The structure allows for unconventional symbolic
           expressions not anticipated by particular tools.

Available: The document translates to conventionally formatted
           documents adhering to mainstream expectations: Doc, PDF,
           HTML, etc.

NAMING
------

The directory shall be named with a "jpd" extension within the file
system of the host operating system. (Historically, the directories
had been named camel case, i.e. fileDoc; such usage is no
longer recommended.)

REQUIRED FILES
--------------
- README.txt: this file
- makefile: can produce PDF, ODT, HTML, and Doc formats
- headers.yaml: contains formatting information

OPTIONAL FILES
--------------

These are needed for the more advanced bibliographical style and the
standard style that can refer to the extended glyphs in that style.

- standard-style.tex.include: contains ConTeXt formatting for standard PDFs
- custom-libertine-typescript.mkiv: ConTeXt typescript for standard style
- chicago-fullnote-no-bibliography.csl: Chicago with functional `notes` field
- mkjpd.sh: a bash script to prepare a new JP document
- jpd.el: Emacs functions for working with JP documents

LEGACY FILES
------------

Some features from the bibliographical style have not been fully moved
into the standard style.  It would be better to move those features
over than to use this file, but it is included until I get around to that.

- bibliographical-style.tex.include: ConTeXt for bibliographical style in PDFs

DEPENDENCIES
------------

While the collection of files shall be readable by any standard text
editor, they must also produce a typeset, printable, version.  At this
point, only PDF has been fully implemented.  ODT, Doc, and HTML files
do not include some formatting.  To produce a PDF requires this
software and its dependencies:

- Pandoc: a universal document converter (https://pandoc.org)
- GNU make: a standard tool for compiling (https://www.gnu.org/software/make/)
- ConTeXt: a collection of TeX macros (http://context.aanhet.net)

OPTIONAL DEPENDENCIES
---------------------

Bibliographical style relies on a specialized typeface.

- JunicodeRX: Junicode with Restoration-era typography (https://gitlab.com/cacology/JunicodeRX)
- Symbola: designed by George Douros it provides many, many fallback glyphs
- Emacs: useful for editing and has some automated scripts included
- bash: can run the other automated script to set up a new document

USE
---

Most documents use a single file such as `doc.md`, for which `make
doc.pdf` will prepare a PDF named `doc.pdf`.

For complicated documents, running `make final.pdf` will compile all
.md files into one large document.  Since the files are sorted in
ASCII order, I prefix them with numbers such as 1.2.file.md,
4.5.3.file.md, etc.  As with library number assignment, it is best to
avoid extremes.  Since no number comes before 0, one cannot insert an
additional file at the beginning.  However, if you have:

- 1.file.md
- 2.file.md

Then you can insert a file before, between, and after thus:

- 0.5.file.md
- 1.file.md
- 1.5.file.md
- 2.file.md
- 2.5.file.md

Each file can have a distinct YAML header and
`\input{standard-style.tex}` early on causes ConTeXt to load that
style file for PDF production.

Following GNU conventions `make clean` removes intermediate files and
`make cleanall` removes intermediate files and targets.

TYPING CONVENTIONS
------------------

The contained files with the extension `md` are pandoc-flavored
markdown with raw ConTeXt for some non-markdown typesetting.  At the
time of writing, the Pandoc homepage provides an introduction and full
documentation of their flavor of markdown.  ConTeXt includes complete
manuals in the standard install included as part of the Tex Live
package, but also has manuals on the "ConTeXt Garden Wiki."  Both have
large established user groups on internet forums.

CUSTOMIZATION
-------------

You may wish to update the `doc.md` file to reflect your own name and
wherever you store your BibLaTeX file for references.  You can also
customize the `makefile` to work with other kinds of documents.

HISTORY
-------

This system was developed by JP Ascher during the course of completing
his dissertation, writing term papers, and preparing reports.  For his
dissertation, he needed to code the individual glyphs used by
particular compositors, rather than accepting standard Unicode code
points.  For a discussion of the custom CSL, see
https://github.com/citation-style-language/styles/pull/4578 .
For a more thorough explanation of the design principles see the
"Technical Partition" of https://doi.org/10.18130/jqpe-zc65 .
