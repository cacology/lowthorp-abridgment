---
title: "Study of Lowthrop’s abridgment proposal"
author: J. P. Ascher
date: May 2022
citation-style: chicago-fullnote-no-bibliography.csl
bibliography: /Users/james/org/research.bib
...
\input{standard-style.tex.include}

# TODO

- [ ] type of corrections: note the changes from one to another
- [x] proof Prospectus against original, correct measurements, measure
      chainlines
- [x] review Bagford inventory against original
- [ ] identify Bagford in JBO/9–11 <- think about this, maybe not needed?
- [X] find the CMO (nothing useful)
- [X] & JBO materials, transcribe
- [ ] investigate the biographies of major players
- [ ] contact Huntington about James Brydge’s papers and librarian
      Lowthorp
- [x] look at second edition too!
- [ ] collate second edition and demonstrate absence of revision
- [ ] flesh out descriptions of materials from notes
- [ ] flesh out argument about the changing modes of classification
- [ ] review Oldenburg’s index to *PT*
- [ ] transcribe additional RS material from notes
- [x] finish paper stock measurements through 4A section and B section
- [x] think about plate notation, which is better? [**engraved
      199.0×356.0 (201×361 ; sheet 238.0×374 ; originally folded left
      to right 171v, 132m, 71**]
      [**18(1[199]3)17×7(2[164\|94\|100]2)6**]

## Technical TODO

- [x] update to default old style numerals
- [x] build extension for forcing new style {\os 1996} {\ns 1996}
- [x] Fraktur font needs a better P, y, and long-ſ, long-ſ ligatures,
- [x] Fraktur font needs fallback: Tur: \dontleavehmode{\fraktur Penny for oﬃce bo fol \swashY{}ou.}
- [x] italic fallback for 🙰 symbol! *Italic with 🙰 and &*

## Long term TODO

- [ ] JunicodeRX needs an italic long-s i ligature
- [ ] I need a coordinated fraktur in which I can add the ligatures I
      want
- [ ] fix style sheet for transcriptions so that subsequent lines hang
      (attempted, this is hard because it’s not trivial to distinguish
      a hard line break from a soft one in the typesetting engine.
      You have new paragraph, new line: I don’t know where the forced
      new line might be distinguished from a new paragraph.) Current
      solution is to *end* with italics: not bad for now. 
- [ ] the long-term fix for the above is to create a transcription
      environment that has line breaks in the document versus in the
      text, there are such environments for poetry, which might be
      worth trying out

## Essay outline

- The moment of the Glorious Revolution to the new millennium brought
  about a change in how classification worked, lit review

- Preserved in the collections of the RS and BL, two contrasting
  moments: Bagford’s classification of prospectuses, Lowthorp’s
  classification of transactions.

- Bagford: cite Trettien, etc. assemblage as mode, juxtaposition

- Lowthorp: Anglican, lost rectory, librarianship to James Brydges,
  etc. discipline and book sales

- Lapse of Licensing Act as legitimizing the assemblage as publishing
  project, but the mechanisms unclear

- Bagford succeeded in documenting Lowthorp, but failed to write his
  book; Lowthorp succeeded in writing his book, but failed to document
  Bagford (and much of the society).

- Broad thinking: what do we do when we classify?  What is silenced?
  What promoted?  Cite recent work on justice in archives, etc.

## Description of materials prior to the proposal

- MS/703 describes the presentation:

**Spread 226:** “[An^o^: 1702. M. D.: May : 5; *i.e. 170$\frac{2}{3}$
May 5*] [Lowthorp’s]{.underline}. Proposal for [Astronomical
Instr^ts^.]{.underline} & [mechanical Exper^ts^.]{.underline} —
[Enter’d in Reg^r^ books. Vol. Pag.] 9:100” and “[D^o^.]{.underline}
Proposal for printing his [Abridgement]{.underline} of
[Phil. trans.]{.underline} — [Originals in Guard-books. Vol. Pag. №]
17”

## Journal Books

**JBO/10:** reversed calf conserved with a new spine, pen on upper
board ‘Journal Booke \| N^o^ 10.’, letting pieces on spine, upper:
‘JOURNAL \| BOOK OF THE \| ROYAL \| SOCIETY’, lower: ‘VOL \| X \| 1696
\| 1702’, refreshed endpapers; throughout, Arms of Amsterdam
(cf. Churchill 28–32, particularly 29) with HS monograpm countermark
ca. 1696; paginated in contemporary ink pp. [*14*] 1–255 [*3*];
individual meetings marked in pencil by modern librarian with record
number; written in several hands.  This volume records the revised,
fair-copy minutes of the regular meeting of the Royal Society from 14
Oct 1696 (p. 1) to 14 Oct 1702.  Throughout a penciled dash marks
something.

**JBO/11:** Modern reversed calf with blind tooling and red lettering
pieces on the spine, ‘JOURNAL \| BOOK OF THE \| ROY. SOCIETY’,
‘VOL. XI.’, ‘1702 \| 1714.’, and the Royal Society’s arms stamped in
black; upper endpaper modern, cloth reinforced joints; throughout, Pro
Patria watermark (Churchill 129, cf. 127) and ‘AI’ countermark of
Abraham Janssen ca. 1701; foliated to 7, but paginated throughout
pp. [*4*] 1–77 77a 77b 78–381 383–439[=440] [*16*].  This volume
records the revised, fair copy minutes of the regular meeting of the
Royal Society from 21 October 1702 (p. 1) to 10 June 1714
(pp. 436–439) in a variety of hands, witnh few corrections throughout
and marginal insertions along with marginal notes of papers and some
orders.  Throughout a penciled cross marks mentions of papers and is
likely Dr. Stacks.  Modern librarians have numbered the meetings with
a sequential order, however, the fourth meeting called JBO/11/4 was
unnumbered, so JBO/11/5 and forward have pencil numbers one less.
I.e. JBO/11/5 is numbered 4 in pencil.

**JBO/10/95 (pp. 108–110):**

\TranscriptionStart

[**page 108:**]

[*centered:*] March · 8^th^ · 169$\frac{8}{9}$

[…]

[**page 109:**]

[…]

 Mr Louther read a propoſal to adjust refractions, by\
making an Exp^t^ of the Refraction with the Air Pump.\
The Air being exhauſted out of a Machine made on pur⹀\
poſe.  M^r^ Hunt was ordered to take directions of M^r^\
Louther to try the Experiment this day Sennight.

[…]

\TranscriptionStop

**JBO/10/96 (pp. 110–111):**

\TranscriptionStart

[**page 110:**]

[…]

[*centered:*]  March · 15^th^· 169$\frac{8}{9}$.

[…]

 Mr Louther ſaid he tryed the Experiment mentioned [**page 111:**]\
laſt day, about refraction, but that the ſhaking of the\
Air pump had hindered teh succeſs of the Experiment.

 It was ordered y^t^ Mr Hunt ſhould aſsiſt Mr Louther\
in making Further Experiments toward aſcertaining\
refractions.

[…]

\TranscriptionStop

**JBO/10/97 (pp. 111–113):**

\TranscriptionStart

[**page 111:**]

[…]

[*centered:*] March. 22 169$\frac{8}{9}$.

[…]

 Mr Louther promiſed aganst this day Sennight, to\
show his New experiment about Refractions.

[…]

\TranscriptionStop

**JBO/10/98 (pp. 113–4):**

\TranscriptionStart

 [**page 113:**]
 
 […]
 
 [*centered:*] March. 29. 1699.
 
 […]
 
 [**page 114:**]
 
 […]
 
  M^r^ Louther made report of the ſucceſs of his\
 Experiment about refractions: he was thanked, and\
 deſired to give it in writing.
 
 […]

\TranscriptionStop

**JBO/10/99 (pp. 114–6):**

\TranscriptionStart

[**page 114:**]

[…]

[*centered:*] April. 12^th^ 1699.

[…]

[**page 115:**]

[…]

 Mr Louther excuſed himſelf by D^r^ Woodward till \~\
next day, the giving an account of his Experiment.

[…]

\TranscriptionStop

**JBO/10/100 (pp. 117–8):**

\TranscriptionStart

[**page 117:**]

[*two lines centered:*] April 19 · 1699 ·
M^r^. Bridgeman VP in y^e^ chair.

The Vice Preſident propoſed it as very neceſsary y^t^ an‸^exact^\
account might be kept of the Minutes and y^t^ care might\
hereafter be taken, that the books ſhould not be behind\
hand

[…]

[**page 118:**]

[…]

M^r^ Louthorpe read a learned dyſcourſe concerning\
[*empty line*]\
The Society returned M^r^ Lowthorp thanks, & deſired\
a copy of his diſcourſe.

[…]

\TranscriptionStop

**JBO/10/101–118 (pp. 119–147):** 25 April 1699, 3 May, 10 May, 17 May, 24 May, 31 May, 7 June, 14 June, 21 June, 28 June, 5 July, 12 July, 19 July, 26 July, 2 Aug, 9 Aug, 16 Aug, 25 Oct: Nothing involving Lowthorp reported.

**Uk Sloane 4037 (351–2):**

\TranscriptionStart

[**page 1:**]

S^r^,  [*far right:*] Oct^r^.30. 1699.

 I had sent you the papers, which I have\
now desir’d M^r^ Hunt to deliver, long ago; but\
my affairs call’d me out of Town before you\
had given your [*canceled by crossing out:* des?] Order to him to engrave\
the plate, without which it was Impoſsible\
for me to finish them.  I have not since had an\
oportunity to do it till very lately: and then I\
sent for them on purpoſe to satiſfy you that\
I was not wanting in my Obedience to the\
Com̃ands of the Society.  I submit them entirely\
to their Judgement whom I think the beſt Judges in\
the World: and I only begg that my zeal to Attempt\
something for their ſervice may Cover the Imperfec\
⹀tionſ of my performance; and that you\
S^r^ will believe me\

     [*right:*] Y^r^ Moſt Humble Serv^t^..\
‌          [*right:*] J. Lowthorp.

 If you combine y^e^ Reſolution of inſerting [*canceled by crossing out:* that?] this\
Account of the Experiment in y^e^ Tranſactionſ I should\
be glad to ſee the proof ſheet before it be printed off.\
It may be ſent to me and return’d in 4 or 5 days if the\
Circumſtances of the Preſſ can allow of ſuch a delay.

 If I can ſerve you in any thing be pleas’d to [*canceled by crossing out:* send?]^encloſe^\
your Com̃andſ for me in a cover directed to my wife[*?*]\
at M^r^ Law’s near the Chery Tree Tavern in Jermyn ſt

[**pages 2–3:** [*blank*]

[**page 4:**]

[*folded with a seal, addressed in the central panel:*]

To

   [*centered:*] D^r^. Sloane

one of the Secretary’s to\
‌ the Royall⹀Society. [*pen flourish*]

\TranscriptionStop

**JBO/10/119 (pp. 147–8):**

\TranscriptionStart

[**page 147:**]

[…]

[*centered:*] Novemb^r^. 1^o^. 1699.

[…]

 A Lett^r^. was read from M^r^. Lowthorp with an encloſed\
paper, concerning the Experiments about Refraction: It was\
ordered to be printed in y^e^ Transactions: and he was thanked.

\TranscriptionStop

**JBO/10/120–3 (pp. 148–53):** 8 Nov 1699, 15 Nov, 22 Nov, 29 Nov: Nothing involving Lothorp reported.

**JBO/10/124 (pp. 153–5):**

\TranscriptionStart

[**page 153:**]

[*centered:*] Decem^r^. 6. 1699.

[…]

 D^r^. Woodward promiſed to bring the lett^r^. from M^r^ Lowthorp.

[…] 

\TranscriptionStop

**JBO/10/125 (pp. 155–6):**

\TranscriptionStart

[**page 155:**]

[*centered:*] Decemb 13 · 1699 ·

[…]

 D^r^. Woodward read an acc^t^ of a veſſel deigned to go directly ag^t^.\
the wind, from M^r^. Lowthorp, with a letter to illuſtrate his\
account

 It was deſired by the So: y^t^ D^r^. Woodward would ſhow the\
paper, and lett^r^. to S^r^ Chr: Wren, and deſire his thoughts of it.

It was alſo deſired y^t^ M^r^. Lowthorp ſhould be thanked & deſired\
to preſecute his Experiments of this nature.

[…]

\TranscriptionStop

**JBO/10/126–234 (pp. 156–254):** 20 Dec 1699, 27 Dec, 3 Jan
$\frac{1699}{1700}$, 10 Jan, 17 Jan, 24 Jan, 31 Jan, 7 Feb, 14 Feb,
21 Feb, 28 Feb, 7 March, 13 March, 20 March, 27 March 1700, 3 April,
10 April, 17 April, 25 April, 1 May, 8 May, 15 May, 22 May, 29 May, 5
June, 12 June, 19 June, 4 July, 10 July, 17 July, 24 July, 31 July, 23
Oct, 30 Oct, 6 Nov, 13 Nov, 26 Nov, 27 Nov, 30 Nov, 4 Dec, 11 Dec, 17
Dec, 8 Jan 170$\frac{0}{1}$, 15 Jan, 22 Jan, 29 Jan, 5 Feb, 10 Feb,
26 Feb, 11 March, 19 March, 26 March 1701, 9 April, 16 April, 30
April, 7 May, 14 May, 21 May, 28 May, 4 June, 11 June, 18 June, 25
June, 2 July, 9 July, 15 July 1701, 23 July, 30 July, 6 Aug, 22 Oct, 5
Nov, 12 Nov, 19 Nov, 26 Nov, 3 Dec, 10 Dec, 17 Dec, 24 Dec, 31 Dec, 7
Jan 1701/2, 14 Jan, 21 Jan, 28 Jan, 4 Feb, 11 Feb, 18 Feb, 25 Feb, 4
March 1701/2, 11 March, 18 March, 25 March 1702, 1 April, 8 April, 15
April, 22 April, 29 April, 6 May, 13 May, 20 May, 27 May, 3 June, 11
June, 17 June, 24 June, 1 July, 8 July, 15 July: Nothing involving
Lowthorp reported.

**JBO/10/189–90 (p. 221–2):** Bagford presents

**JBO/10/235 (pp. 254–5):**

\TranscriptionStart

[**page 254:**]

[…]

 [*two lines centered:*] July.22.1702.\
M^r^ Hill V. P. in the Chair.

 M^r^ Lowthorp was permitted to be present.

[…]

\TranscriptionStop

**JBO/10/236 (p. 255):** 14 Oct 1702: Nothing involving Lowthorp reported.

**JBO/11/1–3 (pp. 1–2):** 21 Oct 1702, 28 Oct, 4 Nov: Nothing involving Lowthorp reported; no meeting on 4 Nov.

**JBO/11/4 (pp. 2–3):**

\TranscriptionStart

[**page 2:**]

[…]

[*centered:*] Novemb. 11. 1702.

[…]

 M^r^ Lowthorp was permitted to be present.

[…]

\TranscriptionStop

**JBO/11/5 (pp. 3–4):**

\TranscriptionStart

[**page 3:**]

[…]

[*three lines centered:*] November 18. 1702.\
S^r^.  John Hoskyns V.P. in the Chair.\
M^r^ Lowthorp & M^r^ Jones were permitted to be present.

[…]

 The same [M^r^ Hunt] gave, from M^r^ Bagford, a Ratt, which had accidentally been shut up in a\
chest at Richmond, & had been eating it’s own Thigh, for want of food.  M^r^ Bagford was Order’d\
the Thanks of the Society.

[…]

\TranscriptionStop

**JBO/11/6 (pp. 4–5):**

\TranscriptionStart

[**page 4:**]

[…]

[*centered:*] Novemb. 25. 1702.

[…]

 Mons^r^. Chardeloup & M^r^ Lowthorp were permitted to be present.

[…]

\TranscriptionStop

**JBO/11/7 (p. 5):**

\TranscriptionStart

[**page 5:**]

[…]

[*two lines centered:*] Novemb. 30. 1702.\
S^r^. John Hoskyns V. P. in the Chair

 M^r^ Lowthorp, M^r^ Young, M^r^ Ludlow, & Mons^r^. Chardeloup were proposed, Ballot-\
ed for, & Elected Members of the Society.

 M^r^ Lowthorp & Mons^r^. Chardellou subscribed the Statutes, & were Admitted.

[…]

\TranscriptionStop

**JBO/11/8 (pp. 5–6):** Nothing involving Lowthorp reported.

**JBO/11/9 (pp. 6–7):**

\TranscriptionStart

[**page 6:**]

[…]

[*centered:*] December 9. 1702.

[…]

 M^r^ Lowthorp said, that the Banks in Romney Marsh do not ascend by an An- [*descender from ‘r‘ in ‘Lowthorp’ canceled*]\
gle, above 20 degrees.

[…]

\TranscriptionStop

**JBO/11/10–11 (pp. 7–8):** 16 Dec 1702, 23 Dec: Nothing involving Lowthorp reported.

**JBO/11/12 (pp. 8–9):**

\TranscriptionStart

[**page 8:**]

[…]

[*centered:*] December 30 1702.

[…]

 M^r^ Lowthorp shewed an Instrument newly invented by him, whereby the Al-\
titude of the Sun at Sea, may be taken to a great exactneſs : Twas desired that a Draught [**page 9:**]\
‌ of it may be publish’d in the Transactions,and M^r^ Lowthorp was thanked for the\
Communication.

[…]

\TranscriptionStop

**JBO/11/13–29 (pp. 9–20):** 6 Jan 1702/3, 13 Jan, 20 Jan, 27 Jan, 3
Feb, 17 Feb, 24 Feb, 3 Mar, 10 Mar, 17 Mar, 24 Mar, 31 Mar, 7 April,
14 April, 28 April: Nothing involving Lowthorp reported.

**JBO/11/30 (pp. 20–21):**

\TranscriptionStart

[**Page 20:**]

[…]

[*centered:*] 5 May 1703.

[…]

[**Page 21:**]

[…]

 M^r^ Lowthorp’s written Proposalls for Instruments design’d for advancing Astro-\
nomy, & perfecting Navigation , & for Experiments of good Use in Mechanics, Optics, [*‘for’ inserted between ‘&’ and ‘Experiments’, i.e. ‘& ‸^for^ Experiments’*]\
& Physics.  He was desired by the Society, in the first place to prosecute those Ex-\
periments which relate to Telescopes, and to bring to the next Meeting, a List of\
such Experiments as are neceſsary thereunto.

 M^r^ Lowthorp also presented a Proposal for Printing an Abridgement of\
the Philosophical Transactions, together with a Printed Specimen of such an\
Abridgement.  His Specimen & this Design were approv’d by the Society, & he\
was desired to proceed therein.

[…]

\TranscriptionStop

**JBO/11/31 (pp. 21–2):**

\TranscriptionStart

[**Page 21:**]

[…]

[*centered:*] 12 May 1703

[…]

[**Page 22:**]

 M^r^ Lowthorp presented a Paper wherein he proposed the making a Glass\
Prism coloured to try some Experiments for perfecting Telescopes, which Prism\
he was desired to procure.
[…]

\TranscriptionStop

**JBO/11/32–38 (pp. 22–27):** 19 May 1703, 27 May, 2 June, 9 June, 16
June, 23 June, 30 June: Nothing involving Lowthorp reported.

**JBO/11/39 (pp. 27):**

\TranscriptionStart

[**page 27:**]

[…]

[*centered:*] 7. July. 1703.

[…]

 A Petition was read from Rob.^t^ Rowe , setting forth, that he has found out a [*pen mark over ‘s‘ in ‘has’*]\
Method of Observing the Longitude , & desireing the Societies Certificate of it, if he\
makes it out. [*pen mark over ‘s’ in ‘makes’*]

[…]

 M^r^ Lowthorp [having been] desired to confer with M^r^ Rowe abovementioned , said that [*‘having been’ inserted and corrected by overwriting, ‘Lowthorp ‸^having^ be\rlap{\bfa e}{i}n\rlap{\bfb •}{g}*]\
M^r^ Rowe’s Method of Finding out the Longitude was insignificant, and that he [*canceled word ‘pre’? between ‘was’ and ‘insignificant’*]\
would endeavor to satisfie him about it.

[…]

\TranscriptionStop

**JBO/11/40–45 (pp.27–32):** 14 July 1703, 21 July, 28 July, 4 Aug, 11
Aug, 18 Aug: Nothing involving Lowthorp reported.

**BAGFORD JBO/11/43 (pp. 29–30):**

\TranscriptionStart

[**page 29:**]

[…]

[*centered:*] 4 August. 1704 [*corrected in pencil to 1703*]\

[…]

[**page 30:**]

[…]

 M^r^ Bagford presented Specimens of several Sorts of Paper, For w^ch^\
he was thanked.

[…]

\TranscriptionStop

**JBO/11/46 (p. 32):**

\TranscriptionStart

[**page 32:**]

[…]

[*centered:*] 20. October. 170\rlap{\bfa 3}{2}. [‘170\rlap{\bfa 3}{2}’ corrected from ‘1702’]

[…]

 A Letter was Read from M^r^ Blundell ‸^of\ Coventre,^ giving an account of P. He-\
rison, & Van Keulen abou squaring the Circle.  M^r^ Lowthorp was de-\
sired to puruse it and write an answer.

[…]

\TranscriptionStop

**JBO/11/47 (pp. 32–3):**

\TranscriptionStart

[**page 32:**]

[…]

[*centered:*] 27. October 1703.

[…]

[**page 33:**]

[…]

 Mr^r^ Lowthorp brought an Answer to M^r^ Blundells Letter, for which he was\
thanked, & M^r^ Wanley was desired to forward it to him.

 A paper of M^r^ Wanleys, drawn up by the desire of the Society, concerning the\
Invention of Printing, & the Progreſs of it in Italy, to the year 1465, &c. was read\
He was thanked, & desired to suffer it to be printed in the Transactions.

[…]

\TranscriptionStop

**JBO/11/48–50 (pp. 33–35):** 3 Nov 1703, 10 Nov, 17 Nov: Nothing involving Lowthorp reported.

**JBO/11/51 (pp. 35–6):**

\TranscriptionStart

[**page 31:**]

[…]

[*centered:*] 24. November 1703.

[…]

[**page 32:**]

[…]

 M^r^ Lowthorp said he had tried a Watch out of the Pneumatic En-\
gine, and in it , and that there was no difference in its going.

[…]

\TranscriptionStop

**JBO/11/52–57 (pp. 36–39):** 30 Nov 1703, 8 Dec, 15 Dec, 22 Dec, 5
Jan 1703/4, 12 Jan: Nothing involving Lowthorp reported.

**JBO/11/58 (pp. 39–40):**

\TranscriptionStart

[**page 39:**]

[…]

[*centered:*] 20 January, 1704.

[…]

 A Paper of M^r^ Lowthorp’s concerning Burning-Glaſses, was read.

[…]

\TranscriptionStop

**JBO/11/59–112 (pp. 40–72):** 2 Feb 1703/4, 9 Feb, 16 Feb, 23 Feb, 1 Mar, 8 Mar, 15 Mar, 22 Mar, 29 Mar 1704, 5 April, 12 April, 19 April, 26 April, 3 May, 10 May, 17 May, 24 May, 31 May, 7 June, 14 June, 21 June, 28 June, 5 July, 12 July, 25 Oct, 1 Nov, 8 Nov, 15 Nov, 22 Nov, 6 Dec, 30 Nov, 13 Dec, 20 Dec, 3 Jan 1704/5, 10 Jan, 17 Jan, 24 Jan, 31 Jan, 7 Feb, 14 Feb, 21 Feb, 28 Feb, 7 Mar, 14 Mar, 21 Mar, 28 Mar 1705, 11 April, 18 April, 25 April, 2 May, 9 May, 16 (19?) May, 23 May, 30 May: Nothing involving Lowthorp reported.

**BAGFORDJBO/11/72 (pp. 49–50):**

\TranscriptionStart

[**page 49:**]

[…]

[*centered:*] 3. May. 1704.

[…]

 M^r^ Bagford presented the Society with an old pair of Bellows, whose upper\
Valve is Carved. He was Thanked.

[…]

\TranscriptionStop

**BAGFORDJBO/11/87 (pp. 57–8):**

\TranscriptionStart

[**page 57:**]

[…]

[*centered:*] 22 November 1704.

[…]

[**page 58:**]

[…]

 A Paper was Read from M^r^ Bagford concerning Printing. He was Thanked.

\TranscriptionStop

**JBO/11/113 (pp. 72–3):**

\TranscriptionStart

[**page 72:**]

[…]

[*centered:*] 6 June 1705.

[…]

 M^r^ Lowthorp presented the Society with his Abridgement of the\
Philosophical Transactions, for which he was Thanked.

[…]

\TranscriptionStop

**JBO/11/114–123 (pp. 73–77):** Nothing involving Lowthorp reported 13 June 1705, 20 June, 27 June, 3 July, 11 July, 24 Oct, 31 Oct, 7 Nov, 14 Nov, 21 Nov 1705.

**JBO/11/124 (p. 77a):**

\TranscriptionStart

[*centered:*] November. 30. 1705.

[…]

 M^r^ Foley. D^r^ Tyſon. M^r^ Southwek. M^r^ Wren.\
D^r^ Cockburn. M^r^ Lowthorpe. & D^r^ Mead. were [*‘ſworne’ erased at end of line*]\
sworne of the Councill.

[…]

\TranscriptionStop

**JBO/11/125–132 (pp. 77a–81):** 5 Dec 1705, 12 Dec, 19 Dec, 4 Jan
1705/6, 9 Jan, 16 Jan, 23 Jan, 6 Feb: Nothing involving Lowthorp
reported.

## Description of manuscript proposal

\BiblHangingIndentsStart

**Volume:** Within volume 22i (UkLoRS CLP/22i/59) of the extensive
Classified Papers series with individual papers glued to leaves in a
modern scrapbook; quarter pale yellow (Centroid 89) pigskin on
moderate yellow (Centroid 87) buckram boards, flaps at top and fore
edge, dark yellowish brown (Centroid 78) ties, pale yellow (Centroid
89) endpapers and guards with individual items sewn or glued to the
guards.  The first item is a manuscript catalog signed by A. H. Church
(TODO, biographical details; check notes on the watermark for this
paper too) 27 February 1902 on pen-ruled paper watermarked with a
horn.  Remnants of gluing on the inner edge of items matches the
previous guard book; items numbered in pencil matching the manuscript
index and matching red pencil on the modern guards; leaves of items
foliated as a whole excluding Church’s catalog; many items repaired
with modern conservator’s tissue, previously folded with creases and
soiling patters on rear, or with manuscript docketing.

Church’s manuscript catalog lists this as number 59 by
“Lowthrop. J. M.D., F.R.S.” and the “Title or Subject” as “Proposals
as to printing the Philosophical Transactions + Collections to the End
of 1700 abridged and disposed under general heads, in 3 vols. 3
pp. dated April 12. 1703 read May 5, 1703(?).”  The queried 1703
originally read 1702, with a 3 overwriting the 2.

Items have penciled crosses by Thomas Stack in lower left margin that
he checked during … (TODO, get this out of your notes)

(TODO note current Royal Society catalog)

(TODO write up, evidence points to this arrangement originating in the 1720s, see
CMO/3/84,182)

**Proposal:** 2^o^: χ^2^; 4 pages, [*unnumbered, pp. 1-4*]. On paper
watermarked with the London coat of arms (Churchill 239–244) and
countermarked “C T” (stock, 13 \| 17 \| 24 \| 25 \| 2 [ 23 \| 23 ] 1
\| 25 \| 24 \| 24 \| 24 \| 25 \| 21 [ 4 \| 6 ] 15 [ 3 \| 6 ] 19 \| 24
\| 23 \| 16); 301 × 194 mm, 33 lines (p. 2), in cursive, with heavier
strokes at the ends of words and on descenders, p. 2 continues across
the gutter onto p.3 at the ends of some lines, suggesting this was
written open to both pages.  Dated ‘May 5: 1702.’  on p. 4 and
‘Apr. 12. 1703’ on p. 1.  The inner margin of p. 1 is discolored with
its, presumably, previous pasting in a guard book.  Inked fingerprints
on p.1, possibly oil-based printers’ ink by the spread and
show-through.  P. 4 is faintly discolored into four folded panels, 81
× 194 mm.  In the lower left of p.1 a penciled cross, probably
Dr. Stacks, indicates this item has been counted.  Modern penciled
numbers ‘175’ on p. 1 and ‘176’ on p. 3 refer to the foliation of the
whole volume.

\BiblHangingIndentsStop

## Transcription of manuscript proposal

\TranscriptionStart

[*Next seven lines, centered:*] Apr. 12. 1703

Propoſalls for Printing\
The Philosophicall\
Transactions and Collections [*pen skip after ‘Transactions’*]\
To the End of the Year j700.\
Abridg’d and Diſpoſ\kern1pt{}’d under Generall Heads\
in **III.** Volumes.

By J. Lowthorp. **M.A.**&**F.R.S.** [*pen skip before ‘M.A.’*]

The Philoſophicall Transactions are well known to be an\
Excellent Regiſter of many Valuable Experiments, and a Cu⹀\
⹀rious Collection of such Diſcourſes , as being too ſhort to be
Printed\
alone, were in great Danger of being loſt: And have been\
publiſ\kern1pt{}hed almoſt every Month (some few Years of Interruption\
excepted) since the begining of the Year i665, by the particular\
Encouragement of the Royall Society .  But the generall Appro\
⹀bation they have met with, both at home and abroad , has long\
since made them so scarce , that a compleat Set is not to be pro⹀\
⹀cur’d without much trouble ,and not to be purchaſ\kern1pt{}’d under 15.£.\
And yet the vaſt Bulk of 22. thin Volumes in 4to. to which\
they are swell’d, and that want of Connection which could —\
not be avoided in this manner of Communicating such a Mul⹀\
⹀titude of independent Papers , make it altogether unadviſeable\
to Reprint them in their preſent Form .  The Author therefore\
of this Abridgement , conceives it may be some Service to the\
Publique , to Contract them into a leſs Compaſs; and to Range\
and Diſpoſe them in a better order, under Generall Heads:\
In the Execution whereof, He (hath for the moſt part ) confin’d\
himſelf to theſe Rules.

 i. He has divided the whole Work into 3. Volumes; The First\
contains all the Mathematicall Papers; The Second the —\
Phyſiologicall; and the First part of the 3d thoſe that\
are Medicall, the later part of it being reſerved for the [**Page 2:**]\
the Ph\rlap{\bf i}{y}lologicall and such Miſcellaneous Papers as cannot [*‘Ph\rlap{\bf i}{y}lologicall’ corrected from ‘Phylologicall’ by overwriting*]\
properly be reduc’d to any of the former Claſſes.  Each of theſe\
Volumes are subdivided into Chapters , and Generall Heads ,\
under which each Paper is inſerted in the beſt Order their —\
great Variety will admit of.

 2. He does not think it neceſſary in this Abridgement to\
retain the Accounts of Books which are added to the End of —\
each of the Tranſactions , since the Books themſelves are either\
long ago common in the Hands of such Readers as have Occaſion [*tail on ‘n’ extends to next page*]\
to uſe them , or so near loſt that few will be at the trouble to\
search for them . Yet that the Inquiſitive may have some Notice\
of them , He ha\rlap{\bfa s}{\kern2pt{}\tfx{}d} added to the End of each Chapter a
Catalogue [*‘ha\rlap{\bfa s}{\kern2pt{}\tfx{}d}’ corrected from ‘had’ by overwriting*]\
of such of them as Treat profeſtly and principally of that Subject [*blot over ‘at’ in ‘that’*]\
of that Chapter.

 3. The Additions , Emendations, and Refutations , of Books He does\
not think neceſſary to be retained here, becauſse such remarks can\
be only serviceable to thoſe who have the Books .  And therefore He\
contents himſelf with Directing his Readers to the Tranſac\rlap{\bfa t}{c}ions [*pen skip over ‘e’ in ‘Directing’; ‘Tranſac\rlap{\bfa t}{c}ions’ corrected from ‘Tranſaccions’ by overwriting*]\
themſelves for such Particulars .

 4. There are many Papers which were very Curious at the times [*tail of ‘s’ extends to next page*]\
of their Publication\rlap{\bfa (}{,}as the Calculations of Eclipſes , Lunar [*opening parenthesis overwrites comma*]\
Appulſes , Satellite Eclipſes , Tide Tables , and some others of that\
kind\rlap{\bfa )}{,} which are now uſeleſs .  Yet to do Juſtice to their Authors [*closing parenthesis overwrites comma; tail of ‘s’ extends to next page*]\
the Titles are inſerted in their proper places.

 5. There are another sort of Papers here Omitted which are [*dot over first ‘h’ in ‘which’; tail of ‘e’ extends to next page*]\
alſo very Curious viz. Inquiries and Directions , upon severall\
Subjects , and for places seldom viſited , drawn up and diſperſed\
by Order of the Royall Society · But there being very few who\
have any Oportunity of satiſfying either themſelves or others\
in such Particulars , The Author thinks that the Anſwers\
already given to many of the Enquiries , and the other Diſcourſes\
upon the same or the like Subjects here retained , will sufficiently\
supply this want . And therefore he chuſes to recommend it to [**Page
3:**]\
the Gentlemen of the Royall Society themſelves to Reprint thoſe\
Ingenious Papers, and to put them into such Hands as may probably\
have an Oportunity to make them suitable Returns , rather then to
swell\
theſe Volumes which are deſigned for more \rlap{g}{\bf G}enerall uſe. In the [*‘\rlap{g}{\bf G}enerall’ corrected from ‘generall’ by overwriting*]\
mean time He has not fail’d to inſert the Titles of theſe ,
as well [*pen skip between ‘has’ and ‘not’*]\
as of all other Papers Omitted, at the end of each Chapter to which\
they properly belong.

 6. He has been very Carefull , in Abridging the remaining Papers,\
to preſerve the Sence of their Authors entire, and to relate the\
Hiſtories of Facts with all the Materiall Circumſtances and uſefull\
Reaſonings thereon . But many of thoſe Papers being writ in the\
form of Letters and often interſperſed with perſonall Addreſſes;
[*second d in ‘Addreſſes’ inserted, Ad‸^d^reſſes*]\
And others of them being frequently interrupted with long Excurſi⹀\
⹀ons and Citations of Books, which for the moſt part rather Amuſe\
the Reader than either Inſtruct him or Illuſtrate the Subject; This\
Author humbly hopes that none will think themſelves Injured\
whilſt He takes a modeſt Liberty of Pruning such Luxuriances .\

 7. To cloſe in some Meaſure one of the Chaſmes in the\
Tranſactions , viz. between the Years i678 and i683. He hath inſe\rlap{—}{rt} [*dash overwrites ‘rt’ in inſe\rlap{—}{rt}’*]\
⹀ed , in their proper places , Dr Hook’s Philoſophicall Collections,\
being a Work of the same kind and Publiſh’d by him during that\
Intervall of time.

 8. To make the whole Work more compleat He will add such Ind⹀\
⹀dexes both of Authors and Subjects as Shall be thought of any [*‘⹀dexes’ corrected by canceling an apostrophe from ‘⹀dexe’s’*]\
uſe or Advantage to the Readers.


In short the Author of this Abridgement profeſſes that\
He hath every where deſign’d (according to the beſt of his Under⹀\
⹀ſtanding) to Omit nothing that would be of any Service to the
greateſt\
part of the Readers , and to Burthen them with nothing that could\
be spar’d without manifeſt Prejudice to the Sence of the severall\
Ingenious Authors : And that he hath carefully endeavoured that\
none of their Papers ſ\kern1pt{}hould suffer by paſſing through his Hands.

\TranscriptionStop

## Term Catalogues

TODO

v.3 p. 363 (notice of available proposals, Trinity Term, June 1703; not Easter Term, May 1703) under “Advertisements”: an advertisement for an advertisement.

v.3 p. 460: (May 1705, Easter Term) Under “Advertisements” the publisher notes they’ve sold out of subscriptions “This is to give notice, That Mr. **Lowthorp**’s Abridgment of the Philosophical Transactions is now ready to be delivered to Subscribers; they making their second payment (30s.), according to the Proposals.  N.B. Whereas the Number printed is not sufficient to answer the Demands of all who have desired to be admitted as Subscribers, those Gentlemen who have already subscribed, and do not take up their Books before the 22. of *June* will be excluded from that Advantage.  To those who are not Subscribers, it will not be sold under 50s. in Sheets.  The Undertakers are **T. Bennet** at the Half Moon, **R. Knaplock** at the Angel and Crown, and **R. Wilkin** in the King’s Head, St. *Paul’s* Churchyard.” 

v.3 p. 468 (Miscellanies for sale June 1705)

## Description of volume

Modern binding with oversewn gatherings, originally this was described
as a portfolio (TODO date and citation), but the watermarked paper has
a wide, non-wavy, serpent alone with two eyes; a motif that seems not
to have survived into the 18c (cf. Briquet 13758–13800 (16c), Heawood
3749–3766 (1522-1702)); other paper seems to come from a double mould
and has a horizontal chainline and occasional countermark of ‘H’; John
Bagford’s first inventory is on such paper, suggesting the leaves are
contemporary with whatever form the portfolio took, perhaps the
pasting as well; in is transferred from item 22 to the preceding
unnumbered leaf; item 4 had been glued to the preceding leaf (residue
visible), but is now mounted on a modern guard, the interleaving
leaves are clearly gathered at the spine, suggesting whatever form
this once had was also gathered, perhaps a series of bundles?  Or
perhaps these were inserted in a contemporary blank ledger?

## Portfolio leaves (ignoring insertions)

1. [*Bagford endpaper*]
2. Item 1 on the recto, item 2 on the verso
3. Item 3 on the recto, item 4 on the verso
4. 5 recto, 6–7 verso
5. 8–9, 10–11
6. 12–13, 14
7. 19–20, 21
8. [*blank*], 24–5
9. [*blank*], 28
10. 31–33, 34
11. 37, 38–39
12. [*blank*], 42
13. [*after 43, type 2, blank with vertical chainlines, lighter colored paper*]
14. 44, [*blank, type 1*]
15. 45, [*blank*]
16. 46, [*blank*]
17. [*blank with vertical chainlines, lighter colored paper: paper type 2*], 47
18. 48, [*blank*]
19. 49, 50 [*vertical chainlines, lighter, possibly conjugate with next*]
20. [*blank with vertical chainlines, lighter colored paper, possibly conjugate with the preceding*], 53
21. 54, “55” and “53” numbering but no item, glue residue [*paper type 2*]
22. [*after item 56, blank with vertical chainlines, light. paper type 2*]
23. [*after item 64, type 1 paper, blank*]
24. 66, [*blank, type 1 paper*]
25. [*blank, type 2 paper*], 70-77
26. 78, [*blank, type 2 paper*]
27. [*after item 82, type 2 paper, blank*]
28. [*after item 86, type 2 paper, blank*]
29. [*after item 87, type 2 paper, blank*]
30. [*after item 88, type 1 paper, blank with some transfer of the opposite side of the previous page, perhaps by oil migration?*]
31. 89, [*blank type 1*]
32. [*after item 90, type 1, blank*]
33. [*after item 91, bis*]
34. [*after item 95, type 2, blank*]
35. [*after item 97, type 2, blank*]
36. [*after item 102, type 1, blank*]
37. [*after preceding blank, type 1, blank*]
38. [*after item 104, bis*]
39. 106\*, [*blank, type 1, after item 106*]
40. [*after item 109, type 1, blank*]
41. [*after item 110, type 1, blank*]
42. [*after item 111, type 1, blank*], 112 glued
43. 113, [*blank, type 1*]
44. [*after item 121, type 1, blank*]
45. [*after item 123, type 1, blank*]
46. [*after item 125, type 1, blank*], 126 glued on inner edge of recto to verso
47. [*after item 128, type 1, blank*], 129 glued
48. [*after item 321, type 1*] 133–4 glued, [*blank*]
49. 135, [*blank*]
50. [*after item 146, type 1, blank*], a little ink transfer from item 147
51. [*after item 148, type 1, blank*]
52. [*after item 150, type 1, blank*]
53. [*after item 156, type 1, blank*]
54. [*after item 167, type 1, blank*]
55. [*after item 170, type 2*] some ink transfer not from the preceding, 171 pasted
56. [*after item 172, type 2, blank*]
57. [*after item 180, type 2, blank*]
58. [*type 2:*] 183, 184 with item 185 inserted loosely behind it
59. [*type 2:*] 186, [*blank*]
60. [*type 2, after item 202:*] “No 202 \| Greenvile Colins” in pencil, 203
61. [*after preceding leaf, type 2, blank*], “204 \| [*rightwards arrow*]” in pencil
62. [*type 1:*] item 216, [*blank*]
63. [*type 1:*] item 248, “This Volume contains 248 sheets \| + broadsides  + . \| Feb. 20. 1807 FM” in pencil

There are sixty-three blank leaves into which these were pasted and inserted, thus, some have been inserted or removed.

## Description of printed proposal

\BiblHangingIndentsStart

4^o^: A^2^ [A1 signed]; 2 leaves, pp. 1–4; 000004: b1=b2 A1 o

**Uk Harley 5947, 206–7:** 239 × 182 mm on unwatermarked paper
matching the large-paper abridgment’s size 241 × 190 mm (Uk 740 h. 5:
v. 3, p. 1); with a typographical layout lacking marginal notes at
169–187 × 116 (144–166 × 116) mm (p.2 smaller than p.1) similar to the
abridgment’s larger and more densely set 198 × 135 (188 × 116) (Uk 740
h. 5: v. 3, p. 315); headings measure slightly smaller at 7/5/3
(*Prospectus* p. 3) against 9/6/4 (*Abridgment* v. 3 p. 1);
subheadings 6/4/2 and body 4/3/2 match exactly (same pages); both have
page catchwords, and this copy of the *Prospectus* is printed inner
forme first on the felt side of the paper; the paper is unwatermarked
but comes from stock with chainline distances, 12 \| 24 \| 23 \| 24 \|
24 \| 25 \| 21 \| 25 \| 25 \| 23 \| 12+; previously folded in
uneven thirds and then in half. [*transcribed and measured*]

**Image of IeDuNL P 54:** 203 × 145 mm (measured by librarian) on
unwatermarked paper bound in a pamphlet volume; penciled on p.1
(A1^r^) ‘29’ in upper right, ‘John Lowthorp’ centered below the page
number, circular ownership stamp of ‘NATIONAL LIBRARY OF IRELAND’ in
upper left, at the foot ‘London, 1703’, and *LOWTHORP,* underlined
twice; on p. 4 (A2^v^) ink stain at foot of page passing through
previous three, circular ownership stamp of ‘NATIONAL LIBRARY OF
IRELAND’ on middle left, oval ownership stamp of ‘ROYAL DUBLIN
SOCIETY’ in deep purplish blue (Centroid 197) on middle centered,
penciled ‘6‘ and ‘206’ in lower right; previously folded in uneven
thirds and then in half with soiling indicating it was stored with
p. 4 (A2^v^) outward. [*checked against transcription*]

**Image of UkOxU-SJ Hh.9.32(11):** two penciled lines to the right of
‘ABRIDG’D’ on p. 1 (A1^r^); mountain crease on p. 1 (A1^r^) continuing
to p. 3 (A2^r^) resulting in valley creases on verso; previously
folded in uneven thirds and then in half with soiling indicating it
was stored with p. 4 (A2^v^) outward. [*checked against transcription*]

**Image of UkLiU SPEC Knows. pamph 510(6) Checked:** bound in pamphlet volume
with numeration ‘6’ in upper right p.1, visible backmark across upper
margin and trimmed to fit the volume, but without visible creases from
holding. [*checked against transcription*]

\BiblHangingIndentsStop

This item is mounted on a contemporary-looking paper stub within a
larger volume (Uk Harley 5946) that numbers individual pasted items
and folios. This item is foliated 206–7 in modern pencil by a
librarian at the British Library around 1804 who signed the lower free
endpaper.  Bagford or his emulator inserted the proposals between the
leaves of the volume in apparent themes:

1. Antiquarian, geographic, and historical, 15–89

2. Legal and government, 91–98

2. Metals, 99–102

3. Natural history of plants and herbals, 103–109

3. Travel and geography, 110–125

3. Dictionaries, 127–144

4. Mathematics and astronomy, folios 145 to 161

4. Royal Society, 162–165

4. Ogilby’s and other surveys, 166–186

4. Popular reading on technical subjects, natural philosophy, and maps, 187–216

5. Oxford specific, 217–228

6. Pamphlets, fundraising, miscellanies, 229–235

7. Translations of Latin classics, 236–239

8. Evangelical pursuits, 240–247

The pasted handbills, advertisements, and title pages are inserted *ad
libitum* on what seem to be the original blank pages of the volume
between the thematically arranged proposals.

This one falls toward the end of a series of proposals for
popularizations of natural philosophy, interspersed with catalogs from
instrument makers of the late seventeenth century.  On the same guard
is mounted a proposal that includes a paraphrase of “All the Epistles
of St. Paul’s text” and it is followed by proposals for other
philological works printed in Oxford.  Page 4 has a soiling in a
horizontal line that doesn’t appear to have been a fold, and a
possible vertical water stain.  The inner margin is pierced by nine
holes typical for gatherings side-stiched with other pamphlets, which
must be from a previous volume.  The lower edge preserves the paper’s
deckle and the outer edge has a minor drag defect typical of the edge
of a sheet.  The appearance suggests it hasn’t been trimmed much.

Melvin H. Wolf edited a catalogue of the Bagford Collection in 1974
that was the first catalogue produced by computer.[@wolf74:cat [vii]]
He indexes items by the penciled number, which A.W. Pollard also uses
in his earlier survey.[@pollard02:bagf] Neither inventory includes
every item and the list below is—as far as I know—the only complete
list written.

## List of proposals in Bagford’s portfolio

Catalog 1759, portfolio, later bound.  Hand list on the leaf preceding the item numbered 1 in pencil and in Wolf’s inventory.  Wolf 49 appears to be an earlier hand list without numbering.

\TranscriptionStart

[*next three lines to the right:*]\
This vollom Contans ſevirall\
propoſalles for y^e^ printing of\
Books for Sevirall facultys

[**in two columns, item 3 even with item 44:**]

1 Athenaoxeneis\
2 Somners\
3: Keppe of y^e^ Monuments of Weſtmenſter\
4 Camdens Britania\
5 y^e^ Ant of petterburg\
6 Aubery\
7 Sames\
8. D^r^ Plotes 2\
9. D^r^ Beneir Somersets\
10 M^r^ Tanner\
11: Harfordshire 2\
12: D^r^ Leiyh\
13 Devonshire [*preceded by a mark*]\
14 Anglia Sacra: 2\
15 Hiſtory of Britian\
16 M^r^ Tyrrell:\
17: y^e^ Kings of Englands\
18: S^r^ Will Temple\
19 y^e^ peney proithe[*?*]\
20: D^r^ Hickes\
21 Law Guides.\
22 Abridgment of y^e^ Statues\
23 Ruſhworth.\
24 Craigs Judg\
25 Manilius Spere\
26 Gallileus\
27. M^r^ Collnes\
28 Arethmetick Tab\
29 Dubell Gramer\
30 D^r^ Grew Muſe\
31: Bookſellers [*preceded by a mark*]\
32 Algebra 2\
33 Blome.\
34: philoſophicall Tran\
35 S^r^ Jo: pettus\
36 M^r^ Ray\
37 Herball\
38: D^r^ Moriſon\
39 pittes Atlas\
40 Navall affars\
41 Cardens travales\
42: Cabala\
43 Mapes [**second column:**]\
44 Geographi\
45 Mappes Modern\
46 Heyllen:\
47: Voayes 🙰 Travels\
48. D[*itto?*]\
49 Flavell\
50 Monthey Mercury

\TranscriptionStop

We can correlate the Wolf numbering with the Bagford numbering with
the items in the volume, etc. etc.  Note that this is a inventory of
proposals for printing and that the rest of the materials are merely
briefly noted here.  Those with asterisks have fuller descriptions in
Melvin H. Wolf’s catalogue.  Additionally, the more fully described
prospectuses are also marked with an asterisk after the folio number
are included in Melvin H. Wolf’s catalogue.

\BiblHangingIndentsStart

[*Bagford’s index, transcribed above*] Unnumbered recto.

[*blank*] Unnumbered verso.

[*title page,* New World of Words, *1706*]  Item 1\*.

‘PROPOSALS. \| T^2^*HE\swashR{}E is now \swashP{}ubli’d a new
Treatiſe Intitled*, Meditations \| of a Divine Soul, *or, The*
Chriﬅian’*s* Guide *amidﬅ the Various Opi-* \| […] \| *and Sold by
Mr.*  John Bowden, *at the* \| Harrow, *the Corner Shop of*
Chancery-Lane *in* Fleet-Street.  1705. \| […]’  2^o^: A1; 1 leaf, pp. 1–2
(ESTC T142536) Item 2\* pasted onto the verso of preceding leaf on
inner edge, recto facing away.

[*title page for Lexicon Technicum, 1704*]  Item 3\* pasted on recto of blank.

[*handbill for medicine, 2 pp.*] Item 4.

[*manuscript list of locations and genres, with page number
references, Bagford’s hand*] Item 5 pasted to recto of blank

[*advertisements and handbills, medicine, books, etc.*] Item 6–14.

‘PROPOSALS \| For PRINTING \| *Athenæ Oxonienſes*, and *Faﬅi
Oxonienſes,*. \| […]’  2^o^: χ^2^; 2 leaves, pp. [1–4]  (ESTC R221540 [1690])  Bagford 1,
Item 15–16\*.

‘PROPOSALS | FOR \| Printing by Subſcription. \| Mr. SOMNER’S
*Antiquities of* CANTERBURY, \| […]’  2^o^: χ^2^; 2 leaves, pp. [1–4],
pp. 3–4 ‘*A Specimen of the* New Edition.’  (ESTC T142529, [1702?])
Bagford 2 (also “Canterbury” on Item 49), Item 17–18\*.

[*advertisements for medicines and entertainment*] Item 19–21.

‘PROPOSALS \| For Printing the GENERAL HISTORY \| OF \|
Weﬅminﬅer-ABBY, \| With the Lively Repreſentation of all the
[Monuments,]{.smallcaps} \| […] \| By HENRY KEEPE, \| […]’  2^o^: A1; 1 leaf, pp. [1-2]  (Not
in ESTC) Bagford 3, Item 22\*.

‘By HENRY KEEPE, formerly of New-Inn-Hall in the \| Univerty of
Oxford, Gentleman-Commoner, and \| now of the Inner-Temple London,
Eſq;. \| […] \| […] fini my larger Volume, *Of the Ori-* \| *ginal of
thi\ligis{} Monaﬅry*, &c. […] \| […]’  2^o^: χ1; 1 leaf, pp. [1–2]
(ESTC R179312, [1683]) Bagford 3, Item 23.

[*advertisements for medicine and entertainment*] Item 24–5 pasted to verso of blank.

‘*Enquiries* for Information towards the Completing a moﬅ Ela- \|
borate and Exa *Hiﬅory of Yor\swashk{}shire* ; together with the Au-
\| thor’s *Degn* ; and *Propoſals* for the Publication of ſo uſefull
\| and dereable a Work. \| […]’  2^o^; A^2^; 2 leaves, pp. 1–4  (Not in ESTC)  Item 26–7.

[*advertisement for medicine*] Item 28, pasted to verso of blank.

‘*Enquiries for Information towards the Iuﬅrating and* \| *Compleating the Antiquities and Natural Hiﬅory of* \| York-ire. \| *By* Nathaniel Johnﬅon, *\swashD{}r. in Phyck.* \| […]’  2^o^: A^4^; 2 leaves, pp. 1–4  (ESTC R225210)  Item 29–30\*.

[*advertisements for medicine*] Items 31–3 pasted to recto of blank, 34 pasted to verso.

‘[*two rules*] \| PROPOSALS \| FOR \| Printing by Subſcription, \|
\dontleavehmode{\fraktur Cambden’s Britannia, Engli.} \| *Newly
\swashT{}ranated with Large A\swashD{}\swashD{}I\swashT{}IONS.* \|
[…]’  2^o^: χ^2^; 2 leaves, pp. [1–4] p. 3: [*specimen:*] ‘[*two
rules*] \| BRITAIN. \| […]’, p. 4 ‘THE \| SUBSCRIBERS’ (ESTC R176553,
[1693?])  Bagford 4, Item 35–36\*.

[*advertisements for curiosities and medicines*] Item 37 recto, 38–39 verso of blank.

‘PROPOSALS \| For Printing the [History]{.smallcaps} of the
[Church]{.smallcaps} of \| [Peterburgh]{.smallcaps} by *Subſcription.*
\| *Auguﬅ* 19. 1684. \| […]’  2^o^: χ1; 1 leaf, pp. [1–2] p. 1
[*t.p. specimen:*] ‘The \| HISTORY \| […]’, p. 2 ‘PROPOSALS \| […]’.
(Not in ESTC) Bagford 5, Item 40\*.

[*leaf B1 (pp. 1-2) of ESTC R5107, which the preceding proposes*]  Bagford 5, Item 41.

‘PROPOSALS \| For Printing by SUBSCRIPTION, \| *An ATTEMPT towards Recovering an Account of the Numbers and* \| *Suﬀerings of the* Clergy *of the* Church *of* England, Heads *of* Colleges, \| Fellows, Scholars, […] \| […] *Occaoned by the* Ninth Chapter *of Mr.* Calamy’*s* \| Abridgment of Mr. *Baxter*’s Life and Hiﬅory […] \| T^4^HIS is the Account of the *Suﬀerings* of the *Loyal Clergy*, which hath been ſo \| […]’  2^o^: χ1; 1 leaf, pp. [1–2]  (ESTC T142523, [1714?])  Item 42\* pasted by the inner edge on the verso of a blank facing outward.

‘PROPOSALS \| For PRINTING \| \dontleavehmode{\fraktur Monumenta Britannica,} \| Written by Mr. *\swashJ{}OHN A\swashU{}\swashB{}RE\swashY{}*, Fel- \| low of the [Royal Society.]{.smallcaps} \| […]’  2^o^: χ1; 1 leaf pp. [1-2] (2 as ‘25’)  p. 2: [*specimen:*] ‘Templa Druidum \| […]’.  (ESTC R38552 [1690?])  Bagford 6, Item 43\*.

‘PROPOSALS \| Concerning the Printing of a \| [Chronological History]{.smallcaps} \| OF \| ENGLAND. \| […] \| […] The Author thereof, *\swashA{}ylett Samms* of the *Inner-\swashT{}emple Gent*. maketh \| theſe *Propoſals*. \| […] \| Anno Domini 1674.’  2^o^?: χ1; 1 leaf, pp. [1] [2] (*blank*)  (Not in ESTC, but similar to ESTC R183220 [1677])  Bagford 7, Item  44\* pasted to recto of blank.

‘PROPOSALS \| Concerning the Printing of a \| CHRONOLOGICAL HISTORY \| OF \| ENGLAND. \| […] \| […] the Author thereof, *Aylett Samms* Gent. lodging at one Mr. *\swashJ{}ohn Leet* \| […] \| […] Anno Domini 167’  2^o^: χ1; 1 leaf, pp. [1] [2] (*blank*)  (Not in ESTC)  Bagford 7, Item 45\* pasted to recto of blank.

‘PROPOSALS \| Concerning the Printing of a \| [Cronological
History]{.smallcaps} \| OF \| ENGLAND. \| […] \| thereof,
*A\swashy{}lett* S*amms* of the *Inner-\swashT{}emple* Gentleman ,
maketh theſe *Pro-* \| […] \| […] Anno Domini 167’ 2^o^: χ1; 1 leaf, pp. [1]
[2] (*blank*)  (Not in ESTC)  Bagford 7, Item 46\* pasted to recto of blank.

‘W^4^Hereas it plainly appears by the Specimens of *Oxford* and \|
*Staﬀord* Shires given by Dr. *\swashR{}O\swashB{}E\swashR{}T
\swashP{}LOT*, \| […] \| […] We whoſe Names are underwritten, do
hereby promiſe \| under our Hands to contribute the certain *Summs* to
our *Names* aﬃx’d \| ( as was done in the aforeſaid *Counties* already
ﬁni’d ) toward the \| travelling over and writing the *Natural Hiﬅory
of* [London]{.smallcaps} *and* \| [Middlesex,]{.smallcaps} the next
*Counties* degn’d by the ſaid Door, in caſe \| he receive a
proportionable Encouragement […]’  8^o^?: χ1; 1 leaf, pp. [1] [2] (*blank*)
(Not in ESTC) Bagford 8, Item 47\* pasted to verso of blank.

‘*\swashD{}ecember* 21ﬅ.  1704. \| Propoſals for Reprinting
Dr. *Plot’s Na-* \| *tural Hiﬅory of Oxford-ire*, with large \|
Additions throughout, left Perfeed \| for the Preſs under the Door’s
own \| Hand, containing, \| […]’  2^o^: χ1; 1 leaf, pp. [1] [2] (*blank*)
(ESTC T42966, 1704) Bagford 8, Item  48\* pasted to a recto of a blank.

[*earlier manuscript list of proposals, Bagford*]  Item 49 pasted to the recto of a blank

‘Jan. 1. 1713 \| T^4^HERE are now printing at the Theater in OXFORD
Four Chronological Tables ; in \| which are contain’d not only all the
cheif things of ſacred Hiﬅory from the Creation of the \| World ’till
[Christ]{.smallcaps}’s time, but alſo all other the moﬅ remarkable
things of thoſe times, \| that are recorded in any of the Antient
Writers now extant. \| […] \| All theſe Tables are compiling by
*Benjamin Marall* M. A. late Student of *Chriﬅ Church* in \|
*Oxford*, and one of his Lordip’s domeﬅick Chaplains. \| […]’  2^o^?: χ1; 1
leaf, pp. [1] [2] (*blank*) (ESTC T142519) Item 50\* pasted to verso of a blank.

‘*ATLAS GEOGRAPHUS:* \| OR, A COMPLEAT \| \dontleavehmode{\fraktur
Syﬅem of Geography,} \| [Ancient]{.smallcaps} and
[Modern]{.smallcaps}. \| […] \| lateﬅ Obſervations, by *Herman Moll*,
Geogra- \| […] \| \dontleavehmode{\fraktur To be Publied
Monthly.} \| [*rule*] \| This 25th for *May*, 1710, ﬁnies the Hiﬅory
of the *Roman* Conſuls, con- \| […]’ 4^o^: χ^2^; 2 leaves, pp. [1–4]  (ESTC T142521)  Bagford 43?, Item 51–2\*.

[*advertisement for entertainment and medicines*]  Item 53 pasted to verso of blank, item 54 pasted to recto of blank.

‘A^14^ [Catalogve]{.smallcaps} and true Note of the *Names* of ſuch
*Perſons* , which (vpon good \| liking they haue to the
*Wor\swashk{}e*, being a great helpe to *Memorie*) haue receiued the
*Etymologicall* [Dictio-]{.smallcaps} \| [narie]{.smallcaps} of
XI. *Languages*, viz. *Engli*, *\swashB{}riti* or *Welch*, *French*,
*Italian*, *Spani*, *Portugueswashz{}*, *High-Dutch*, \| […] \| […]
Mr. [Min-]{.smallcaps} \| [shey]{.smallcaps} the *Author* and
*Publier* of the ſame in *Print* ; […]’  2^o^: χ1; 1 leaf, pp. [1–2]  (ESTC S123153
[between 1616–21])  Item 55.

‘[*two rules*] \| A \| [Scheme]{.smallcaps} of the intended
[History]{.smallcaps} \| OF \| WILTSHIRE. \| […]’  χ1; 1 leaf, pp. [1–2]  (ESTC
T142517)  Item 56.

‘PROPOSALS \| For [Printing]{.smallcaps} by SUBSCRIPTION, \| THE \|
HISTORY \| OF THE \| County of KENT. \| [*rule*] \| By
*\swashJ{}OH\swashN{} HARRIS,* D. D. \| […]’  2^o^: χ1; 1 leaf, pp. [1–2], p. 2 ‘London,
*March* 25. 1713.’  (ESTC T142516) Item 57.

‘PROPOSALS for Printing a Book entituled, \|
\dontleavehmode{\fraktur The hiﬅory and Antiquities of}
Hertfordire. \| Written by Sir [Henry Chauncy,]{.smallcaps} K^t^,
Serjeant at Law.  Containing, \| […]’  2^o^: A^2^ [A1 signed]; 2 leaves, pp. 1–4.  (ESTC
R213936) Bagford 11, Item 58–59.

‘To THE \| REVEREND \| THE \| CLERGY \| OF \| The City of *LONDON*, \|
And the Suburbs. \| *Gentlemen*, \| A^3^ New Edition of *Stow*’s
[Survey]{.smallcaps} of *LONDON*, being \| now preparing, and in good
forwardneſs for the Preſs, \| may receive a conderable Improvement
from your \| helping hands. \| […]’; 2^o^: χ^2^; 2 leaves, pp. 1–2
[3–4] (*blank*) “Dec. 20. 1702.” p. 2; “dupl: D.190.” manuscript note
in pen on p. 1; (ESTC T142515) Item 60.

‘[*rule*] \| PROPOSALS \| For the Printing of \| *\swashT{}he Hiﬅory
and Antiquities of* HERTFORDSHIRE. \| […]’  2^o^: χ^2^; 2 leaves, pp. [1–2] 3 [4], p. 1
[*t.p. specimen*], p. 2 [*blank*], p. 3 [*specimen*], p. 4
[*proposal*].  (Not in ESTC, similar to R213936)  Bagford 11, Item
61.64\*.

[*pp. 139 (U2)–142 (U3) of work the preceding proposed*] Bagford 11,
Item 62–3 inserted into item 61, 64 but previously folded slightly
differently.

‘[*two rules*] \| PROPOSALS \| FOR \| Printing, A Natural HISTORY \|
OF \| *LANCASHIRE*, *CHESHIRE*, \| AND THE \| *PEAK* in
*Derbyire*. \| [*rule*] \| By *CHARLES LEIGH*, [Doctor]{.smallcaps}
of [Physick.]{.smallcaps} \| […]’  horizontal chainlines and size
suggest double-mould paper; 2^o^: χ1; 1 leaf, pp. 1–2; p. 2 “The
Undertaking about Twelve Years ago was begun by a Recommendation from
the University of Oxford, and is now compleated ; will be about Five
Hundred Pages in Folio …”  (Not in ESTC, but the work is 1700) Bagford
12, Item 65.

[*advertisements for medicine masquerading as poetry*] Item 66–69
glued to the recto of a blank, 70–77\* glued to the verso of the next
blank with first leaf facing inward.

‘PROPOSALS \| Made for an \| Aual Survey of the Country of
[Devon]{.smallcaps}. \| […] \| [*rule*] \| London, Printed in the year
1700.’  horizontal chainlines and size suggest double-mould paper
2^o^: χ1; 1 leaf, pp. [1] [2] (*blank*) (Not in ESTC) Bagford 13, Item
78.

‘[*rule*] \| [London,]{.smallcaps} *March* 25. 1691. \| [*rule*] \|
PROPOSALS \| FOR \| SUBSCRIPTION \| To a BOOK in the Preſs, Intituled,
\| [*rule*] \| ANGLIA SACRA: \| SIVE \| […] \| PARS PRIMA. \| […] \|
*EDITORE,* \| [Henrico Whartono,]{.smallcaps}
A. M. \swashR{}. \swashR{}. *in Chriﬅo \swashP{}atri ac \swashD{}om.*
\| […]’  2^o^: χ^2^; 2 leaves, pp. [1–4] (Not in ESTC) Bagford 14,
Item 79–80.

‘[*rule*] \| [London,]{.smallcaps}
*\swashJ{}anuary* 25. 169$\frac{1}{2}$. \| [*rule*] \| PROPOSALS \|
For SUBSCRIPTION \| To a [Book]{.smallcaps} now ﬁnied at the Preſs,
and ready for Publication, \| INTITULED, \| [*rule*] \| ANGLIA SACRA,
\| [Pars Secunda.]{.smallcaps} \| SIVE \| […] \| [*rule*] \| The
[Undertaker]{.smallcaps} is \| [Richard Chiswel]{.smallcaps} *at the*
Roſe *and* Crown *in St* Paul’*s Church-* \| *Yard.* […]’  2^o^: χ^2^; 2 leaves, pp. [1–4]  (ESTC
R506132)  Bagford 14, Item 81–2.

[*pp. 1 (B1^r^)–4 (B2^v^) of the work proposed next*]  Bagford 15,
Item 83–4.

‘PROPOSALS \| FOR PRINTING \| A \| \dontleavehmode{\fraktur General
Hiﬅory of England} \| […] \| […] Written by *\swashJ{}AMES
\swashT{}YRRELL* of *O\swashk{}ely* \| in the Country of
*Buc\swashk{}s*, Eſquire, Author of *Bibliotheca Politica*. \| […]’
2^o^: A^2^ [A1 signed]; 2 leaves, pp. [1–4] (ESTC R182177 which is
probably a duplicate of R43723) Bagford 16, Item 85.

‘ADVERTISEMENT, \| CONCERNING \| Some *Miﬅa\swashk{}es* in the late
Propoſals for Printing a \| Pretended *Compleat Hiﬅory of England,*
&c. \| […]’  A2^r^ (3)–A3^v^ (4) of the preceding (ESTC R172100)
Bagford 16, Item 86.

‘[*two rules*] \| PROPOSALS for PRINTING \| A Compleat \| HISTORY of
ENGLAND: \| OR, THE \| Lives of all the Kings \| TO \| His Preſent
MAJESTY. \| CONTAINING \| […]’  2^o^: χ1; 1 leaf, pp. [1–2] (ESTC
R222507) Bagford 17, Item 87.

‘PROPOSALS \| FOR PRINTING \| A \| \dontleavehmode{\fraktur
General Hiﬅory of England,} \| Purſuant to the Model laid down \| By
Sir *WILLIAM TEMPLE,* \| In his late *Introduion* to the *Hiﬅory of
E\swashN{}\swashG{}LA\swashN{}\swashD{}.* \| […]’  2^o^: χ1; 1 leaf, pp. [1–2]  (Not
in ESTC)  Bagford 18, Item 88.

‘*SIR,*        London, *February the* 6th  1695. \| I^3^ Have here
ſent you *PROPOSALS* for Printing of *The Hiﬅory of* England,
*🙰c*. The \| whole Title you may ſee underneath ; […] \| THE \|
HISTORY \| OF \| ENGLAND \| GIVING \| […] \| *LONDON,* Printed for
*\swashJ{}ohn Gwiim*, againﬅ the *Great* \| *James Tavern* in
*Biopſgate-ﬅreet*, 1696. \| […]’  2^o^: χ1; 1 leaf, pp. [1] [2]
(*blank*) (Not in ESTC) Bagford 18, Item 89\*.

‘[*two rules*] \| *SIR*, \| I^3^N obedience to your Commands, I here
ſend you a ort Account of Dr. *Hicks*’s Book, now \| in the Preſs at
*Oxford* ; \| The Title of it is, \| *Linguarum veterum
Septentrionalium Theſaur\ligus{} Grammatico-critic\ligus{} 🙰
Archæolo-* \| *gic\ligus{}.* *Accedit de* […]’  2^o^; χ1; 1 leaf,
pp. [1–2], p. 2 “Jan. the 22d. 1700.”  (ESTC R170414, attributed to
William Brome) Bagford 20, Item 90.

‘PROPOSALS \| For Printing by Subſcription, \| *HISTORIA LEGALIS*; \|
OR, \| An Hiﬅorical ACCOUNT \| OF \| Laws & Law-givers. \| […]’ χ1; 1
leaf, pp. [1–2], p. 2 [*specimen:*] ‘CHAP. XIII. \| *Of the Laws of*
England, *how divided and call’d* ; *of their* \| […]’  2^o^: χ1; 1
leaf pp. [1–2] (ESTC T142514) Bagford 21, Item 91.

[*misfolded A2^r (p. ^2^1)–A2^v^ (p. ^2^2) specimen of next, item 93*]
‘[*two rule*] \| THE \| ORIGINATION \| OF \| GOVERNMENT; \| OR, THE \|
Fundamental Laws of *ENGLAND*, \| […]’ Item 92.

‘PROPOSALS \| For [Printing]{.smallcaps} \| The Origination of
Government, \| Both *MORAL* and *\swashD{}IVINE.* \| […]’  2^o^: A^2^ [A1 signed]; 2 leaves, pp. 1–2, ^2^1–2; p. ^1^2
“Subscription will be taken in till the first Day of January next,
1705.”  (ESTC T142513)  Item 93 [92].

‘PROPOSALS \| For the Printing of a \| General Abridgment \| OF THE \|
COMMON \| And moﬅ Uſeful part of the \| \dontleavehmode{\fraktur
Statute Law of England,} \| Digeﬅed under proper Titles, with Notes
and Refe- \| rences to the whole. \| […]’ 2^o^; B^2^ [B2 signed as B]; 2 leaves, pp. [*2*] 1–2, p. 1–2
[*specimen:*] ‘[*two rules*] \| OF \| Aions in General. \| […]’ (ESTC T142512) Bagford 22, Item 94–5.

‘*London*, March 27. 1699. \| PROPOSALS \| For Subſcriptions to a
[Book]{.smallcaps} now Ready for the Preſs, \| ENTITULED, \| The
[Fourth]{.smallcaps} and [Last Part]{.smallcaps} \| OF \|
Mr. *Ruworth*’s Hiﬅorical Colleions. \| CONTAINING \| […]’  2^o^: χ^2^; 2
leaves, pp. [1–3] [4] (*blank*), p. 3 ‘Advertiſement to
*Bookſellers*.’  (Not in ESTC) Bagford 23, Item 96–7.

‘[*two rules*] \| PROPOSALS \| For Printing by way of \| SUBSCRIPTION,
\| THE CELEBRATED \| Sir Tho. Craig’s Book, \| Concerning the
[Right]{.smallcaps} of [Succession]{.smallcaps} of the \| Kingdom of
*ENGLAND*, againﬅ the Sophiſms of \| *Doleman*, &c. faithfully
tranated into *Engli*, from the \| *Latine* Manuſcript. \| […]’
2^o^: χ1; 1 leaf, pp. [1–2] (ESTC T142511) Bagford 24, Item 98.

‘OF THE \| Laws of Nature and Art, \| Concerning the Judging of \|
METALS, \| AND OF \| Fining, Reﬁning, and Seperating them. \|
Originally Writte in *High Dutch* by *La\swashz{}arus Erſker\ligus{},*
alias *Erckern*, and \| Tranſlated into *Engli* by the Care of Sir
*\swashJ{}ohn Pettus* of *Suﬀolk* K^t.^ \| and by him ſome curious
things Seleed from the moﬅ \| Eminent Writers on that Subje. \| […]
\| [*rule*] \| *London,* Printed for the Author, *Anno Dom.* 1681.’  2^o^: χ^2^; 2 leaves, pp. [1–3] [4] (*blank*) (Not in ESTC; proposal for R498473, 1683)  Bagford 35, Item 99–100.

‘The Mine-Adventure; \| OR AN \| UNDERTAKING, \| Advantagious *for the
\swashP{}ublic\swashk{} Good*, Charitable *to the \swashP{}OOR ,and*
Proﬁtable \| […] \| Propoſed *by Sir* Humphry Mackworth, […]’  2^o^: A^2^ [A1 signed]; 2 leaves, pp. 1–4
(ESTC R30042 [1698])  Item 101–102\*.

‘London, March 26. 1701. \| PROPOSALS \| For Printing \| By
SUBSCRIPTION, \| *The Third and laﬅ Volume of* \| The General Hiﬅory
of PLANTS; \| BY \| *\swashJ{}OHN RAY,* F. R. S. \| In *Folio*. \|
[…]’  2^o^: χ1; 1 leaf, pp. [1–2], p. 2 “The Undertakers are Sam. Smith and Benj. Walford,
Printers to the Royal-Society”  (possibly ESTC T142510 without the
specimen leaf)  Bagford 36, Item 103.

‘The Univerſal and Compleat HERBAL in *Oa\swashv{}o :* The like
hitherto not Extant. \| […]’ 1^o^: χ1; 1 leaf, pp. [1] [2] (*blank*)  (Not in ESTC) Bagford 37, Item
104 folds out.

‘PROPOSALS, \| For Printing a *\swashG{}eneral Hiﬅory of
\swashP{}lants*, \| […] \| *The Author \swashJ{}OHN \swashR{}AY,* M. A. *Fellow of the \swashR{}oyal Society*. \| […] \| *At a Meeting of the* Royal Society *at* Gream-College, *May* 20. 1685. \| […]’  2^o^: χ1; 1 leaf, pp. [1–2]  (Not quite the same as ESTC R232605)  Bagford 36, Item 105.

[*leaf pp. 601-2 of ESTC T142049? The preceding work proposed, possibly a specimen?*]  Bagford 36?, Item 106.  TODO, investigate this one a bit more, it’s obviously not conjugate, but might have been.

‘W^4^*Here\ligas{}* Robert Moriſon *Door of Phyc\swashk{},
Botanic\swashk{} to* \| *His Majeﬅy, and Profeor of that Science in
the \swashU{}iverty* \| *of* Oxford , *hath by His Aduous and many
Years Obſer-* \| *vation , diſco\swashv{}ered a more eae and
Scientiﬁcal Method of* \| […]’ 8^o^?: χ1; 1 leaf, pp. [1] [2] (*blank*) (TODO check ESTC) Bagford 38, Item ‘106\*’ (labeled thus, not in Wolf’s catalogue)

[*specimen of page ‘81’ of the preceding?*] 2^o^: χ1; 1 leaf pp. [1] [2] (*blank*) (TODO
check ESTC, match specimen?) Bagford 38, Item 107.

‘PROPOSALS, \| For [Printing]{.smallcaps} a Compleat \| ENGLISH \|
HERBAL. \| [*rule*] \| WRITTEN \| By *WILLIAM SALMON*, M. D. \|
[*rule*] \| […]’  2^o^: A^2^ [A1 signed]; 2 leaves, pp. [*2*] 1–2, pp. 1–2 [*specimen:*]
‘OF \| Engli Herbs and Plants. \| CHAP. I. \| […]’  (ESTC T142509)
Bagford 37, Item 108–109.

‘[*two rules*] \| A \| PROPOSAL \| For Printing and Publishing a
Compleat and General \| Hiﬅory of Naval Affairs, \| ENTITULED, \|
HISTORIA NAVALIS: \| Beginning at *Noah*’s Ark, and continued \| down
to the preſent time.  In Two Volumes Folio. \| […]’  2^o^: χ1; 1 leaf,
pp. 1–2 (Not in ESTC) Bagford 40, Item 110.

‘[*rule*] \| *\swashT{}he Preface of* Sir John Chardin *to his
\swashT{}ravels into* Pera *and* \| *the* Eaﬅ-Indies, *giving an
Account of what he degns to Write* \| *touching thoſe Countries* ;
*which \swashR{}elations will be perfely New* \| *to this
\swashP{}art of the World*. \| […]’ 2^o^: χ1; 1 leaf, pp. [1–2]  (ESTC R171152)  Bagford 41,
Item 111.

[*page 28 of “The Lord Buckehurſt to the Queen”*] Item 112 pasted to the verso of a blank.

‘PROPOSALS \| For REPRINTING of \| CABALA: \| OR, \| MYSTERIES \| OF
\| STATE and GOVERNMENT, \| IN \| Letters of Illuﬅrious Perſons, and
Great Miniﬅers of State, \| […]’  2^o^: χ1; 1 leaf, pp. [1] [2] (*blank*)  (Not in ESTC)  Bagford 42, Item
113 pasted to recto of blank.

‘[*two rules*] \| ADVERTISEMENT \| Concerning the MAPS Printed in this
\| Syﬅem of Geography. \| […]’  2^o^: A^2^ [$2 signed]; 2 leaves,
pp. 1–4; p. 1 at foot “The Binder must Place this next the Preface.”
(ESTC T142508, 1701?, Herman Moll) Bagford 43, Item 114–5.

‘PROPOSALS For \| PRINTING by SUBSCRIPTION, \| A NEW \| BODY of
GEOGRAPHY: \| Or, a Complete \| \dontleavehmode{\fraktur Deſcription
of the World.} \| […]’ 2^o^: χ1; 1 leaf, pp. [1–2] p. 2 “… to the
Undertakers, Abel Swall and Timothy Chide, at the Unicorn, at the West
End of S. Paul’s Church-Yard, London.” (Not in ESTC) Bagford 44, Item
116.

‘PROPOSALS \| FOR PUBLISHING \| *A Ne\swashw{} Sett of MAPS* \| Both
of Ancient and Modern \| *GEOGRAPHY*. \| […]’  2^o^: χ1; 1 leaf, pp. [1] [2]
(*blank*) p. 2 “Subscriptions will be taken till 10th of June next, by
Mr. Churchill at the Black Swan in Pater-noster-row, and Mr. Bennet at
the Half Moon in Saint Pauls Church-yard, London, and J. Croke at the
Theater in Oxford.”  (ESTC R504218, [1699?])  Bagford 45, Item 117.

‘[*two rules*] \| PROPOSALS \| For Printing by way of Subſcription, \|
A NEW EDITION OF \| Dr. Heylins Coſmography, \| In Four Books. \| […]’
Two single-leaf copies printed half-sheet: 2^o^: B^2^ [B1–2 signed
identically as B]; 2 leaves, p. [1] [2], ^2^[1] [2] (ESTC T142506,
[1703?])  Bagford 46, Item 118-9.

‘[CATALOGUE des CARTES]{.smallcaps} \| ET TABLES GEOGRAPHIQUES \| DE
M^R^. SANSON \| A l’Uſage DE MONSEIGNEUR LE DAUPHIN. \| Qui ſe vendent
à [Amsterdam]{.smallcaps} , chez PIERRE MORTIER , Libraire ſur le
Vygendam à Ville de Paris; Elles ſont Tré bien \| […]’  2^o^: χ^2^; 2 leaves, pp. [1–4], p. 1,4
[*blank*]  (Not in ESTC scope)  Item 120–1.

‘*London, March* 27. 1701. \| PROPOSALS \| FOR \| Printing by
Subſcription, \| A NEW \| COLLECTION \| OF \| Voyages & Travels. \|
[…]’  2^o^: χ^2^; 2 leaves, pp. [1–4] (4 as 1), p. 4 [*specimen:*] ‘The \|
TRAVELS \| […]’ (ESTC T142505) Bagford 47, Item 122–3.

‘London, March, 1701. \| Propoſals for Printing *\swashN{}avigantium
atque Itinerantium \swashB{}ibliotheca* ; \| or a Compleat Colleion
of [Voyages]{.smallcaps} and [Travels]{.smallcaps} \| in Two Volumes
in Folio, Illuﬅrated with Many Cuts \| and Maps. \| […]’  2^o^: χ^2^;
2 leaves, pp. [*2*] 245–6, pp. 245–6 [*specimen:*] ‘*The Firﬅ*
[Voyage]{.smallcaps} *to* VIRGINIA, *by the Captains* Amadas *and*
Barlow, *in a* \| *Letter from one of them to Sir* Walter Ralegh. \|
[…]’  (ESTC T142504) Bagford 48?, Item 124–5.

‘[*two rules*] \| PROPOSALS \| For a *New Edition* of the
[Fountain]{.smallcaps} \| of [Life]{.smallcaps} Opened ; or *A
Diſplaty of* \| *Chriﬅ in his Eential and Mediatorial*  \| *Glory :*
Containing 42 Sermons on va- \| rious Texts.  And alſo of
Πνευματολογια, \| or, *a Treatiſe of the Soul of Man.* \| Both in
4^o^. \| [*rule*] \| By the Late Reverend and Learned \|
Mr. *\swashJ{}OHN FLAVEL*, \| […]’  4^o^: χ1; 1 leaf, pp. 1–2.  (Not in ESTC)  Bagford
49,  Item 126 pasted to the verso of a blank facing inward.

‘W^2^EE whoſe Names are hereunto ſubſcribed, for the better in- \|
couragement of printing a Book, Entituled, *Etymologicon Lin-* \| *guæ
Anglicanæ*, compoſeb by *Stephen S\swashk{}inner* , Door of Phyc, do
\| […]’; p. 2 ‘AN \| ETYMOLOGICON \| OF THE \| Engli Tongue, \| Or
the Derivations of *Engli Words* from their proper \| Fountains, that
is, theſe twelve Languages following, *vi\swashz{}*. \| […] \|
[*rule*] \| By *STEVEN SKINNER* of *Lincoln* , Door of Phyc. \|
[*rule*] \| *LONDON,* \| Printed for *H. Brome*, *R. Clavel* , and
*B. Too\swashk{}e.* \| M DC LXIX.’  2^o^: χ^2^; 2 leaves, pp. [1–4], p. 1
[*subscription note*], p. 2 [*title*], p. 3 [*Latin title*], p.4
[*specimen:*] ‘ETYMOLOGICON \| LINGUÆ \| […]’ (ESTC R184455, [1699])
Bagford 29?, Item 127–8\*.

‘AN \| ETYMOLOGICON \| OF THE \| Engli Tongue, \| Or the Derivations of *Engli Words* from their proper \| […] \| *LONDON*, \| Printed by *\swashJ{}.G.* for *\swashJ{}ohn Croo\swashk{}*, at the Ship in S. *Paul*’s Churchyard. \| [*rule*] \| M. DC. LXVI.’  2^o^: χ^2^; 2 leaves, pp. [1–4], p. 1, 4 [*blank*], p. 2 [*title in English*], p. 4 [*title in Latin*]  (Not in ESTC TO DO, check again?)  Bagford 29?, Item 129\*.

‘PROPOSALS \| FOR PRINTING A \| Double GRAMMAR \| FOR \| *Engli*-Men
to Learn *German* or *High-\swashD{}utch.* \| AND *\swashG{}ermans* to
Learn the *Engli* Tongue. \| COMPOSED BY \| HENRY OFFELEN, Door in
Law, \| And Profeor of Seven Languages in *LONDON.* \| […]’  2^o^:
χ1; 1 leaf, pp. [1] [2] (*blank*) (ESTC R181181 [1686]) Bagford 29,
Item 130.

‘*Cantab.* 1. *April*. 1701. \| PROPOSALS \| For Printing a New
Edition of \| SUIDAS \| IN \| GREEK and LATIN, \| In Three Volumes in
Folio , publied by \| LUDOLPHUS NEOCORUS; \| AND \| Printed at the
Univerty’s Preſs in Cambridge. \| […]’ 2^o^: χ^2^; 2 leaves, pp. [1–4],
p. 3 ‘883’ [*specimen*], p. 4 [*blank*] (ESTC T142503 [1701]) Bagford
29?, Item  131-2\*.

[*revised manuscript copy for the preceding*] χ^2^; 2 leaves, pp. [1–3] [4] (*blank*)  Bagford 29?, Item 133-4\*.

[*revised manuscript copy for the same*]  χ1; 1 leaf, pp. [1] [2] (*blank*)  Bagford 29?, Item 135.

‘Propoſals for Printing \| THE GREAT \| HISTORICAL, \| GEOGRAPHICAL \|
AND \| Poetical DICTIONARY: \| BEING \| A Curious *Miſceany* of
*Sacred* and *Profane* HISTORY ; \| […]’  2^o^: A^2^ [A2 signed as
A1]; 2 leaves, pp. [1–4], p.3–4 [*specimen:*] ‘[*two rules*] \| THE
GREAT \| HISTORICAL, \| GEOGRAPHICAL \| […]’  (ESTC R506227 [1693?])
Bagford 29?, Item 136–7.  (TODO is it a specimen?)

‘PROPOSALS \| For Printing, by Subſcription, \| A \| SUPPLEMENT \| To
the Second Engli Edition of the Great \| *Hiﬅorical , Geographical ,
Genealogical ,* and *Poetical* \| DICTIONARY, \| Being a Curious
Miſcellany of Sacred and Prophane Hiﬅory, *🙰c*. \| […] \| […] By
*\swashJ{}eremy Collier* , M. A. \| […]’  2^o^: χ1; 1 leaf, pp. [1] [2] (*blank*)  (ESTC T142502)
Bagford 29?, Item 138.

‘*Lexicon Technicum Magnum* ; \| Or an UNIVERSAL \| Engli Diionary \| OF \| \dontleavehmode{\fraktur Arts and Sciences :} \| Explaining not only the *Terms of Art* but the *Arts* themſelves. \| […] \| By *\swashJ{}ohn Harris*, M. A. and *Fellow of the Royal Society*.’  2^o^: χ^2^; 2 leaves, pp. [1] 2–3 [4] (*blank*),  p.2 “November 25. 1702. Proposals for Printing an Universal English dictionary of Arts and Sciences”  (ESTC T142501)  Bagford 29?, Item 139–40.

‘[*two rules*] \| PROPOSALS \| For PRINTING \| Moneur BAYLE’s \|
Great Hiﬅorical and Critical \| DICTIONARY. \| […]’  2^o^: χ^2^; 2 leaves,
pp. 1 2 623 [624] [=4], p. 623 [*specimen:*] ‘GROTIUS ( *Hugo* ) one
of the greateﬅ Men of *Europe*, born at *Delft* the 10*th* \| […]’,
p. 624 [*blank*] (ESTC T142500 [1705?])  Bagford 29?, Item 141–2.

‘PROPOSALS \| For PRINTING by SUBSCRIPTION A \| New *Spani* and
*Engli* DICTIONARY, \| To which will be added a \| Compleat *Spani*
GRAMMAR. \| […]’  2^o^: χ^2^; 2 leaves, pp. [1–4], p. 2–3: [*specimen, three columns:*] ‘A^3^Babol, *A Poppey*. Vide Amapóla. \| Abanár, *to fan, or to winnow.* \| […]’
(ESTC T142499 [1705?])  Bagford 29?, Item 143–4.

‘[*two ornament rules*] \| The *Boo\swashk{}-ſeers* to the Reader. \|
[*rule*] \| *Courteous Reader *, \| W^6^E have here exhibited a
*Synops* of a Book of [Algebra]{.smallcaps}, \| recommended to us to
be Printed by ſome of the Members of \| the *R. SOCIETY* , whoſe
Licenſe and Approbation we \| […]’  2^o^: χ^2^; 2 leaves, pp. [1-4], p. 2
‘[*rule of ornaments*] \| THE \| ELEMENTS \| OF THAT \| MATHEMATICAL
ART \| COMMONLY CALLED \| ALGEBRA, \| Expounded in Four BOOKS. \|
[*rule*] \| By [John Kersey.]{.smallcaps} \ […]’  (Not in ESTC, but
proposed work is ESTC R35421 [1673])  Bagford 31, Item 145–6.

‘A \| PROPOSAL \| ABOUT \| PRINTING \| A Treatiſe of *Algebra*, Hiﬅorical and Praical, written in the year 1676, by \| the Reverend and learned Door *\swashJ{}ohn Wais* ( *Savilian* Profeor of *Geometry* \| in the Univerty of *Oxford*,) and then by him ſent up to Mr. *\swashJ{}ohn Coins* , to \| be communicated with others of the Royal Society ; and nce inlarged by \| him,ſo as to contain not only a Hiﬅory,but an Inﬅitution of *Algebra*,accord- \| ing to ſeveral Methods hitherto in praice:with many Additions of his own. \| […]’  2^o^: χ^2^; 2 leaves, pp. [1–4]  (Not ESTC R218356, but similar [1683])  Bagford 32, Item 147–8.

‘[*two rules*] \| A \| PROPOSAL \| About Printing a
[Treatise]{.smallcaps} of \| ALGEBRA, \| [Historical]{.smallcaps} and
[Practical]{.smallcaps} : \| *Written by the \swashR{}everend and
Learned \swashD{}r.* John Wallis ( Savilian \| […]’  2^o^: A^2^ [A1 signed]; 2
leaves, pp. [1–4]  (ESTC R218356 [1683])  Bagford 32, Item 149–50\*.

‘[Nathanael Brook]{.smallcaps} Stationer at the *Angel* in *Cornhil* ,
\| to the Reader. \| *The worthy, learned and moﬅ accomplied* Edward
Sherburn *Eſquire, Cler\swashk{} of His* \| *Majeﬅies Ordnance* ,
*having been pleaſed to convert the Ancient Aﬅronomical* Latin \|
*Poem of* Manilius *on the Sphere into a polite and elegant* Engli
*one , hath more-* \| *over interlaced the ſame with Annotations
concerning the Diſcoveries and Inventi-* \| […]’  2^o^: A^2^ [A1 signed]; 2 leaves,
pp. 1–3 [4] (*blank*)  (ESTC R10624 [1675?])  Bagford 25, Item 151.4.

[**next title inserted between χ1 and χ2 of the preceding:**]

‘Propotions concerning the Printing of two \| Volumes of the Works of
*Galilæus*, and other Famous \| Mathematicians. \| […]’  2^o^: χ^2^; 2
leaves, pp. [1-2] 432–3 (*specimen*)  (Not in ESTC, but work is ESTC
R19153, 1661)  Bagford 26, Item 152–3 inserted into Item 151.154

[*manuscript of “A Narrative of the Case of John Collins Accomptant in
relation to Employment about his Majesties affaires”*]  Bagford 27,
Item 155–6\*.

[*two leaves of* John Collins’s *“To the Reader” from ESTC R22633?*]  Bagford 27, Item 157–8.

[*another copy of Item 151.4: “Nathanael Brooks Stationer at the Angel
in Cornhil, to the Reader …”*]  Bagford 27?, Item 159–60.

‘NEW \| PROPOSALS \| FOR \| Engraving and Printing \| OF \|
ARITHMETICAL TABLES \| By way of Subſcription. \| […] \| C^4^*Laudius
Bardon* Profeor in Mathematicks, and Teacher of them in
Mr. *Foubert*’s Aca- \| demy, […] \| […]’  2^o^: χ^2^; 2 leaves, pp. [1–2]
[3–4] (*blank*), p. 2 “London, the 27th of July, 1686.”  (Not in ESTC)
Bagford 28, Item 157.

‘W^4^Hereas a *Boo\swashk{}* Entituled, *Muſæum Regal\ligis{}
Societat\ligis{}*, being not \| only a perfe *Catalogue* of all the
*Rarities* Natural and Arti- \| ﬁcial, belonging to the *Royal
Society*,and preſerved at *Gream* \| *Co edge.* \| […] \| Whereto is
alſo ſubjoyned a Treatiſe, Entituled, *The Comparative Anatomy* \| of
the *Stomachs* and *Guts* of about *Thirty* ſorts of *Animals* ; being
ſeveral \| […] \| Propoſed at a Meeting of the *Royal Society,
February* 16*th,* 16$\frac{79}{80}$ ’ χ^2^; 2^o^: 2 leaves, pp. [1–3]
[4] (*blank*); p. 4 ink transferred from next item, but in a previous
binding (ESTC R43222, but with a variant state of “Entituled” rather
than “entitutled”; the EEBO representation of the copy at UkOxU is
this variant state as well; ESTC is probably a typo) Bagford 30, Item
162–3.

‘At a Meeting of the Council of the *R. Society*, \| *Febr.* 22*th*,
168$\frac{1}{2}$. \| D^3^*R.* Grew *having read ſeveral Leures of
the* Anatomy of \| Plants, *ſome whereof have been already printed at
divers times*, \| […] \| *It is therefore Ordered, That he be dered
to cauſe them to be printed* \| *together in one Volume : and in
regard of the great number of Figures* \| *belonging to them, to
ta\swashk{}e upon Himſelf a more particular Care of the* \|
*Impreon*. \| […] \| *At a Meeting of the* R. Society, *March*
15*th*, 168$\frac{1}{2}$. \| P^3^*Ropoſals made ( agreeable to the
Order above ) for Printing a Book,* \| *Entitul’d*, The Anatomy of
Plants &c. by Dr. *Nehemjah Grew,* \| […]’  2^o^: χ^2^; 2 leaves, pp. [1–4]
(ESTC R177689, but incorrect pagination)  Bagford 30, Item 164–5.

[*fair-copy manuscript of the subsequent John Ogilby proposals*]  Item 166–7\*.

‘[*arms of London*] \| Hanſon Maior. \| Jovis *Decimo nono die*
Junii , 1673. *Annoq*; *Regni Reg\ligis{}* CAROLI *Secundi*, \|
Angliæ, *🙰c. \swashv{}icemo quinto*. \| W^2^HEREAS *\swashJ{}ohn
Ogilby* Eſq; His Majeﬅy’s *Coſmographer* , having ob- \| tained
Authority from His Majeﬅy, under His Sign Manual, for an *Aual* \|
*SURVEY* of His Majeﬅy’s Kingdom of *ENGLAND* and Dominion of \| […]
\| Letters to Recommend the ſame, by way of a General Suſcription, […]
\| […] \| […] Upon the Humble Requeﬅ of the ſaid *\swashJ{}ohn
Ogilby*, this \| Court wa spleas’d not onely to grant him a Licenſe
for proceeding in the ſaid Work, \| but to Encourage him in ſo Great
and Changeable an Undertaking, have led the \| way by a Subſcription
of Two hundred Pounds ; […] \| *WA\swashG{}S\swashT{}AFFE.*’  2^o^:
χ^2^; 2 leaves, pp. [1–3] [4] (*blank*), p. 2 “To the Right Honorable
The Lord Maior and Court of Aldermen of the City of London.”, p. 3
“Hooker Maior. Jovis decimus die Februarii, 1673. Annoq; Regni Regis
Caroli Secundi, Angliæ, &c. Vicesimo sexto.”  (Not in ESTC) Item
168–9\* Wolf catalogue incorrectly reports two separate items.

‘[*arms of London twice*] \| Hanſon Maior. \| Jovis *decimo nono die*
Junii, 1673.  *Annoq; \swashR{}egni \swashR{}eg\ligis{}* CAROLI
*Secundi*, Angliæ, *🙰c.* \| *Vicemo quinto*. \| W^8^HEREAS
*\swashJ{}ohn Ogilby* Eſq; His Majeﬅies *Coſmographer* , having
obtain- \| ed Authority from His Majeﬅy , under His Sign Manual, for
an *\swashA{}ual* \| *S\swashV{}\swashR{}VE\swashY{}* of His Majeﬅy’s
Kingdom of *E\swashN{}\swashG{}LA\swashN{}\swashD{}* […] \| […] \| His
Majeﬅy was […] \| […] to Recommend […] \| […] a General Subſcription
[…]\| […] Leading the way by a Royal Example of Five Hundred Pounds \|
for Himſelf, and Five hundred Pounds more for His Royal Conſort. […]
\| […] \| *WA\swashG{}S\swashT{}AFFE.*’  1^o^: χ1 pp. [1] [2] (*blank*)
(Not in ESTC)  Item 170\*.

[*t.p. and leaves from ESTC R30755*] Item 171–6\*.

‘THE \| REPORT \| Of the Right Honorable \| The Earls of Bridgewater,
Sadwich, Exex, and \| Angleſey, and the Right Honorable \| the Lord
Holles : \| Upon an Order of Reference from His Majeﬅy to Their Lord-
\| ips, upon the Propoſals of *\swashJ{}ohn Ogilby* Eſq; His Majeﬅy’s
\| *Coſmographer*, for a General Survey of *England* and *VVales, 🙰c.*
\| How ſo Great a Work may be Supported and Carried on to \|
Perfeion. \| […]’  2^o^: χ1; 1 leaf, pp. [1–2], p. “Dated this Ninth day of
April, 1672.”  (ESTC R182667)  Item 177\*.

‘[*royal arms*] \| CHARLES R. \| C^8^HARLES *\swashB{}y the Grace of
\swashG{}od, \swashK{}ing of* \| England , Scotland , France , *and*
Ireland, \| *\swashD{}efender of the Faith,* &c. *To all and every* \|
[…] \| *Whereas upon Humble Application made unto \swash{}Vs by Our
Well-beloved* \| *Subje and Servant* John Ogilby *Eſq; Our*
Coſmographer, […] \| *Iuﬅration of Our \swashK{}ingdom and
\swashD{}ominion hitherto omitted,* \| […] \| [… *We were
\swashG{}raciouy pleaſed to \swashG{}rant Our Warrant* \| *unto him,
under Our Signet and Sign Manual, for the \swashV{}nderta\swashk{}ing
the* \| *ſaid \swashG{}eneral Survey.  \swashA{}nd he the ſaid* John
Ogilby, *ﬁnding the* \| *Charge of ſo \swashG{}reat an
\swashV{}nderta\swashk{}ing much to exceed his \swashP{}rivate
Fortune,* \| […] \| *HE\swashN{}\swashR{}Y
COVE\swashN{}\swashT{}\swashR{}Y.’ 2^o^: χ1; 1 leaf, pp. [1–2], p. 2 “… at
Whitehall the Eleventh day of July, 1672.”  (ESTC R173743) Item 178\*.

‘Mr. OGILBY’s PROPOSALS \| For the more Speedy and Better Carrying on
of His \| BRITANNIA, \| And the GENERAL SURVEY thereof, by way of a
Standing LOTTERY. \| […]’  1^o^: χ1; 1 leaf, pp. [1] [2] (*blank*), last
line: ‘open the *LOTTERY*, and begin to Draw.’  (Similar to ESTC
R213782 and R224401)  Item 179\*.

‘A SECOND \| PROPOSAL \| By the [Author]{.smallcaps}, \| For the
better and more ſpeedy Vendition of ſeveral Volumes , ( his own
Works ) by the way of a ﬅanding [Lottery.]{.smallcaps} \| *Licenſed by
H\ligis{} Royal Highneſs the Du\swashk{}e of* York,*and Aﬅants of the
Corporation of the* Royal Fiing. \| […]’ 1^o^: χ1; 1 leaf, pp. [1] [2]
(*blank*) (ESTC R181194 [1668?])  Item 180\*.

‘PROPOSALS \| FOR THE \| \dontleavehmode{\fraktur ACTUAL SURVEY}
\| Of all the \dontleavehmode{\fraktur Counties} in \| ENGLAND and
WALES: \| BY \| Mr. *ADAMS* of the *Inner-Temple.* \| […]’  2^o^: χ^2^; 2
leaves, pp. [1–3] [4] (*blank*)  (ESTC R213919 [1682?])  Item 181–2\*.

‘The CASE of *Corneli\ligus{} Bee* and his *Partners, Richard Royﬅon,
William Wells,* \| *Samuel Thompſon, Thom\ligas{} Robinſon,* and
*William Morden*, Bookſellers. \| […]’  2^o^: χ1; 1 leaf, p. [1] [2]
(*blank*)  (ESTC R37531 [1666?])  Item 183 pasted to a blank recto.

[*newspaper advertisement for Ogilby*]  Item 184–5 pasted to blank verso.

‘PROPOSALS \| *For the laﬅ general Sale of Mr.* [Ogilby]{.smallcaps}’s
Books, Maps, \| Roads, &c. *intended to be opened on
the  th. \swashD{}ay of* \| March 169$\frac{0}{1}$. *by* Robert
Morden *; and continue to be* \| *drawn on* Monday, Wedneſday, *and*
Friday , *from \swashT{}wo of* \| *the Cloc\swashk{} Afternoon, ti
Seven in the Evening, at* Garraway’*s* \| Coﬀe-Houſe *in*
Exchange-Alley. \| […]’  2^o^: χ1; 1 leaf, pp. [1] [2] (*blank*)  (ESTC
R181192)  Item 186\* pasted to blank recto.

[*Gathering C^2^ from an edition of Sturmy’s* Mariners magazine] Item 187–8.

‘[*rule*] \| [May]{.smallcaps} the 16th. 1693. \| [*rule*] \| New
PROPOSALS \| FOR THE \| Printing of a BOOK \| OF \| William
Leybourn’s, \| […]’  2^o^: χ^2^; 2 leaves, pp. 1–4.  “E 98” manuscript
pen on p.1; (Not in ESTC) Item 189–90\*.

‘PROPOSALS \| FOR \| PRINTING \| A \| New Atlas. \| [Moses
Pitt]{.smallcaps} *of* London, *Bookſeller, being* \| […]’ 2^o^: χ^4^;
4 leaves, pp. [1–4], p. 3 “May 3. 1678.” p. 4 subscriber’s signature; on heavy atlas paper; (ESTC
R218763) Bagford 39, Item 191–192\*.

‘A Catalogue of the Subſcribers Names to the *Engli Atlas*, now
Printing at the *Theater* in \| *Oxford.*  They are dered to take
notice, that the ﬁrﬅ Volmue is ﬁnied , ( containing the \| […]’ 2^o^:
A^2^ [\$2 signed]; 2 leaves, pp. [1–4] p. 1 “This Paper is delivered
*Gratis* by *M. Pitt* in *London*. Mr. *R. Davis* in *Oxon*, and
Mr. *Sympſon* in *Cambridg* Bookſellers: To the end, that if any error
be committed it may be correed.”  Bagford 39, Item 193–4.

‘PROPOSALS \| Humbly tendred to the \| *NOBILITY* and *GENTRY*, by
*Richard Blome* \| […] \| undertake a work no leſs uſeful than
delightful ,to be Entituled the *GENTLE-* \| *MANS RECREATION,*which
in a large Volume in two *\swashP{}arts* or *\swashT{}omes* all treat
\| […]’ 2^o^: χ^2^; 2 leaves, pp. [1–3] [4] (*blank*) (Not in ESTC but
similar to R233440, and the proposed work is R23267 [1685]) Bagford
33, Item 195–6.

‘PROPOSALS *for Reprinting* [Guillim]{.smallcaps}’*s* Diſplay of
Heraldry, \| *in Folio, with large and uſeful Additions, \ligas{} well
of* Coat-Armour *\ligas{} of* Precedency, \| […]’  Two single-leaf copies printed
half-sheet: 2^o^: χ^2^; 2 leaves, p. [1] [2] (*blank*), ^2^[1] [2]
(*blank*), p. 1 “the Proprietor of the Copy, Mry Richard Blome, […]
doth intend to proceed to a Sixth Impression.”  (Not in ESTC)  Bagford 33, Item
197–8.

[*catalogue of books and instruments sold by* John Seller]  Item 199\*.

[*catalogue of atlases and charts sold by* John Seller] Item 200.

‘ADVERTISEMENT. \| W^3^Hereas, *\swashJ{}ohn Seller,* Hydrographer to
the King, *\swashJ{}ohn Oliver* and *\swashR{}ichard \swashP{}almer*,
\| being Authoriz’d by the King’s moﬅ Excellent Majeﬅy , by a ſpecial
War- \| rant to them direed , from His Majeﬅies Court at
*Windſor*,dated the 15*th* \| of *Auguﬅ*, 1679. for Surveying the
Kingdom of *England,* and Principality of *Wales.* \| […] \| […] their
Book, Entituled *\swashA{}TLAS \swashA{}NGLICANUS,* to have large \|
[…] \| *\swashJ{}anuary* the 7. 16$\frac{80}{81.}$’  2^o^: χ1; 1 leaf,
pp. [1] [2] (*blank*)  (Not in ESTC)  Item 201.

‘PROPOSALS \| For the Aual Hydrographical SURVEY \| Of the Sea-Coaﬅs
of Great BRITAIN. \| \dontleavehmode{\fraktur By Captain Greenvile
Collins.} \| […]’  1^o^?: χ1; 1 leaf, pp. [1] [2] (*blank*) (Not in
ESTC, but the work is ESTC R28567 1693) Item 202.

[*advertisements for globes*]  Item 203.

‘PROPOSALS \| For a New large Size of \| GLOBES, \| By ROBERT MORDEN
at the *Atlas* in *Corn-Hill*, and \| JOHN ROWLEY,
*Mathematical-Inﬅrument-Maker*, under \| S. *Dunﬅan*’s-Church in
*Fleetﬅreet*. \| […]’ 1^o^: χ1; 1 leaf, pp. [1] [2] (*blank*), p. 1, in manuscript
“dupl: E98” (ESTC T491332 [1695-1703?])  Item 204.

‘PROPOSALS, \| By \| Coll. WILLIAM PARSONS, \| AND \| Tho. Tuttell
*Hydrographer and Mathematical* \| *Inﬅrument-maker to the* KING’S
*Moﬅ* \| *Excellent Majeﬅy*. \| FOR \| *A New Pair of* GLOBES *Thirty
Six Inches Diameter, to be Delineated by* \| […]’ 2^o^: χ1; 1 leaf, pp. [1]
[2] (*blank*), p. 1, in manuscript “dupl: H:115”  (ESTC T42843 [1735?]
is certainly wrong for Tuttle, active 1695–1702)  Item 205.

‘PROPOSALS \| For [Printing]{.smallcaps} the \| PHILOSOPHICAL \|
*\swashT{}RANSAC\swashT{}IONS* and *COLLEC\swashT{}IONS* \| To the end
of the YEAR< 1700. \| ABRIDG’D \| AND \| Diſpos’d under [General
Heads.]{.smallcaps} \| In Three VOLUMES. \| By *\swashJ{} LOWTHORP,*
M. A. and F. R. S. \| […]’ 4^o^: A^2^; 2 leaves, pp. 1–4.  (ESTC
T142498) Bagford 34, Item 206–7.

‘*A PROPOSAL.* \| *Theſe Three Excellent Book\swashk{}s are Propoſed,*
\| (*One of which is left for your Peruſal, and the* \| *Others, if
dered*) viz. \| […]’  4^o^: χ1; 1 leaf, pp. [1–2], p. 2  “All Three Printed
for, and Sold by T. Huse, in Exeter-Court in the Strand, 1704.”  (ESTC
T142497)  Item 208.

‘*February* 27. 170$\frac{4}{5}$. \| PROPOSALS \| For Printing a \|
COMPLEAT HISTORY \| OF \| ENGLAND, \| FROM \|
\dontleavehmode{\fraktur The Earlieﬅ Account of Time} \| To the
Death of his Late Majeﬅy \| King William the Third: \| CONTAINING, \|
A Faithful Relation of all Aﬀairs of State, Eccleaﬅical \| and Civil,
with the [Effigies]{.smallcaps} of all the Kings and \| Queens, taken
from the Originals, and curiouy En- \| graved by the beﬅ Maﬅers. \|
[…]’  2^o^: χ^2^; 2 leaves, pp. [1-2], ^2^1 [2] (*blank*) (ESTC T142496)
Item 209–10.

‘PROPOSALS \| For Printing a Colleion of all the remarkable \| *Funeral Monuments* and *Public Inſcriptions*, \| ANTIENT and MODERN, \| Throughout \| England, Scotland, & Ireland; \| WITH \| […]’ 2^o^: χ1; 1 leaf, pp. [1] [2] (*blank*)  (ESTC T142495 [1705?])  Item 211.

‘PROPOSALS FOR ENGRAVING \| A \| MAUSOLEUM \| OR \|
\dontleavehmode{\fraktur Magniﬁcent Monumental Structure of Compote
Architecture} \| IN \| PERSPECTIVE, \| Adorn’d with various FIGURES,
gniﬁcant EMBLEMS \| and HISTORIES, *🙰c.* \| TO THE \| […] \| *The
whole curiouy Written and Engraven in great Variety of Hands by* JOHN
STURT. *Undertaken* \| […] \| ‘Twill not be ſold to any but
Subſcribers under 7*s*. 6*d*.’ 2^o^: χ1; 1 leaf, pp. [1] [2] (*blank*)
(ESTC T142494) Item 212\*.

[*another copy of the previous*] Item 213\*.

‘PROPOSALS \| FOR \| Engraving and Printing a large Volume in Folio \|
OF \| Architeure and Perſpeive, \| \dontleavehmode{\fraktur In
Engli and Latin} : \| Conﬅing of *One Hundred* and *Five* ample
*Folio \swashP{}lates* ; with Explanatory Diſcourſes on Each: \| […]
\| Excellent *Archite* and *Painter* [Andrea Pozzo]{.smallcaps}, \|
[…]’  2^o^: χ1; 1 leaf, pp. [1] [2] (*blank*) on heavier engraving paper; (ESTC T142493 [1705?])  Item 214.

‘An \| ESSAY, \| T^6^Owards a [Historical Treatise]{.smallcaps}, on
that moﬅ Uni- \| verſally Famous, as well as Uſeful *Art of
Typography*, from \| the ﬁrﬅ Invention of it at *Harlem*, by *Coﬅer*,
with Molds \| or Blocks of Wood. \| […]’  2^o^: χ1; 1 leaf, pp. [1] [2]
(*blank*) TODO Bagford’s proposal? (ESTC N8305 [1707?]) Item 215.

‘*London, March* 26. 1701. \| A PROPOSAL. \| T^3^HERE being now
Preparing for the \| Preſs, *An Hiﬅory of the Lives of the moﬅ* \|
*Illuﬅrious \swashP{}erſons in Church and State that* \| *Flouried
in* England *during this laﬅ Century , with* \| […]’  2^o^: χ1; 1 leaf,
pp. [1] [2] (*blank*), p. 1 “The Booksellers concern in Printing this
History.  Jacob Tonson, […] Andrew Bell […] Thomas Cockerill […]”
(ESTC T142492)  Item 216 pasted to the recto of a blank.

‘PROPOSALS \| *For Subſcription to* [Thucydides]{.smallcaps}
Gr. Lat. Folio , *now \swashP{}rinting* \| *at the Theater in Oxford.
Three of the eight Books of this Hi-* \| *ﬆory are already printed ;
Copies of which may be ſeen at the* \| *Undertaker’s* : *and the whole
is degn’d, God willing, to be ﬁni’d* \| *and ready for publication
in* Trinity Term, 1695. \| […]’  2^o^: χ^2^; 2 leaves, pp. [1–2] 161–2, pp. 161–2
[*specimen*]   (ESTC R219316)  Item 217–218.

[*True patriot vindicated, London, 1701*] Item 219\*.

[*A letter to Mr. Jacob, 1702, 4 pp.*]  Item 220–1\*.

[*Explanation of the Christ-Church Almanacks, 1702*]  Item 222\*.

‘PROPOSALS \| FOR SUBSCRIPTIONS \| To the Works of \| S^t^. CYRIL \|
Biop of JERUSALEM, \| *Now \swashP{}rinting at the Theater in*
[Oxford.]{.smallcaps} \| […]’  2^o^: χ^2^; 2 leaves, pp. [1–2] 3–4
(*specimen*)  (ESTC T142490 [1702?])  Item 223–4.

‘PROPOSALS \| FOR SUBSCRIPTIONS \| To a Compleat HISTORY \| Of the
*HEBREW TEXT*, \| And the *Greek*, and *Latin* Tranations \| OF THE
\| OLD TESTAMENT, \| In IV. Books, by Dr. *Hody*. \| […]’  2^o^: χ^2^
[1–4], p. 1,4 [*blank*], p. 2 [*t.p.*], p. 3 ‘PROPOSALS \| […]’  (ESTC
T142489 [1703?])  Item 225–6.

‘[*two rules*] \| *London, \swashJ{}uly* 30th. 1701. \| PROPOSALS \|
For Printing by Way of Subſcription, \| The Works of Joſephus. \|
Tranated into Engli by Sir *\swashR{}oger L’ Eﬅrange*, K^t.^ \| […]’
2^o^ χ1; 1 leaf, pp. [1] [2] (*blank*), (ESTC T142488)  Item 227.

[*another copy of Item 227*] Item 228.

‘PROPOSALS \| *Concerning the Printing a Volume of Elaborate SERMONS*
\| *upon the One hundred and nineteenth* Pſalm.  *Preached in* \| *his
life-time by that eminently Learned and \swashJ{}udicio\ligus{}
Divine,* \| Thomas Manton, *\swashD{}. D. and now from his own
Manuſcripts* \| *carefully peruſed by Dr.* William Bates, *🙰c. for the
\swashP{}reſs.* \| […]’  2^o^: χ1; 1 leaf, pp. [1] [2] (*blank*), p. 1
“Undertakers […] Thomas Parkhurst […] Jonathan Robinson […] Brabazon
Aylmer […] Benjamin Alsop […]”  “dupl: H. 84[*?*]” in manuscript on
p. 1 (ESTC R180299 [1681]) Item 229.

‘[*two rules*] \| THE \| PROPOSAL \| OF \| William Laycock, \| Of the
*Inner-Temple, London,* Gent. \| *Humbly Recommended to all ſuch
Perſons , who are generouy incli-* \| *ned to encourage Arts and
Learning, and in Order thereunto for* \| *raing a Fund for the buying
up of a Stoc\swashk{} of ſcarce Stitcht Boo\swashk{}s* \| *and
Pamphlets , amongﬅ which all Boo\swashk{}i Gentlemen well
\swashk{}now ,* \| *that there are to be found abundance of Excellent
\swashT{}ras and Diſ-* \| *courſes , not treated of in larger
Boo\swashk{}s.* \| […]’  2^o^: A^2^ [A1 signed]; 2 leaves, pp. 1–4.  (ESTC  T142487
[1702])  Item 230–1.

[*Case of proprietors, 1702; prospectuses for annuities 1705,7*]  Item
232–5\*.

‘PROPOSALS \| FOR \| Printing by Subſcription, \| A BOOK Entitled, \|
*\swashJ{}AMES AUGUSTUS THUANUS* \| HIS \| HISTORY \| Of His own
[Times]{.smallcaps} : \| CONTAINING A \| COMPLEAT ACCOUNT \| OF THE \|
TRANSACTIONS \| OF THE \| WHOLE WORLD, \| From the Year 1543, to
1618 : \| Faithfully Tranated and Abridg’d from the beﬅ \| Edition in
*LATIN*. \| […]’  2^o^: χ^2^; 2 leaves, pp. [*2*] 1 [2], p. 1 [*specimen:*]
THUANUS,s *Hiﬅory of hi\ligis{} own Times.* \| The Death of this
Viorious King of \| […]’  p. 2 [*blank*] (ESTC T142485) Item 236–7.

‘PROPOSALS \| For Printing by Subſcription all the *Odes* , *Epods*,
*Satyrs*, and \| *Epiﬅles* of *HORACE*, Tranated into *Engli* Verſe,
\| together wirh the *Notes* and *Critical Remarks* of the Fa- \| mous
Mr. *Dacier*.  Correed and Enlarged with ſeveral \| more
Additions. \| [*rule*] \| By *\swashJ{}OS. BROWNE*, L. L. M. D. \|
[*rule*] \| […] \| *Novemb.* 10*th*, 1704.’  2^o^: χ^2^; 2 leaves,
pp. [1–4], p. 1 [*specimen:*] ‘THE \| ODES \| OF \| HORACE. \| […]’,
p. 2–3 ‘*\swashR{}EMA\swashR{}KS on the Firﬅ ODE.*’, p. 4 ‘PROPOSALS
…’ paper has horizontal chainlines suggesting an extra large size (ESTC T142484)  Item 238–9.

[*not a proposal:* ‘AN ACCOUNT \| OF THE \| Propagation of the Goſpel
in \| FOREIGN PARTS. \| […]’  χ^2^; 2 leaves, pp. 1–4, p. 4 “London,
Printed by Joseph Downing in Bartholomew-Close near West-Smithfield,
1704.”  (ESTC T18722)]  Item 240–1\*.

[*catalogue of Christian tracts, ca. 1703*]  Item 242–3\*.

[*Philadelphian Society: protestation; elegy; counterpart of the
elegy, 1703*]  Item 244–247\*.

[*Proposal to reprint Mercuries to 1688, damaged:*] ‘*of* Europe,
contain’d in *The Hiﬅorical and Political Monthly Mercuries*, \| from
the late happy Revolution in *November* 1688, to *\swashJ{}uly*, 1690,
\| where the Tranation was begun, and is continu’d to this Time : \|
Done from the Originals published at the *Hague* by the Authority \|
of the States of *Hoand*, \| […]’ 2^o^?: χ1; 1 leaf, pp. [1] [2]
(*blank*) (possibly the prospectus for ESTC P2171) Bagford 50, Item
248.


\BiblHangingIndentsStop

## Transcription of printed proposal

\TranscriptionStart

[*Next eleven lines, centered:*]

[*page number:*] ( 1 ) [**6.6+//3.3**]

PROPOSALS [**/11.3/**]\
For [Printing]{.smallcaps} the [**/5.0/3.0**]\
PHILOSOPHICAL [**/6.6/**]\
*\swashT{}RANSAC\swashT{}IONS* and *COLLEC\swashT{}IONS* [**/5.0/3.0**]\
To the end of the YEAR, 1700. [**5.6+/4.0/2.3**]\
ABRIDG’D [**5.6+/5.0/**]\
AND [**/3.0/**]\
Diſpos’d under [General Heads]{.smallcaps}. [**7.0/5.0/3.3**]\
In Three VOLUMES. [**/3.3/2.0**]\
By *\swashJ{}. LOWTHORP*, M. A. and F. R. S. [**5.6+/4.0/3.0**]


T^4^ HE Philoſophical Tranſaions are well known to be an Excel- [**15.6/15.0/, 4.0/2.6/1.6 (82.0)**]\
‌  lent Regiﬅer of many Valuable Experiments, and a Curious\
‌  Colleion of ſuch Diſcourſes , as being too ort to be Printed\
‌  alone, were in great Danger of being loﬅ ; and have been\
publied almoﬅ every Month (ſome few Years of Interruption excepted)\
nce the beginning of the Year 1665, by the particular Encouragement
of\
the Royal Society.  But the General Approbation they have met with ,
both\
at home and abroad, has long nce made them ſo ſcarce , that a
compleat\
Set is not to be procur’d without much Trouble , and not to be
purchas’d\
under 15 *l*.  And yet the vaﬅ Bulk of 22 Volumes in *\swashQ{}uarto* [**3.0+/2.0/1.3**]
to which\
[*direction line, centered:*]          A          they[**page 2:**]\
[*page number centered:*] ( 2 ) [**6.6+//3.3**]\
they are ſwell’d, and that want of Conneion which could not be
avoided [**4.0/2.6/1.6 (82)**] [Uk: *t in ‘they’ shifted up*; UkOxU-SK, UkLiU: *d in ‘avoided’ shifted up*]\
in this manner of communicating ſuch a multitude of independent
Papers ,\
make it altogether unadviſable to Reprint them in their preſent Form.
The\
Author therefore of this Abridgment conceives it may be ſome Service
to\
the Publick, to Contra them into a leſs Compaſs ; and to Range and
Diſpoſe\
them under General Heads : In order to which,

 I.  He has divided the whole Work into Three Volumes.  The Firﬅ\
Volume contains all the Mathematical Papers ; The Second, the
Phyolo-\
gical ;  And the Firﬅ Part of the Third , thoſe that are Medical and\
Anatomical ; the latter part of it being reſerv’d for the
Philological , and\
ſuch Miſcellaneous Papers , as cannot properly be reduced to any of
the\
Former Claes.  Each of theſe Volumes are ſubdivided into Chapters
and\
General Heads , under which each Paper is inſerted in the beﬅ Order
their\
great Variety will admit of.

 II. He has been very careful in Abridging the Papers, to preſerve the\
Sence of their Authors entire, and to relate the Hiﬅories of Fas,
with\
all the material Circumﬅances and uſeful Reaſonings thereon.

 III. To cloſe in ſome meaſure one of the Chaſmes in the Tranſaions ;\
*\swashv{}i\swashz{}*. between the Years 1678 and 1683.  He hath
inſerted in their proper\
Places, Dr. *Hoo\swashk{}*’s [**3.3+/2.0/1.3**] Philoſophical Colleions , being a Work
of the ſame\
kind, and Publi’d by him during that Interval of Time.

 IV. To make the Work Compleat, He will add ſuch Indexes both of\
Authors and Subjes, as may be of any Uſe or Advantage to the Readers.

 In ort , the Author of this Abridgement profees, that He hath every\
where degn’d ( according to the beﬅ of his Underﬅanding ) to leave
out\
nothing that would be of any Service to the greateﬅ Part of Readers,
and to\
burthen them with nothing that could be ſpar’d, without manifeﬅ
prejudice to\
the Sence of the ſeveral Ingenious Authors ; and that he hath
carefully en-\
deavour’d that none of their Papers ould ſuﬀer by pang through his\
Hands.

[*direction line, to the right:*] *MAY* [**/3.3/**]

[**page 3:**]

[*page number, centered:*] ( 3 ) [**6.0+//3.3**]

[*centered line:*] *MA\swashY{}* 28. 1703. [**6.6+/5.0/3.3**]

*An Account of th\ligis{}* Undertaking ( *ſomething more at* [**5.3/4.0/2.3 (120)**]\
‌ *large, than can conveniently be here inſerted* ) *being*\
‌ *laid before the* [Royal Society]{.smallcaps}, *by the* Author\
‌ *at one of their late Meetings*, The Degn was Ap-\
‌ proved by the ſaid *Society*, and he was Dered\
‌ to Proceed therein.

[*centered line:*] The PROPOSALS, *🙰*c. [**6.0+/4.6/3.0**]

I. T^3^HIS Work will contain about 240 Sheets of Paper , which , [**11.6+/10.6/, 4.0/2.6/1.6**]\
‌  with a large Number of Cuts, will make the Charge equal to\
‌  that of 300 Sheets.

 II. The Book all be Printed on the ſame Paper , and with the ſame\
Letter that this Propoſal is done with , except ſome few on large
Paper for\
ſuch Gentlemen as are Curious.

[*direction line, right:*] III. The [**/2.6/1.6**]

[**page 4:**]

[*page number, centered:*] ( 4 ) [**6.3+//**]

 III. The Price to Subſcribers to be Forty Shillings for the Small Paper ,[**4.0/2.6/1.3, italic: 3.3/2.0/1.3**]\
and Three Pounds for the Large in Quires :  Ten Shillings to be paid
down,\
and the reﬅ on delivery of the Book.  Thoſe who procure Subſcriptions
for\
Six Books of the Small Paper, all have a Seventh *Grat\ligis{}*,
which will reduce\
the Price of thoſe Copies to near Thirty four Shillings.  It will not
be ſold\
under Fifty Shillings to any that are not Subſcribers.

 IV. Subſcriptions will be taken in till *Michaelmas* next , and no longer ;\
for part of the ﬁrﬅ Volume being already Printed , and the Copy of
the\
other Two in great forwardneſs , the whole Work will be ﬁnied in
next\
*Michaelm\ligas{}* Term.

*Propoſals and Specimens are deliver’d, and Subſcriptions
ta\swashk{}en by the \swashV{}nderta\swashk{}ers*,\
‌ Thomas Bennet, *at the* Half Moon ; Robert Knaplock, *at the* Angel ;
*and*\
‌ Richard Wilkin , *at the* King’s Head *in St.* Paul’*s Church-yard*,
London,\
‌ *and by the Boo\swashk{}ſeers in moﬅ of the conderable Towns in* England. [*long ſ in Booksellers bent to avoid the swash \swashk{}*]

\TranscriptionStop

## Type sizes

Write an introduction here (TODO), FIX your measurements with above corrected ones (TODO), left to right here.

\bgroup\ttxx\setupinterlinespace[1ex]

|           | Heads      | Text   | Layout               | Page |
|-----------|------------|--------|----------------------|------|
| 1 (A1^r^) | /11/, /6/, | 4/3/2  | 10 ll.,              | 7//3 |
|           | 7/5/3,     | (82)   | 30.3\|167.0\|41.3 ×  |      |
|           | 6/4/3,     |        | 35.0\|116.3\|31.0    |      |
|           | /3/2       |        | (15.0\|186.6\|35.3 × |      |
|           |            |        | 35.0\|116.3\|31.0)   |      |
|           |            |        |                      |      |
| 2 (A1^v^) | —          | 4/3/2  | 35 ll.,              | 7//3 |
|           |            | (82)   | 22.0\|144.0\|72.0 ×  |      |
|           |            |        | 30.3\|116.0\|36.0    |      |
|           |            |        | (14.0\|169.0\|56.0 × |      |
|           |            |        | 30.3\|117.0\|36.0)   |      |
|           |            |        |                      |      |
| 3 (A2^r^) | 7/5/3      | 6/4/3  | 5 ll., 6 ll.,        | 7//3 |
|           |            | (120), | 28.0\|115.3\|93.6 ×  |      |
|           |            | 4/3/2  | 35.0\|115.0\|32.0    |      |
|           |            | (82)   | (13.3\|156.6\|68.0 × |      |
|           |            |        | 35.0\|115.0\|32.0)   |      |
|           |            |        |                      |      |
| 4 (A2^r^) | —          | 4/3/2  | 16 ll.,              | 7//3 |
|           |            |        | 26.0\|66.3\|145.0 ×  |      |
|           |            |        | 31.0\|116.3\|26.6    |      |
|           |            |        | (14.6\|77.6\|145.0 × |      |
|           |            |        | 31.0\|116.3\|26.6)   |      |
: Type sizes

\egroup

## Skeleton

- Outer forme left-to-right: 61.6
- Outer forme top-to-bottom furniture: 29.6
- Outer forme top-to-bottom text: 56.3
- Outer forme measure: 116.3

- Inner forme left-to-right: 71.0
- Inner forme top-to-bottom furniture: 27.3
- Inner forme top-to-bottom text: 50.0
- Inner forme measure: 116.0, 115.0

- Printed inner forme first (probable)


## Description of abridgment

The abridgment introduces categories …

1. Mathematics
    1. Geometry, algebra, arithmetic
    2. Trigonometry, surveying
    3. Optics
    4. Astronomy
    5. Mechanics, acoustics
    6. Hydrostatics, hydraulics
    7. Geography, navigation
    8. Architecture, ship-building
    9. Perspective, sculpture, painting
    10. Music
    
2. Physiology
    1. Meteorology, pneumatics
    2. Hydrology
    3. Mineralogy
    4. Magnetics
    5. Zoology
    
3. Anatomy, physic, chemistry
    1. Anatomy, physic
       i. Structure, external parts, and common teguments
       ii. Head
       iii. Thorax
       iv. Abdomen
       v. Humors and general affections
       vi. Bones, joints, muscles
       vii. Monsters
       vii. Period of human life
    4. Pharmacy, chemistry

4. Philology, miscellanies
    1. Philology, grammar
    2. Chronology, history, antiquities
    3. Voyages and travels
    4. Miscellaneous

\BiblHangingIndentsStart

**UkLoRS LM 191d:** Three volumes in dark green (Centroid 146) modern
buckram with red leather lettering pieces on the spine, the arms of
the society gilt directly to spine.  First lettering piece, ‘PHIL. |
TRANS. | ABRIDGED’; second ‘I [II] [III]’.  Modern endpapers and
oversewn gatherings is what looks to be a working reference copy.  No
annotations suggesting this copy was presented to the society. (MORE
HERE BASED ON V.4-5 being presentation copies, the date, etc.)  Leaf
B1^r^ (1): 239.3 × 186.0, a large paper copy.

**UkLW EPB/Ser PHI/3 (ABR1):** Three volumes in vivid red (Centroid
11) modern buckram with gold lettering direct to spine.
‘PHILOSOPHICAL \| TRANSACTIONS \| ABRIDGED \| 1665-1700 \| 1 [2,3] \|
1705’, yellow edges sprinkled red.  Modern enpapers in light yellow
(Centroid 86) with cloth reinforced hinges.  Soiled from handling
suggesting it was a working reference copy.  Leaf B1^r^ (1) 224 × 162,
a small paper copy.  General Index, v. 3, bound between A2 and B1,
rather than after 4S4; lacks v. 1 pl. 4 (previously misbound facing
558 (4B3^v^)); v. 3 pt. 1: pl. 2 (facing 130 (S1^v^)), pl. 4 misbound
facing 310 (2R3^v^) rather than facing 210 (2E1^v^).  Paper typically
with boot within circle watermark, except distinctive now browner paper
with the odor of decay and no watermark for all the plates and v.1 \*–2\* h-n, v.2 [*A*]
3Y-6A, v.3 for [*A*], a-e, 4C-4S

**Uk 740.h.5:** Volume three only in modern institutional quarter deep
brown (Centroid 56) morocco over strong orange yellow (Centroid 68)
cloth boards with bind crown typical of British Library institutional
bindings, binder’s stamp ‘B.M. 1971’; seven panel spine, panel 1 with
image of two hands grasping a ring, panel 2 with ‘PHILOSOPHICAL \|
TRANSACTIONS. \| TO 1700 \| ABRIDG’D’, panel 4 ‘VOL. 3 \| LOWTHORP’,
smaller panel seven ‘LONDON \| 1705’; black hexagonal British Museum
ownership mark associated with Hans Sloane ‘MVEVM \| BRITAN \| NICVM’
on t.p. verso and e4^v^, penciled at head of t.p. ‘R. Academies [etc?]
Europe Great Britain and Ireland \| London Royal Society’ with a check
mark by the word “London” and the number “10” in lower right, pen
shelf mark 9.K.l, on endpaper facing t.p. ‘740.H.#5.’, label for
“PRESERVATION SERVICE” on lower paste down endpaper; t.p. glued to a
second piece of stiffer laid paper; p. 1 (B1^r^) 241×192, a large
paper copy.

**US-icu Q41 .L74 v.1:** Volume one only in institutional half sheep
repaired with black buckram, deep reddish brown (Centroid 75) German
marbled paper with pale yellow endpapers (Centroid 89); five panel
spine, panel 2 with gilt on sheep ‘Philosophical \| Transactions’,
panel 4 ‘1’, panel 5 with remnant of book label and white call number
‘Q \| 41 \| L74’; edges hair vein marbled; volume as v.5–6 which
include gilt ‘UNIV. OF \| CHICAGO \| LIBRARY’ on upper board lower
left on the leather back; on upper paste-down endpaper book plate of
‘\dontleavehmode{\fraktur The University of Chicago} \|
\dontleavehmode{\fraktur Libraries}’ with arms penciled ‘Gen. Lib.’ on
top center of book plate, penciled ‘Gen. Lib.’ top center of endpaper;
on t.p. ownership perforation of ‘THE \| UNIVERSITY [*previous word
diagonally up*] \| OF \| CHICAGO LIBRARY’, penciled ‘9842’ in upper
right, some other marks, and ‘Roy. Soc. of Lond.’ penciled to the
right of ‘In Three Volumes.’; on t.p. verso upper left ‘Q \| .L74 \|
v.1 \| c.2 \| RARE’, upper right ‘Q41 \| .L74 \| Rare Book \| v.1 \|
c.2’; on (t.p. + 1) recto inked ‘99931’ in upper center stamped
‘230098’ in faded, now very light greenish blue (Centroid 171); on
lower paste-down endpaper blank library card pasted with ‘99931’
penciled; p. 1 (B1^r^) 213×169, a small paper copy; paper typically
unwatermarked or with boot within circle watermark; lacks leaf \*1
containing the imprimatur, plate 3 facing p. 383 (3C4^r^) and bound
from opposite edge than usual with soiling on its outer edge; paper
stock for pl. 1–2 {6 \| 13 \| 27 \| 26 \| 29 \| 26 \| 27 \| 28 \| 26
\| 27 \| 28 \| 26 \| 27 \| 26 \| 15 \| 13}, pl. 2,7 {11 \| 12 \| 28 \|
26 \| 27 \| 26 \| 26 \| 27 \| 26 \| 28 \| 26 \| 26 \| 26 \| 28 \| 19},
pl. 4–5 {12 \| 26 \| 28 \| 25 \| 27 \| 25 \| 29 \| 27 \| 28 \| 25 \|
27 \| 27 \| 28 \| 17 \| 12} suggesting half-sheet engravings printed
on Post-sized paper; plates represent an early state with guide lines
and letters: pl. 1–2 have forward-reading numbers on some figures,
pl. 3–5,7 have backwards-reading numbers on some figures; staining
throughout indicating the volume was used to press leaves.

**Vol. 1:**\
4^o^: [*\**]^4^ 2\*^2^ a–n^2^ B–3M^4^ 4A–4U^4^ 4X^2^ [\$2 (−2\* a–n 4X2) signed; d1 signed D1]; 342 leaves, pp. [*32*] 1–454 543–708[=620] (misnumbering 200 as 100); 1–7 engraved plates (pl. 6 as ‘7’), pl. 1 facing pp. 54 (H3^v^), 2: 190 (2B3^v^), 3: 388 (3D2^v^), 4: 556 (4B2^v^), 5: 626 (4L1^v^), ‘7’ [=6]: 660 (4P2^v^), 7: 698 (4U1^v^)\
170504: a1=a2 2\* \$a — a1 a1 ns : a2 n1 at — b1 B1 r : b2 4X1 all\$is\$d ( 3M2 tur. : 4A2 locitate : 4N2 Leagues,\$ : 4O1 m\$in\$ea\$ )

**Vol. 2:**\
4^o^: [*A*]^2^ B–5Z^4^ 6A^2^ [\$2 (−6A2) signed]; 460 leaves, pp. [*4*]
1–915 [916]  (misnumbering 144 as 441, 281 as 211, 763 as 762); 1–14
engraved plates, pl. 1 facing pp. 42 (G1^v^), 2: 164 (Y2^v^), 3: 204 (2D2^v^), 4: 318
(2S3^v^), 5: 510 (3T2^v^), 6: 512 (3T4^v^), 7: 518 (3U3^v^), 8: 662 (4P3^v^), 9–10: 776 (5F4^v^), 11: 812 (5L2^v^), 12: 872 (5S4^v^), 13–14: 904 (5Y4^v^)\
170504: b1 B1 \$ : b2 6A1 ſur\$les\$*Vi*

**Vol. 3:**\ 4^o^: [*A*]^2^ B–4S^4^ a–e^2^ [\$2 (−3B 4Qa–e2) signed; a–e
signed ‘[a]’–‘[e]’]; 356 leaves, pp. [*4*] 1–371 [372] 373–688 [*20*]
(458–9 as 468–9, 462–3 as 472–3, 494 as 495, 623 as 605, 285 as
‘(285)’ rather than ‘( 285 )’); ^1^1–5, ^2^1–7 [=12] engraved
plates, pl. ^1^1 facing pp. 120 (Q4^v^), ^1^2: 130 (S1^v^), ^1^3: 162 (Y1^v^), ^1^4: 210 (2E1^v^),
^1^5: 320 (2S4^v^), ^2^1–2: 442 (3L1^v^), ^2^3–4: 526 (3X3^v^),
^2^5: 528 (3X4^v^), ^2^6–7: 682 (4S1^v^)\
170504: b1 B1 de : b2 4S2 \$*Numberi* — c1 a1 ;\$th : c2 e1 *nus*,\$

### Volume 1 contents

**\*1^r^:** [*blank*]

**\*1^v^:** [**Imprimatur**]

\TranscriptionStart

[*five lines, centered:*] \swashM{}*ay* 5.  1703. [**7.3/5.6/3.3**]

At a [Meeting]{.smallcaps} [**/5.3/3.3**]\
OF THE [**/3.6/**]\
*ROYAL SOCIETY* [**/6.6/**],

*Sir* [John Hoskyns,]{.smallcaps} V. P. *in the Chair*, [**7.3/5.0/3.0**]

*Mr.* [Lowthorp]{.smallcaps} *Preſented a* Propoſal *for Printing* [**6.0/4.0/3.0**]\
‌ *an* Abridgment *of the* Philoſophical Tranſaions.\
‌ *Th\ligis{}* Degn *w\ligas{} Appro\swashv{}’d by the* Society, *and He*\
‌ *w\ligas{} Der’d to* Proceed *therein*.

[*rule 109.6*]

[*centered:*] *\swashM{}ay* 12.  1705. [**7.3/5.0/3.0**]

Imprimatur [**9.0/7.0/5.0**],

    *Iſ. Newton,* R. S. Pr. [**9.0/7.0/5.0**]

\TranscriptionStop

**\*2^r^:** [**General title page**] THE \| PHILOSOPHICAL \| TRANSACTIONS \| AND \|
COLLECTIONS, \| To the End of the Year 1700. \| ABRIDG’D \| AND \|
Diſpos’d under [General Heads.]{.smallcaps} \| [*rule*] \| In Three
Volumes. \| [*rule*] \| By *\swashJ{}OHN LOWTHORP*, M. A. \|
F. R. S. \| [*rule*] \| *LONDON:* \| Printed for *Thom\ligas{} Bennet*
at the *Half-Moon*, *Robert \swashK{}naplock* at the \| *Angel* and
*Crown*, and *Richard Wilkin* at the *\swashK{}ing’s-Head*, in \|
St. *Paul*’s Church-yard, M DCC V.

\TranscriptionStart

THE [**/5.0/**]

PHILOSOPHICAL [**/5.3/**]

TRANSACTIONS [**10.0**]

AND [**/3.3/**]

COLLECTIONS, [**8.0**]

To the End of the Year 1700. [**6.0/4.3/3.3**]

ABRIDG’D [**10.0**]

AND [**/3.3/**]

Diſpos’d under [General Heads.]{.smallcaps} [**7.3/5.0/3.3**]

[*rule, 119.3*]

In Three Volumes. [**/7.0/5.0**]

[*rule, 118.3*]

By *\swashJ{}OHN LOWTHORP*, M. A. [**6.3/4.0/3.0**]\
and F. R. S.

[*rule, 119.0*]

*LONDON:* [**/4.0/**]

Printed for *Thom\ligas{} Bennet* at the *Half-Moon*, *Robert \swashK{}naplock* at the [**5.0/3.6/2.3**]\
‌ *Angel* and *Crown*, and *Richard Wilkin* at the *\swashK{}ing’s-Head*, in\
‌ St. *Paul*’s Church-yard, M DCC V.

\TranscriptionStop


**\*2^v^:** [*blank*]

**\*3^r^–\*4^v^:** [**Dedication**]

\TranscriptionStart

[*two rules, 5.3 × 108.3, 190.0*]

[*next seven lines centered:*] TO HIS [**/5.3/**]\
Royal Highneſs [**16.0/11.3/8.0**]\
THE [**/4.3/**]\
PRINCE, [**16.0**]\
Lord High ADMIRAL [**9.0/7.0/4.6**]\
OF [**/4.0/**]\
ENGLAND, [**9.0**] *🙰c* [**9.0/7.3?/4.0**].

 SIR, [**/5.0/**]

Y^3^ *OUR* Royal Highneſs’*s Great Con-* [**7.0/5.0/3.3 (163.0)**]\
‌  *decenon to Subſcripbe the Statutes of*\
‌  *the* Royal Society, *\ligas{} one of their*\
Fellows, *Commands ſome Tribute from* [**\*3^v^:**]\
*e\swashv{}ery Member.  Th\ligis{},* [Sir]{.smallcaps}, *I humbly*\
*hope, \swashw{}i in ſome meaſure excuſe the Pre-*\
*ſumption of laying theſe* Papers *at \swashY{}our*\
Royal Highneſs’*s Feet.  They ha\swashv{}e una-*\
*\swashv{}oidably, in th\ligis{} Abridgment, loﬅ much of*\
*their Original Beauty :  But the* Order\
*\swashw{}herein the Remaining Subﬅance \ligis{} Diſpo-*\
*ſed, \swashw{}i gi\swashv{}e \swashY{}our* Royal Highneſs *a*\
*Nearer Proſpe of the Courſe of thoſe Stu-*\
*dies you ha\swashv{}e the Goodneſs to* Prote.

 *It \swashw{}\ligas{} a Noble Degn, Worthy of their*\
Royal Founder, *by* Incorporating *th\ligis{}*\
Society *to Perpetuate a* Succeon *of* Uſe-\
ful Inventions: *But the \swashD{}iſcouraging Neg-*\
*le of the Great, the Impetuo\ligus{} Contradic-*\
*tions of the Ignorant, and the Reproaches*\
*of the Unreaſonable, ha\swashv{}e unhappily Re-*\
*tarded them in their Purſuit of thoſe Great*\
*Ends.  To* Reﬅore *them therefore to their*\
*ﬁrﬅ Vigour, \ligis{} a Glory reſer\swashv{}’d for \swashY{}our* [**\*4^r^:**]\
Royal Highneſs ; *And already,* [Sir]{.smallcaps}, *\swashw{}e*\
*feel the Chearful Inﬂuence of a Return-*\
*ing Spring.*

 *The Commands \swashY{}our* Royal High-\
neſs *h\ligas{} gi\swashv{}en, for Publiing, at \swashY{}our*\
*o\swashw{}n Expence, a moﬆ Magniﬁcent* Urano-\
graphy (*far exceeding that of a the* Ara-\
bian Princes, *the Noble* Tycho Brahe,\
*and the Induﬅrio\ligus{}* Hevelius) *cannot fail*\
*of ſurpri\swashz{}ing Eﬀes.  A* Art *and* Na-\
ture *\swashw{}i exert their \swashP{}o\swashw{}ers upon this*\
*Occaon, to keep pace \swashw{}ith* Aﬅronomy ;\
*particularly* Navigation, (*being under*\
*\swashY{}our* Royal Highneſs’*s Immediate* Care)\
*\swashw{}i Induﬅriouy apply thoſe* Accurate Ob-\
ſervations *to a* Nautical *Purpoſes, and*\
*by ſome Familiar Method, Deli\swashv{}er the*\
*Anxio\ligus{}* Seamen *from the* Fatal Acci-\
dents *that frequently attend their* Miﬅa-\
ken Longitude.

[**\*4^v^:**]

 *Th\ligus{},* [Sir,]{.smallcaps} *the* Muniﬁcence *of the*\
Prince, *and the* Vigilance *of the* Lord\
High Admiral, *\swashw{}i be equay a Bleſ-*\
*ng to the Preſent, and to a Future Ages.*\
*May \swashY{}our* Royal Highneſs *e\swashv{}er meet*\
*\swashw{}ith Returns of Gratitude, Suitable to the*\
*Uni\swashv{}erſal* Beneﬁcence.  *So Wies, \swashw{}ith*\
*great Sincerity,*

 *May it pleaſe \swashY{}our* [Royal Highness,]{.smallcaps}

    *\swashY{}our* [Royal Highness’]{.smallcaps}*s*

        *Moﬆ* Faithful, *and*\
‌         *Moﬆ* Obedient Servant,

            *\swashJ{}ohn Lowthorp*.

\TranscriptionStop

**2\*1^r^–2\*^v^:** [*two rules 5.0 × 109.6, 108.6*] \| THE [**/5.3/**] \| PREFACE. [**11.6**]

\TranscriptionStart

T^4^ HE *Philoſophical Tranſaions* having met with [**4.6/3.3/2.0 (111.0)**]\
‌  General Applauſe and Encouragement for many\
‌  Years, it would be a Needleſs Trouble to give\
‌  any *Hiﬅory* of them : ’Tis enough to ſay, That\
Many of the Diſcourſes were Compos’d, and All of them\
Colleed and Publi’d, by *Particular Members* of the\
*Royal Society*.  I all therefore employ theſe very few Pa-\
ges only to acquaint the Reader with My Own Condu in\
this *Abridgment* of them.

 When I ﬁrﬅ Reſolv’d upon this Undertaking, I had Two\
Sorts of Readers in View, whom I was derous to ſerve ;\
thoſe who make uſe of Books for their Private Inﬅrucion\
or Entertainment, and thoſe who Conſult them in order to\
Publi ſomething of Their Own.  To a Reader of the For-\
mer Claſs, I thought it ſuﬃcient to give him the Subﬅance\
of ſo many Curious Papers, in ſuch Order as would beﬅ Suit\
with the Courſe of thoſe Studies that might Denominate\
him a *General Scholar :*  But for the ſake of the Latter, I have,\
in the Margin, given the Title and Author of each Paper, [**2\*1^v^:**]\
and Direed to the *Number* and *Page* of the *Tranſaions* or\
*Coeions*, where he may meet with the Original it ſelf.\
To the former I degn’d this *Abridgment* to be as Uſe-\
ful as the Volumes at large, and to ſerve the Latter in-\
ﬅead of a not inconvenient *Repertorium :*  And in the Pro-\
ſecution of this Degn, I have Generally conﬁned my ſelf to\
theſe *Rules*.

 I. I have not only *Retain’d* the Eential Parts of the\
Diſcourſes, but I have kept in many Places to the very *Words*\
of the Authors, only Varying them a little, to preſerve the\
Conneion ; For I thought it very Unwarrantable to Ob-\
trude any thing of mine , under the Name of another\
Perſon.

 II. But to *Shorten* the Whole Work, wherever I found\
any *Perſonal Addrees*, Long and Unneceary *Excurons*,\
or Pompous *Citations* of Books, I have taken the Liberty to\
*Suppreſs* them ; yet, I hope, without Injuring the Force of\
the Author’s Reaſoning.

 III. I have *Omitted* all *Accounts* and *Extras* of *Books*,\
which now, after ſo many Years Publication, ſeem almoﬅ\
Uſeleſs : Yet to put the Readers in mind of them, eſpecially\
ſuch as are about to Furi or Enlarge their Libraries, I\
have added a *Catalogue* at the End of each Chapter, to\
which they chieﬂy belong ; And I have alſo Direed them\
to ſuch *Additions, Emendations,* or *Refutations,* as ought to\
be conſulted, when thoſe Books fall under their Exami-\
nation.

[**2\*2^r^:**]

 IV. I have alſo *Omitted* all *Heads* of *Enquiries*, and Ex-\
periments mply Propos’d, without further Proſecution ;\
Believing that the Anſwers already given to many of\
them, and other Diſcourſes upon the ſame, or like Sub-\
jes, will ſuﬃciently Dire the Notice of an Inquitive\
Reader.

 V. The Previous Calculations of *Eclipſes, Lunar Appulſes,*\
and *Sateite Eclipſes* and *Occultations* ; alſo *Tide-Tables*, and\
many other Curious Papers of that kind ; have long ago\
Out-liv’d the Reaſon of their Publication,

 VI. All Simple *Catalogues* of *Natural Curioties*, (as of\
*Shes, Minerals, Plants, Animals,* &c.) without particular\
Deſcriptions of them, are little Inﬅrucive *:* And Chieﬂy\
ſerve to enlarge the Hiﬅory of the *Muſæum* where they are\
Depoted ; which is no part of the Degn of theſe Vo-\
lumes.

 VII. I have commonly *Omitted* ſuch Papers as have been\
*Coeed* into Juﬅ Volumes by their own Authors.  For\
this Reaſon I have *Omitted* ſome of thoſe Surpring\
*Microſcopical Diſco\swashv{}eries* by the Famous M. *Leeuwenhoeck :*\
But I further confeſs, I was alſo leſs inclin’d to Inſert them\
here, becauſe moﬅ of them Treat of Subjes not at all Con-\
venient *(*in my Opinion*)* for Common Readers.

 VIII. But to do all the Right I could to the Ingeni-\
ous Authors of thoſe Papers, which the Limits of this [**2\*2^v^:**]\
*Abridgment* oblig’d me to *Omit*, I have at the End of each\
Chapter Annexed their *Titles,* and ſometimes a ort *Ac-*\
*count* of them.

 Theſe are the *Rules* I have carefully Obſerved through\
the whole Condu of this Work : Wherein I have\
Faithfully Aim’d at the *General Good* of all Sorts of Rea-\
ders.  If I have fail’d in the Performance, ’tis for want of\
Judgment to do it better :  But I am bold to ſay, That if a\
Kind Reception of this all encourage a like *Abridgment* of\
the *Foreign Philoſophical \swashJ{}ournals*, in the Same Order, it\
will much Facilitate the many *Diſco\swashv{}eries* ﬅill Ready to\
Reward the *Labours* and *Expences* of all Induﬅrious *Pro-*\
*moters* of *Natural \swashK{}nowledge*.

[*rule 106.3*]

\TranscriptionStop

**a1^r^–m2^r^:** [**General contents, containing:**]

\SubordinateStart

**a1^r^–c2^r^:** [*two rules 5.0 × 126.3, 128.0*] \| THE [**/3.3/**] \| CONTENTS. [**9.3**] \| VOL. I. [**/6.3/**] \| MATHEMATICS. [**6.0**] \| [*rule 127.0*] \| […]

**c2^r^–f2^r^:** […] \| [*two rules 5.0 × 125.0, 110.3+1.0+13.0*] \| VOL. II. [**/6.6/**] \| […]

**f2^r^–h2^r^:** […] \| [*two rules 4.0 × 124.3, 109.6+1.0+12.3*] \| VOL. III. Part I. [**/6.6/4.3**] \| *Anatomy. \swashP{}hyck. Chymiﬅry.* [**7.0/5.0/3.3**] \| [*rule 126.0*] \| […]

**h2^r^–i2^r^:** […] \| [*two rules 4.0 × 125.3, 110.3+1.0+12.3*] \| VOL. III. Part II. [**/7.0/5.0**] \| *Philology. Miſceanies.* [**7.0/5.0/3.3**] \| [*rule 129.0*] \| […]

**i2^r^–m2^r^:** [*two rules 4.0 × 126.3, 111.3+1.0+13.0*] \| AN [**/5.3/**] \| Alphabetical Index, [**16.0/11.0/7.3**] \| OF THE [**/3.3/**] \| Names of the AUTHORS: [**/7.0/5.0**] \| WHEREIN, [**/3.3/**]

\TranscriptionStart

The *Numeral Letters* diﬅingui the *Volumes*, and the *Fi-* [**6.0/4.0/2.6 (120)**]\
‌ *gures* gniﬁe the *Pages :* The Numbers following im-\
‌ mediately after the *Names*, dire to the *Papers Abridg’d*\
‌ and *Inſerted* ; thoſe after \*, to the *Titles* of *Books* ; and\
thoſe after \*\*, to the *Titles* of the *Papers Omitted.*

[…*two column contents continue*]

\TranscriptionStop

**m2^r^:** […] \| The End of the INDEX [**/4.3/3.0**].

\SubordinateStop

**m2^v^:** [*blank*]

**n1^r^:** [**Volume title page identical to general, except:**]

[*…*] \| Diſpos’d under [General Heads.]{.smallcaps} [**7.3/5.0/3.0**] \| [*rule 118.3*] \|
VOL. I. [**/6.0/**]\| [*rule 119.0*] \| Containing all the [**6.0/4.0/2.6**] \| *Mathematical* Papers. [**9.0/6.3/4.6**] \|
[*rule*, 118] \| By *\swashJ{}OHN LOWTHORP*, M. A. [**6.0/4.0/2.6**] \| [*…*]

**n1^v^:** [*blank*]

**n2^r^:** [**Volume dedication**]

\TranscriptionStart

[*all but last line centered:*] To the [Honourable]{.smallcaps} [**/5.0/3.0**]\
Sir *ISAAC NEWTON* Kt. [**/7.0/5.0**]\
PRESIDENT, [**/5.3/**]\
And to the [**/4.3/2.6**]\
COUNCIL and FELLOWS [**/6.6/5.0**]\
OF THE [**/4.3/**]\
Royal Society [**15.6/11.0/7.3**]\
OF [**/4.0/**]\
LONDON, [**12.0**]\
FOR THE [**/3.0/**]\
Advancement of *Natural Kno\swashw{}ledge,* [**7.0/5.0/3.0**]\
THESE [**/3.0/**]\
*Mathematical* Papers, [**9.0/6.6/5.0**]\
ABRIDG’D [**8.0**]\
AND [**/3.0/**]\
Diſpos’d under [General Heads,]{.smallcaps} [**7.3/5.3/3.3**]\
Are Moﬅ Humbly Dedicated [**6.0/4.0/3.0**],

         By *\swashJ{}ohn Lo\swashw{}thorp.* [**7.3/5.3/3.0**]

\TranscriptionStop

**n2^v^:** [*blank*]

**B1^r^ (1)–X2^v^ (708):** [*two rules, 5.3 × 116.0, 115.6*] \| THE [**/3.3/**] \|
\dontleavehmode{\fraktur Mathematical Papers,} [**11.3/10.0/7.6**] \|
[Publish’d]{.smallcaps} and [Dispers’d]{.smallcaps} [**/4.0/3.0**] \| IN THE [**/3.0/**] \|
[Philosophical]{.smallcaps} [**/5.0/3.0**] \| Tranſaions and Colleions, [**/7.0/4.6**] \|
ABRIDG’D; [**/5.0/**] \| AND [**/3.0/**] \| Diſpos’d under [General Heads.]{.smallcaps} [**7.3/5.0/3.3**] \|

\SubordinateStart

**B1^r^ (1)–Q4^v^ (120):** [*rule, 46.0+0.3+68.0*] \| CHAP. I. [**/4.0/**] \| *Geometry, Algebra, Arithmetick, Logarithmotechny.* [**6.0/4.0/2.3**]

\SubordinateStart

**Facing 54 (H3^v^):** Geometric figures for pp. 7 (B4^r^)–55 (H4^r^):
‘*Plate 1. Vol.1.* \| *Fig. 1. [2–42]* \| *Page 54*’ [**engraved, 198 × 339
(205 × 354 ; sheet, 238 × 368 ; originally folded left to right 162v, 128m, 78)**] [**17 (7 [197] 4) 14 × 6 (1 [ 158 ∨^2^ 91 ∧^1^ 97] 4) 11 UkLoRS LM 191d**]  [**8 (1 [199] 1) 2 × 7 (2 [137 ∧^2^ 101 ∨^1^ 102] 3) 19 US-icu**]

\SubordinateStop

**Q4^v^ (120)–R4^v^ (127):** […] \| [*rule*, 59.3+0.3+53.0] \| CHAP. II. [**/4.0/**] \| *Trigonometry, Sur\swashv{}eying.* [**6.0/4.0/2.3**]

**R4^r^ (128)–2E4^r^ (215):** [*rule, 119.6*] \| CHAP. III. [**/4.3/**] \| *OPTICKS.* [**/4.0/**]

\SubordinateStart

**Facing 190 (2B3^v^):** Mostly geometric figures, for pp. 56 (H4^v^)–189 (2B3^r^): ‘*Plate. 2. Vol.1.* \| *Fig. 43. [44–98]* \| *Page 190*’  [**engraved, 198 × 295 (201 x 350 ; sheet, 239 × 367 ; folded left to right 163v, 125m, 79)**] [**22(1[200]0)16×8(4[155∨^2^126∧^1^64]2)10 UkLoRS LM 191d**] [**8 (1 [199] 1) 4 × 13 (3 [139 ∧^2^ 101 ∨^1^ 101] 1) 11 US-icu**]

\SubordinateStop

**2E4^v^ (216)–3M4^v^ (544):** [*rule, 116.0*] \| CHAP. IV. [**/4.3/**] \| *ASTRONOM\swashY{}.* [**/4.0/**]

\SubordinateStart

**Facing 388 (3D2^v^):** Geometric figures, astronomical figures, and
astronomical instruments diagrams, for pp. 197 (2C3^r^)–401 (3F1^r^):
‘*\swashP{}late 3. \swashV{}ol.1.* \| *Fig. 99. [100–141]* \| *Page
388*’ [**engraved, 197 × 344 (201 × 354 ; sheet, 239 × 364 ; folded left to right 164v, 125m, 75)**] [**14(1[199]2)26×6(2[161∨^2^126∧^1^62]4)9 UkLoRS LM 191d**]  [**9 (2 [196] 1) 3 × 17 (1 [94 ∧^1^ 106 ∨^2^ 145] 3) 4 US-icu**]

\SubordinateStop

**4A1^r^ (545)–4H2^r^ (603) :** [*rule, 114.6*] \| CHAP. V. [**/4.3/**] \| *Mechanicks. Acouﬅicks.* [**6.0/4.0/2.3**]

\SubordinateStart

**Facing 556 (4B2^v^):** Astronomical figures, geometrical figures,
and clock diagrams, for pp. 404 (3F2^v^)–554 (4B1^r^):
‘*\swashP{}late 4. Vol.1.1* \| *Fig. 142. [143–164]* \| *\swashP{}age
556*’ [**engraved, 197 × 341 (212 × 349 ; sheet, 240 x 369 ; folded
left to right 161v, 127m, 81)**]
[**26(3[196]2)17×2(5[153∨^2^127∧^1^67]3)9 UkLoRS LM 191d**] [**10 (2
[196] 2) 5 × 9 (5 [139 ∨^2^ 103 ∧^1^ 102] 2) 11 US-icu**]

\SubordinateStop

**4H2^r^ (603)–4M1^v^ (634):** […] \| [*rule, 116.0*] \| CHAP. VI. [**/4.3/**] \| *Hydroﬅaticks. Hydraulicks* [**6.0/4.0/2.3**]

\SubordinateStart

**Facing 626 (4L1^v^):** Geometric figures and diagrams of
apparatuses, for pp. 557 (4B3^r^)–626 (4L1^v^): ‘*Plate 5. Vol.1.* \|
*\swashF{}ig. 168. [167–197]* \| *\swashP{}ag. 262*’ [**engraved, 197
× 346 (200 × 353 ; sheet, 240 × 369 ; originally folded left to right 161v, 123m, 85)**] [**15(3[199]2)22×0(2[160∨^2^125∧^1^63]4)11 UkLoRS LM 191d**] [**11 (2 [197] 1) 2 × 5 (1 [149 ∨^2^ 101 ∧^1^ 94] 3) 16 US-icu**]

\SubordinateStop

**4M1^v^ (634)–4R2^r^ (675):** […] \| [*rule 115.3*] \| CHAP. VII. [**/4.3/**] \| *Geography. Na\swashv{}igation.* [**6.0/4.0/2.3**]

\SubordinateStart

**Facing 660 (4P2^v^):** Maps and diagram, for pp. 633 (4M1^r^), 659
(4P2^r^)–660 (4P2^v^)‘*\swashP{}late 7. [* i.e. 6 *]
Vol.1. Page. 660.* \| *\swashF{}ig. 204. [205]*’ [**engraved, 261 ×
294(263 × 299 ; sheet, 289 × 314 ; folded bottom to top 75v then left
to right 158v, 70m, 69m, 17)**]
[**22(3[190∨^1^70]2)4×7(3[152∨^2^70∧^3^74]3)4 UkLoRS LM 191d**] [**13
(2 [185 ∨^1^ 73] 2) 4 × 49 (2 [111 ∨^3^ 95 ∧^2^ 85] 1) 16; arms of Amsterdam watermark and ‘RW’ countermark US-icu**]

\SubordinateStop

**4R2^v^ (676)–4S3^r^ (685):** [*rule 112.3*] \| CHAP. VIII. [**/5.3/**] \| *\swashA{}rchiteure. Ship-Building.* [**7.0/5.0/3.0**]

**4S3^v^ (686)–4T3^r^ (693):** [*rule 114.6*] \| CHAP. IX. [**/5.3/**] \| *Perſpei\swashv{}e. Sculpture. Painting.* [**7.0/5.3/3.0**]

**4T3^v^ (694)–4X2^v^ (708):** [*rule, 113.0*] \| CHAP. X. [**/5.3/**] \| *Muck.* [**6.6/5.3/3.0**]

\SubordinateStart

**Facing 698 (4U1^v^):** Geometric figures, diagrams, and musical
notation, for pp. 627 (4L2^r^)–696 (4T4^v^)
‘*\swashP{}late. 7. Vol.1. \| \swashF{}ig. 198. [199–203, 206–231]*’ plate number ‘7’ over mostly erased ‘6’
[**engraved, 197 × 347 (202 × 350 ; sheet, 238 × 362 ; originally folded left to right 160v, 129m, 73)**] [**15(3[196]2)21×0(0[158∨^2^129∧^1^61]2)10 UkLoRS LM 191d**]  [**13 (2 [196] 0) 3 × 11 (2 [139 ∨^2^ 102 ∧^1^ 101] 4) 9 US-icu**]

\SubordinateStop

**4X2^v^ (708):** […] \| [*rule 114.3*] \| FINIS. [**/4.0/**] \| [*rule 116.3*]

\SubordinateStop

\BiblHangingIndentsStop

### Volume 2 contents

\BiblHangingIndentsStart

**A1^r^:** THE \| PHILOSOPHICAL \| TRANSACTIONS \| AND \| COLLECTIONS,
\| To the End of the Year 1700. \| ABRIDG’D \| AND \| Diſpos’d under
[General Heads]{.smallcaps}. \| [*rule*] \| VOL. II. \| [*rule*] \|
Containing all the \| *Phyological* Papers. \| [*rule*] \| By
*\swashJ{}OHN LOWTHO\swashR{}\swashP{}*, M. A. \| and F. R. S. \|
[*rule*] \| *LO\swashN{}\swashD{}O\swashN{}*: \| Printed for *Thomas Bennet*
at the *Half-Moon*, *Robert \swashK{}naplock* at \| the *Angel* and
*Crown*, and *Richard Wilkin* at the *\swashK{}ing*’s-*Head*, \| in
St. *Paul*’s Church-yard. M D C C V.

\TranscriptionStart

THE [**/3.6/**]

PHILOSOPHICAL [**/5.3/**]

TRANSACTIONS [**/10.0/**]

AND [**/3.0/**]

COLLECTIONS, [**7.3**]

To the End of the Year 1700. [**5.3/4.0/3.0**]

ABRIDG’D [**10**]

AND [**/3.0/**]

Diſpos’d under [General Heads.]{.smallcaps} [**8.0/5.3/3.6**]

[*rule 115.3*]

VOL. II. [**7.0**]

[*rule 116.0*]

Containing all the [**5.6/4.0/2.6**]

*Phyological* Papers. [**9.0/6.6/5.0**]

[*rule 115.0*]

By *\swashJ{}OHN LOWTHO\swashR{}\swashP{}*, M. A. [**5.6/4.0/2.6**]

and F. R. S. [**5.6/4.0/2.3**]

[*rule 115*]

*LO\swashN{}\swashD{}O\swashN{}*:  [**/4.0/**]

Printed for *Thomas Bennet* at the *Half-Moon*, *Robert \swashK{}naplock* at [**5.0/3.3/2.3**]\
‌ the *Angel* and *Crown*, and *Richard Wilkin* at the *\swashK{}ing*’s-*Head*,\
‌ in St. *Paul*’s Church-yard.  M D C C V.

\TranscriptionStop

**A1^v^:** [*blank*]

**A2^r^:** [**Volume dedication:**]

\TranscriptionStart

To His GRACE [**/4.0/2.6**]

The DUKE [**/6.6/4.6**]

OF [**/3.3/**]

ORMOND, [**11.6**]

LORD LIEUTENANT [**/5.3/**]

OF [**/3.3/**]

IRELAND, *🙰c*. [**9.0**]

THESE [**/3.6/**]

*Phyological* Papers, [**9.0/6.6/4.6**]

ABRIDG’D [**/7.0/**]

AND [**/3.3/**]

Diſpos’d under [General Heads]{.smallcaps}, [**7.3/5.3/4.0**]

‌        Are with all Poble [**6.0/4.3/3.0**]\
‌         Humility and Gratitude,\
‌              Dedicated, by

‌           *\swashJ{}ohn Lowthorp.* [**9.6/6.3/4.6**]

\TranscriptionStop

**A2^v^:** [*blank*]

**1 (B1^r^)–915 (6A2^r^):** [*two rules 4.0 × 114.3, 114.6*] \| THE [**/3.3/**] \| \dontleavehmode{\fraktur
Phyſiological Papers,} [**11.3/9.0/7.3**] \| [Publish’d]{.smallcaps} and
[Dispers’d]{.smallcaps} [**/4.0/2.6**]\| IN THE [**/3.3/**] \| PHILOSOPHICAL [**/4.0/**] \| Tranſaions and
Colleions, [**/7.0/5.0**]\| ABRIDG’D ; [**/5.3/**] \| AND [**/3.3/**] \| Diſpos’d under [General
Heads]{.smallcaps}. [**7.3/5.3/4.0**] \|

\SubordinateStart

**1 (B1^r^)–257 (2L1^r^):** [*rule 114.0*] \| CHAP. I. [**/4.0/**] \| *Phyology.* [**7.3/5.0/3.3**] \| *Meteorology.
\swashP{}neumatic\swashk{}s.*  [**5.0/4.0/2.6**]\| […]

\SubordinateStart

**Facing 42 (G1^v^):** Instrument and geometrical diagrams, for pp. 11
(C2^r^)–42 (G1^v^) ‘*\swashP{}late. 1 Vol. 2. Pag. 42 \|
\swashF{}ig. 1. [2–15]*’ [**engraved, 199 × 347 (203 × 353 ; sheet, 238 × 362 ; folded left to right  167v, 118m, 77)**] [**20(1[202]1)17×0(2[162∨^2^119∧^1^67]3)8**]

**Facing 164 (Y2^v^):** Chart, map, view, instrument diagram, and
illustrations, for pp. 42 (G1^v^)–164 (Y2^v^)
‘*Plate 2. Vol. 2. \swashP{}ag. 164. \| \swashF{}ig. 17. [18–22]*’
[**engraved, 195.0×352.3 (202×355 ; sheet, 238×363 ; folded left to right 168v, 129m, 66)**] [**21(3[194]3)15×3(1[165∨^2^129∧^1^58]2)9**]

**Facing 204 (2D2^v^):** Geometric and astronomical diagrams, for pp. 166 (Y3^v^)–203 (2D2^r^) ‘*\swashP{}late 3. Vol. 2 \swashP{}ag. 204. \| \swashF{}ig. 23. [24–44]*’ [**engraved 196.3×349.0 (201×355.0 ; sheet 238.0×366 ; folded left to right 169v, 125m, 72**] [**13(2[198]1)22×5(3[166∨^2^125∧^1^64]2)9**]

\SubordinateStop

**257 (2L1^r^)–366 (3A3^v^):** […] \| [*rule 117.0*] \|
CHAP. II. [**/4.0/**] \| *Hydrology.*  [**5.3/4.0/2.6**]\| […]

\SubordinateStart

**Facing 318 (2S3^v^):** Instrument and geometrical diagrams, views,
and subterranean plan, for pp. 258 (2L1^v^)–316 (2S2^v^) ‘*\swashP{}late 4. \|
Vol. 2. \| Pag. 318. \| \swashF{}ig. 45. [46–62]*’ [**engraved 199.0×356.0 (201×361 ; sheet 238.0×374 ; originally folded left to right 171v, 132m, 71**]  [**18(1[199]3)17×7(2[164\|94\|100]2)6**] [**20(1[199]3)18×18(1[164∨^2^130∧^1^62]2)6**]

\SubordinateStop

**367 (3A4^r^)–600 (4G4^v^):** CHAP. III. [**/4.0/**] \| *Mineralogy.* [**6.0/4.0/2.6**] \| […]

\SubordinateStart

**Facing 510 (3T2^v^):** Illustrations of specimens, instrument
diagrams, microscopical views, and subterranean plans, for pp. 354
(2Z1^v^)–510 (3T3^v^), 511 (3T4^r^) ‘*\swashP{}late 5  Vol. 2. \|
Pag. 510. \| \swashF{}ig. 64. [65–128, 149–50]*’ [**engraved
199.0×256.0 (201×262 ; sheet 239×365 ; originally folded left to right
164v, 129m, 72**] [**18(1[199]0)20×7(2[157∨^2^128∧^1^63]2)10**]

**Facing 512 (3T4^v^):** Illustrations of specimens, for pp. 511
(3T4^r^) ‘*\swashP{}late 6. Vol. 2. \swashP{}ag. 512. \|
\swashF{}ig. 130. [131–144]*’ [**engraved 199×347 (201×355 ; sheet
239×366 ; originally folded left to right 166v, 132m, 68**]
[**18(3[198]2)20×9(2[156∨^2^132∧^1^62]3)9**]

**Facing 518 (3U3^v^):** Views and illustration of a specimen, for
pp. 515 (3U2^r^)–531 (3Y2^r^) ‘*\swashP{}late 7
Vol. 2. \swashP{}ag. 518. \| \swashF{}ig. 151. [152–6]*’
[**engraved 195×343 (201×349 ; sheet 238×362 ; folded left to right 162v, 125m, 75)**] [**20(2[196]5)17×1(2[160∨^2^125∧^1^62]3)9**]

\SubordinateStop

**601 (4H1^r^)–623 (4K4^r^):** CHAP. IV. [**/4.0/**] \| Magneticks. [**6.0/4.0/2.6**] \| […]

**623 (4K4^r^)–756 (5D2^v^):** […] \| [*rule 117.3*] \|
CHAP. V. [**/4.0/**] \| *\swashB{}otany.  Agriculture.*
[**6.0/4.0/2.6**] \| […]

\SubordinateStart

**Facing 662 (4P3^v^):** Illustrations of specimens, for pp. 625
(4K4^r^)–659 (4P2^r^) ‘*\swashP{}late. 8. Vol. 2. \swashP{}ag. 662.*
\| *\swashF{}ig. 157. [158–65]*’ [**engraved 345×196 (262×201.6 ;
sheet 366×239 ; originally folded bottom to top 165v, 124m, 77)**]
[**9(2[198]2)29×8(3[159∨^2^125∧^1^65]2)10**]

\SubordinateStop

**756 (5D2^v^)–915 (6A2^r^):** […] \| [*rule 118.0*] \|
CHAP. VI. [**/4.0/**] \| *Zoology.* [**6.0/4.0/2.6**] \| […]

\SubordinateStart

**Facing 776 (5F4^v^):** Illustrations of specimens, for pp. 663
(4P4^r^) – 672 (4Q4^v^) ‘*\swashP{}late 9 Vol. 2. \swashP{}ag. 776. \|
\swashF{}ig. 166. [167–173]*’ [**engraved 195×348 (204×358 ; sheet
239×371 ; originally folded left to right 168v, 131m, 72)**]
[**22(6[199]3)12×7(1[161∨^2^130∧^1^58]5)11**]

**Facing 776 (5F4^v^):** Illustrations of specimens, apparatuses, and
architecture, for pp. 672 (4Q4^v^) – 776 (5F4^v^)
‘*\swashP{}late 10. Vol. 2. Pag. 776. \| \swashF{}ig. 173. [174–189]*’
[**engraved 204×352 (205×359 ; sheet 239×332 ; folded left to right
162v, 134m, 36**] [**16(1[204]1)18×8(5[153∨^2^134∧^1^66]2)9**]

**Facing 812 (5L2^v^):** Illustrations of specimens and geometrical
diagrams, for pp. 781? (5G3^r^)–811
(5L2^r^)‘*\swashP{}late 11. Vol. 2. Pag. 812.* \|
*\swashF{}ig. 190. [191–216]*’ [**engraved 201×342 (207×354 ; sheet
238×365 ; folded left to right 164v, 127m, 74)**]
[**21(2[201]5)11×1(1[162∨^2^126∧^1^61]5)11**]

**Facing 872 (5S4^v^):** Illustrations of specimens and diagrams, for
pp. 818 (5M1^v^)–871 (5S4^r^) ‘*\swashP{}late 12. Vol. 2.* \|
*\swashP{}ag 872* \| *\swashF{}ig. 217 [218–247]*’ [**engraved 196×346
(205×358 ; sheet 238×371 ; folded left to right 164v, 132m, 75)**]
[**15(2[200]3)20×8(5[152∨^2^132∧^1^65]3)10**]

**Facing 904 (5Y4^v^):** Illustrations of mammals and mammal bones,
organs, and dissection, for pp. 873 (5T1^r^)–903 (5Y4^r^)
‘*\swashP{}late 13. Vol.2 . swashP{}ag.* \|
*\swashF{}ig. 248. [249–272, 276–7, 279]*’ [**engraved 202×350
(206×358 ; sheet 238×371 ; folded left to right 165v, 127m, 79)**]
[**14(1[203]3)19×7(1[159∨^2^128∧^1^69]3)11**]

**Facing 904 (5Y4^v^):** Illustrations of deformed animals, for
pp. 899 (5Y2^r^)–903 (5Y4^r^) ‘*\swashP{}late 14. Vol. 2. Pg. 904.* \|
*\swashF{}ig. 273. [274–5, 278]*’ [**engraved 192×210 (205×230 ; sheet
239×293 ; folded left to right 169v, 61)**]
[**17(2[193]9)20×29(4[136∨73]2)50**]

\SubordinateStop

**915 (6A2^r^):** […] \| [*rule 113.3*] \| *FINIS.* [**/5.0/**]

\SubordinateStop

**916 (6A2^v^):** [*blank*]

\BiblHangingIndentsStop

### Volume 3 contents

\BiblHangingIndentsStart

**A1^r^:** THE \| PHILOSOPHICAL \| Tranſaions and Colleions \| To
the End of the Year 1700. \| ABRIDG’D \| AND \| Diſpos’d under
[General Heads]{.smallcaps}. \| [*rule*] \| VOL. III. In Two Parts. \|
[*rule*] \| The firﬅ Containing all the \| *Anatomical Medical* and
*Chymical*, \| And the Second all the \| *Philological* and
*Miſcellaneous* Papers. \| [*rule*] \| By *\swashJ{}OHN LOWTHORP*,
M. A. and F. R. S. \| [*rule*] \| *LONDON:* \| Printed for *Thomas
Bennet,* at the *Half Moon* ; *Robert Knaplock*, at \| the *Angel* and
*Crown* ; and *Richard Wilkin,* at the *King*’s-*Head* ; \| in
St. *Paul*’s Church-yard. 1705.

\TranscriptionStart

THE [**/3.6/**]

PHILOSOPHICAL [**/8.3/**]

Tranſaions and Colleions [**/7.0/4.6**]

To the End of the Year 1700. [**6.0/4.3/3.0**]

ABRIDG’D [**7.0**]

AND [**/3.0/**]

Diſpos’d under [General Heads]{.smallcaps}. [**7.3/5.0/3.6**]

[*rule 120.0*]

VOL. III. In Two Parts. [**/5.3/3.6**]

[*rule 122.6*]

The firﬅ Containing all the [**4.6/3.0/2.0**]

*Anatomical Medical* and *Chymical*, [**8.6/6.6/4.3**]

And the Second all the [**4.6/3.0/2.0**]

*Philological* and *Miſcellaneous* Papers. [**7.0/5.0/3.6**]

[*rule 122.6*]

By *\swashJ{}OHN LOWTHORP*, M. A. and F. R. S. [**6.0/4.0/2.6**]

[*rule 122.3*]

*LONDON:* [**/3.6/**]

Printed for *Thomas Bennet,* at the *Half Moon* ; *Robert Knaplock*, at [**5.0/3.6/2.3**]\
‌ the *Angel* and *Crown* ; and *Richard Wilkin,* at the *King*’s-*Head* ;\
‌ in St. *Paul*’s Church-yard. 1705.

\TranscriptionStop

**A1^v^:** [*blank*]

**A2^r^:** [**Volume dedication:**]

\TranscriptionStart

[*two rules 6.0 × 122.3, 123.0*]

[*next ten lines centered:*]

To the Right Honourable [**7.0/5.3/4.0**]

*ROBERT HARLEY,* Eſq; [**9.0/7.0/**]

ONE OF [**/4.3/**]

Her Majeﬅy’s [**17.0/12.3/7.6**]

Principal Secretaries of State, *🙰c.* [**9.0/7.0/5.0**]

THESE [**/4.3/**]

*Medical* and *Philological* Papers [**9.0/6.6/5.0**]

ABRIDG’D [**/5.3/**]

AND [**/3.0/**]

Diſpos’d under [General Heads,]{.smallcaps} [**7.6/5.0/3.6**]

‌        Are Moﬅ Humbly [**7.6/5.6/4.0**]\
‌              Dedicated, by\
‌                *\swashJ{}ohn Lowthorp. [**10.0/7.0/5.0**]*

\TranscriptionStop

**A2^v^:** [*blank*]

**1 (B1^r^)–371 (3B2^r^):** [*two rules 4.0 × 116.3, 116.6*] \| THE [**/3.3/**] \| \dontleavehmode{\fraktur
Anatomical, Medical, and Chymical Papers,} [**6.0/5.0/4.0**]\| [Publish’d]{.smallcaps}
and [Dispers’d]{.smallcaps} [**/5.3/3.6**]\| IN THE [**/3.0/**] \| Philoſophical Transaions [**9.0/7.0/4.6**] \|
AND [**/3.0/**] \| COLLECTIONS [**/8.0/**] \| ABRIDG’D, [**/5.3/**]\| AND [**/3.0/**] \| Diſpos’d under [General
Heads]{.smallcaps}. [**7.0/5.3/4.0**] \|

\SubordinateStart

**1 (B1^r^)–23 (D4^r^):** [*rule 117.0*] \| CHAP. I. [**/4.3/**] \| *Anatomy. Phyck.* [**9.0/6.3/4.0**] \| *The
Struure, External Parts, and common Teguments,* [**6.0/4.0/2.3**] \| *of Humane
Bodies.* \| […]

**23 (D4^r^)–60 (I2^v^):** […] \| [*rule 112.6*] \| CHAP. II. [**/4.3/**] \| *The Head.* [**/5.0/3.0**] \| […]

**61 (I3^r^)–81 (M1^r^):** CHAP. III. [**/4.3/**] \| *The Neck.  The Thorax.* [**/4.0/2.6**]\| […]

**81 (M1^r^)–225 (2G1^r^):** […] \| [*rule 118.0*] \| CHAP. IV. [**/4.3/**] \| *The Abdomen.* [**/4.0/2.6**] \| […]

\SubordinateStart

**Facing 120 (Q4^v^):** Illustrations of specimens, for pp. 3
(B2^r^)–118 (Q3^v^) ‘*\swashP{}late 1. Vol. 3. Pag. 120.* \|
*\swashF{}ig. 1 [2–15]*’ [**engraved 201×348 (202×355 ; sheet 239×370
; folded left to right 166v, 127m, 77)**]
[**19(1[201]1)16×10(0[160∨^2^127∧^1^67]1)9 UkLoRS LM 191d**]
[**18(2[200]0)24×11(0[147∨^2^105∧^1^102]0)3 Uk 740.h.5**]

**Facing 130 (S1^v^):** Illustrations of specimens, for pp. 124
(R2^v^)–129 (S1^r^) ‘*\swashP{}late 2. Vol.3. \swashP{}ag. 130.* \|
*\swashF{}ig. 16 [17–21]*’ [**engraved 201×350 (202×355 ; sheet
239×360 ; folded left to right 170v, 127m, 63)**] [*Fig. 16 on p. 129,
Fig. 21 on 124*] [**19(2[200]3)17×6(2[165∨^2^127∧^1^59]3)6 UkLoRS LM 191d**]
[**17(3[199]0)25×10(2[149∨^2^103∧^1^96]3)1 Uk 740.h.5**]

**Facing 162 (Y1^v^):** Illustrations of specimens, for pp. 130
(S1^v^)–162 (Y1^v^) ‘*\swashP{}late 3. Vol. 3. \swashP{}ag.162.* \|
*\swashF{}ig. 22 [23–43]* ’ [**engraved 201×354 (200×356 ; sheet
239×363 ; folded left to right, 164v, 134m, 65)**] [*Fig. 39 on p. 182
(2A3^v^)*] [**17(2[201]0)21×5(2[164∨^2^134∧^1^58]2)7 UkLoRS LM 191d**]
[**30(1[199]0)5×14(2[140∨^2^105∧^1^107]0)0 Uk 740.h.5**]

**Facing 210 (2E1^v^):** Illustrations of specimens, for pp. 183
(2A4^r^)–210 (2E1^v^) ‘*\swashP{}late 4.Vol.3.\swashP{}ag.210.* \|
*\swashF{}ig. 44. [45–59]*’ [**engraved 198×349 (210×353 ; sheet
239×365 ; folded left to right 164v, 130m, 71)**]
[**19(2[199]2)17×8(4[158∨^2^130∧^1^61]2)9 UkLoRS LM 191d**]
[**22(2[195]2)17×14(4[142∨^2^105∧^1^100]2)2 Uk 740.h.5**]

\SubordinateStop

**225 (2G1^r^)–292 (2P2^v^):** […] \| [*rule 111.3*]
CHAP. V. [**/4.3/**] \| *The Humours, and General Aﬀeions of the
Body.* [**6.0/4.0/2.6**] \| […]

**293 (2P3^r^)–300 (2Q2^v^):** CHAP. VI. [**/4.3/**] \| *The Bones, \swashJ{}oints, and Muſcles.* [**6.0/4.0/2.6**] \| […]

**301 (2Q3^r^)–306 (2R1^v^):** CHAP. VII. [**/4.3/**] \| *Monﬅers.* [**6.0/4.0/2.6**] \| […]

**306 (2R1^v^)–312 (2R4^v^):** […] \| [*rule 76.0+0.3+41.0*] \| CHAP. VIII. [**/4.3/**] \| *The Period of Human Life.* [**6.0/4.0/2.6**]\| […]

**312 (2R4^v^)–371 (3B2^r^):** […] \| [*rule 116.0*] \| CHAP. IX. [**/4.3/**] \| *Pharmacy.  Chymiﬅry.* [**6.0/4.0/2.6/**] \| […]

\SubordinateStart

**Facing 320 (2S4^v^):** Illustrations of specimens and an apparatus,
for pp. 212 (2E2^v^)–320 (2S4^v^) ‘*\swashP{}late 5  Vol.3. Pag.320*
\| *\swashF{}ig. 60 [61–83]*’ [**engraved 200×352 (201×353 ; sheet
239×367 ; folded left to right 171v, 133m, 63)**]
[**19(1[198]2)19×12(3[157∨^2^134∧^1^60]1)5 UkLoRS LM 191d**]
[**20(1[196]2)21×13(2[142∨^2^108∧^1^101]1)4 Uk 740.h.5**]

\SubordinateStop

**371 (3B2^r^):** […] \| [*rule 116.0*] \| *FINIS.* [**/4.0/**]

\SubordinateStop

**372 (3B2^v^):** [*blank*]

**373 (3B3^r^)–688 (4S4^v^):** [*two rules 4.0 × 116.3, 116.3*] \| THE [**/3./6**] \| \| \dontleavehmode{\fraktur Philological and Miſcellaneous Papers ,} [**6.0/5.0/4.0**] \| [Publish’d]{.smallcaps} and [Dispers’d]{.smallcaps} [**/5.3/3.0**] \| IN THE [**/3.0/**] \| Philoſophical Tranſaions [**9.0/7.0/5.0**] \| AND [**/3.0/**] \| COLLECTIONS [**/8.0/**] \| ABRIDG’D [**5.3**] \| AND [**/3.0/**] \| Diſpos’d under [General Heads.]{.smallcaps} [**7.3/5.3/4.0**] \|

\SubordinateStart

**373 (3B3^r^)–398 (3E3^v^):** [*rule 117.0*] \| CHAP. I. [**/4.3/**] \| *PHILOLOGY.* [**/7.0/**] \| *Grammar.* [**/5.0/3.3**] \| […]

**398 (3E3^v^)–532 (3Y2^v^):** […] \| [*rule 117.0*] \| CHAP. II. [**/4.3/**] \| *Chronolgy.  Hiﬅory.  Antiquities.* [**6.0/4.0/2.6**] \| […]

\SubordinateStart

**Facing 442 (3L1^v^):** Illustrations of script, carving, and
epigraphy, for pp. 382 (3C3^v^)–423 (3H4^r^)
‘*\swashP{}art 2. Vol.3. \swashP{}late 1.* \| *\swashP{}ag. 442 .* \|
*\swashF{}ig. 1. [2–10]*’ [*Fig. 1 called “plate” in text.*]
[**engraved 199x357 (202×358 ; sheet 239×371 ; folded left to right
168v, 132m, 71)**] [**9(1[202]2)26×7(0[163∨^2^131∧^1^60]1)10 UkLoRS LM 191d**]
[**16(1[201]1)20×7(1[150∨^2^104∧^1^100]2)3 Uk 740.h.5**]

**Facing 442 (3L1^v^):** Illustrations of epigraphy and script, for
pp. 423 (3H4^r^)–441 (3L1^r^) ‘*\swashP{}art 2. Vol.3. \swashP{}late
2 .* \| *\swashP{}ag.442.* \| *\swashF{}ig. 11. [12–30]*’ [**engraved
200×354 (203×359 ; sheet 239×371 ; folded left to right 169v, 134m,
68)**] [**11(1[200]2)25×4(3[165∨^2^133∧^1^57]3)8 UkLoRS LM 191d**] [**20(1[199]1)21×10(2[155∨^2^103∧^1^97]3)3 Uk 740.h.5**]

**Facing 526 (3X3^v^):** Illustrations of artifacts and script, for
pp. 442 (3L2^r^)–526 (3X3^v^) ‘*Vol 3. \swashP{}art 2.\swashP{}late
3.\swashP{}ag.526.* \| *\swashF{}ig. 31. [32–65]*’[**engraved 198×354
(200×358 ; sheet 238×370 ; folded left to right 170v, 132m, 68**]
[**14(3[199]1)22×6(3[165∨^2^131∧^1^59]2)8 UkLoRS LM 191d**]
[**22(3[198]1)19×16(3[145∨^2^114∧^1^94]2)16 Uk 740.h.5**]

**Facing 526 (3X3^v^):** View, for p. 496 (3R4^v^)
‘*Vol.3. \swashP{}art 2.\swashP{}late 4.\swashP{}ag.526.* \|
*\swashF{}ig. 63.*’ [**engraved 146×704(351+352) (154,156×716(357+359)
; sheets 239×720(361+370); folded left to right 167v(4th), 145v(3rd),
144m(2nd), 153m(1st), 111**]
[**29(3[146]5)56×9(2[159∨^4^146∨^3^49] below 11, above:
28(3[146]5)56×6(6[95∧^2^153∧^1^105]7+ cut UkLoRS LM 191d**]
[**40(2[145]5)43×5(8[140∨^4^120∨^3^90] below 13, above: 39(3[145]7)45×cut[16∧^2^115∧^1^111]5+cut) Uk 740.h.5**]

**Facing 528 (3X4^v^):** Illustrations of scripts, for pp. 526
(3X3^v^)–527 (3X4^r^) ‘*Vol. 3
\swashP{}art 2. \swashP{}late 5. Pag.528.* \| *\swashF{}ig. 66
[67–71]*’ [**engraved 328×197 (352×206 ; 368×239 sheet ; folded bottom
to top 163v, 142m, 63)**] [**9(6[47∧^1^142∨^2^138]22)6×9(4[195]6)25 UkLoRS LM 191d**] [**23(9[191∨^1^130]14)10×6(5[136∨^2^66]8)1 Uk 740.h.5**]

\SubordinateStop

**533 (3Y3^r^)–634 (4M1^v^):** CHAP. III. [**/4.3/**] \| *Voyages and Travels.* [**6.0/4.0/2.6**]\| […]

**634 (4M1^v^)–688 (4S4^v^):** […] \| [*rule 118.0*] \| CHAP. IV. [**/4.3/**] \| *Miſceaneous Papers.* [**7.0/5.0/3.0**] \| […]

\SubordinateStart

**Facing 682 (4S1^v^):** View, for p. 527 (3X4^r^)
‘*Vol. 3. \swashP{}art 2. \swashP{}late 6. Pag. 682.* \|
*\swashF{}ig.72.*’ [**engraved 196×416 (197×420 ; sheet 238×394 ;
folded left to right 136v, 146m, 112**]
[**14(3[197]7)17×6(4[160∨^2^144∧^1^108]3)2 UkLoRS LM 191d**]
[**18(3[197]7)20×21(3[143∨^2^135∧^1^129]3)2 Uk 740.h.5**]

**Facing 682 (4S1^v^):** Illustrations of artifacts and view, for
pp. 527 (3X4^r^)–682 (4S1^v^) ‘*Vol.3.\swashP{}art
2.\swashP{}late 7. Pag.682.* \| *\swashF{}ig. 73 [73–83]*’ [**engraved
202×351 (206×357 ; 239×371 sheet ; folded left to right 169v, 135m,
67)**] [**12(3[202]2)19×6(4[160∨^2^134∧^1^59]2)7 UkLoRS LM 191d**] [**25(3[199]1)13×7(5[165∨^2^97∧^1^89]1)5 Uk 740.h.5**]

\SubordinateStop

**688 (4S4^v^):** […] \| [*rule 117.0*] \| *FINIS.* [**/6.6/**] \| [*rule 62.0+0.6+54.3*] 

\SubordinateStop

**a1^r^:** [*two rules 5.0 × 61.3+1.0+62.3, 77.6+1.0+47.0*] \| A
GENERAL [**/3.6/**] \| INDEX [**/10.0/**] \| OF ALL [**/3.6/**] \| The
MATTERS, contain’d in theſe Volumes. [**/4.6/3.0**]\| [*rule 62.6+1.0+63.3*] \|

\TranscriptionStart

Note, *That the Numeral Letters denote the* Volume, *and the Figures the* Page. [**4.3/3.0/2.0**]

[…]

\TranscriptionStop

**e2^r^:** […] \| *FINIS.* [**/5.0/**] \| [*rule 125.0*] \|

**e2^r^:** *ADVERTISEMENT.* [**/3.0/**] \|

\TranscriptionStart

T^2^HE Antiquities of *Canterbury* in two parts. 1*ﬅ*. The Antiquities of *Canterbury*, [**4.0/3.0/2.0 (72.0)**]\
‌  or a Survey of that Ancient City , with the Suburbs and Cathedral. By *Wiiam*\
*Sommer.*  2*d*. Cantuaria Sacra or the Antiquities.  1. Of the Cathedral.  2. Of the\
[…]

\TranscriptionStop

**e2^v^:** [*blank*]

\BiblHangingIndentsStop

### Headings

This table measures the typical type sizes of the various section.
Measurements are taken from the first page, unless noted in
parentheses following the measurement.  The measurements of the
typeface follow Vander Meulen (TODO citation) where the face height is
given, then a slash, then the capital height, then another slash, then
the x height.  After that, I give the equivalent of the twenty-line
measurement pioneered by Pollard in the BMC for text measurements as a
way of estimating the actual body size combined with leading. (TODO
citation) In cases where only some measurements can be found, those
absent are left blank.  For titling typefaces, the capital height
fills the entire body height and they lack a lowercase, so only a
single measurement is given without slashes.  The layout is measured
for the text page, excluding the direction line, running heads, page
numbers, and marginal notes; then the layout is measured including
those in parentheses following Bowers (TODO citation), but with the
modification that I write the left margin first, followed by a pipe,
then the right margin; for the height, I write upper, type, and lower
margin.  For sections in multiple columns, a single column is given
first, then the width of the text page with both together and their
rule in parentheses, then the page with the running title, direction
line, and pagination.  The kind of break is noted, whether or not the
preceding page appears to have been spaced to force a page break (p.),
or even a blank page added to force a page break on the recto or verso
(p.^r^ or p.^v^).  It further notes if the section is distinguished by
a double-rule (dr.) or rule (r.) preceding it.  Empty columns indicate
a feature is present, but a measurement wasn’t taken because the
feature is unusual in some way.  For features absent on particular
pages, the column contains a dash, ‘—’.

For example, with the title page and imprimatur, a blank preceeds the
imprimatur, so that the imprint both occurs before the title and the
title occurs on its expected recto.  Since if the aim was merely to
proceed the title with the imprimatur, it could have been put recto as
it is in some books.  The recto for a title page is so conventional
that a deviation would be shocking here, thus, it seems probable the
designer of the layout meant the imprint to face the title page.

\bgroup\ttxx\setupinterlinespace[1ex]

|         |       | Heads    | Text  | Layout         | Break           | Running |
|---------|-------|----------|-------|----------------|-----------------|---------|
|         |       |          |       |                |                 |         |
| \*1^v^  | impr. | 9/7/5,   | —     | 12 ll.,        | p.^v^           | —       |
|         |       | /6/,     |       | 32\|156\|47 ×  |                 |         |
|         |       | 8/5/3,   |       | 34\|117\|32    |                 |         |
|         |       | 6/4/3    |       |                |                 |         |
|         |       |          |       |                |                 |         |
| \*2^r^  | t.p.  | 9,       | —     | 19 ll.,        | p.^r^           | —       |
|         |       | 7/5/3,   |       | 3\|187\|47 ×   |                 |         |
|         |       | 5/4/2    |       | 12\|120\|54    |                 |         |
|         |       |          |       |                |                 |         |
| \*3^r^  | dedi. | 15,      | 7/5/3 | 5 ll.,         | p.^r^, dr.      | —       |
|         |       | 15/11/7, | (164) | 11\|171\|50 ×  |                 |         |
|         |       | 8/6/4    |       | 14\|110\|60    |                 |         |
|         |       |          |       | (11\|180\|41   |                 |         |
|         |       |          |       | × 14\|110\|60) |                 |         |
|         |       |          |       |                |                 |         |
| \*3^v^  | —     | —        | 7/5/3 | 21 ll.[^10],   |                 | 7/5/3   |
|         |       |          | (164) | 14\|171\|53 ×  |                 |         |
|         |       |          |       | 63\|109\|9     |                 |         |
|         |       |          |       | (3\|188\|43 ×  |                 |         |
|         |       |          |       | 63\|109\|9)    |                 |         |
|         |       |          |       |                |                 |         |
| 2\*1^r^ | pref. | 12, 5    | 5/3/2 | 20 ll.,        | p.^r?^[^2], dr. | —       |
|         |       |          | (111) | 23\|160\|55 ×  |                 |         |
|         |       |          |       | 18\|109\|61    |                 |         |
|         |       |          |       | (23\|165\|50 × |                 |         |
|         |       |          |       | 19\|109\|61)   |                 |         |
|         |       |          |       |                |                 |         |
| 2\*1^v^ | —     | —        | —     | 30 ll.,        |                 | /4/2    |
|         |       |          |       | 9\|166\|58 ×   |                 |         |
|         |       |          |       | 63\|109\|14    |                 |         |
|         |       |          |       | (7\|190\|51    |                 |         |
|         |       |          |       | × 63\|109\|14) |                 |         |
|         |       |          |       |                |                 |         |
: Preliminary sections

\egroup

[^10]: This counts blanks lines between paragraphs when they’re set at
    the same height as the rest.


[^2]: This section starts on the rectos, as is the convention, but the
    in the absence of a preceding blank page, I cannot think of
    demonstration to prove this was planned.

\bgroup\ttxx\setupinterlinespace[1.5ex]

|       |             | Heads      | Text      | Layout              | Break           | Running |
|-------|-------------|------------|-----------|---------------------|-----------------|---------|
|       |             |            |           |                     |                 |         |
|       |             |            |           |                     |                 |         |
| a1^r^ | cont.       | 9          | —         | —                   | p.^r?^[^2], dr. | —       |
|       |             |            |           |                     |                 |         |
| a1^r^ | v. 1        | /7/[^5]    | —         | —                   |                 | —       |
|       |             |            |           |                     |                 |         |
| a1^r^ | ch. 1, 2,   | /3/,       | 4/2/1.5   | 2 col. 30 ll.,      | r.[^3]          | —       |
|       | etc.        | 4/3/2      |           | 19\|181\|38 ×       |                 |         |
|       |             |            |           | 9\|62\|4\|62\|49    |                 |         |
|       |             |            |           | (19\|183\|35 ×      |                 |         |
|       |             |            |           | 9\|62\|4\|62\|49)   |                 |         |
|       |             |            |           |                     |                 |         |
|       |             |            |           |                     |                 |         |
| b1^r^ | —           | —          | —         | 2 col. 56 ll.,      |                 | /4/3    |
|       |             |            |           | 13\|186\|44 ×       |                 |         |
|       |             |            |           | 14\|62\|3\|62\|48   |                 |         |
|       |             |            |           | (5\|196\|31 ×       |                 |         |
|       |             |            |           | 14\|127\|48)        |                 |         |
|       |             |            |           |                     |                 |         |
| c2^r^ | v. 2        | /7/        | –         | —                   | dr.             | /4/3    |
|       |             |            |           |                     |                 |         |
| c2^r^ | ch. 1       | /3/,       | 4/2/1.5   | *complex*           |                 | /4/3    |
|       |             | 4/3/2      |           |                     |                 |         |
|       |             |            |           |                     |                 |         |
| c2^v^ | —           | —          | —         | appx. as b1^r^.[^6] |                 | /4/3    |
|       |             |            |           |                     |                 |         |
| d1^v^ | ch. 2, 3,   | /3/,       | 4/2/1.5   | 48 ll. 2 col.,      | pr.[^7]         | /4/3    |
|       | etc.        | 4/3/2      |           | 17\|182\|40 ×       |                 |         |
|       |             |            |           | 50\|62\|3\|62\|9    |                 |         |
|       |             |            |           | (5\|196\|36 ×       |                 |         |
|       |             |            |           | 51\|127\|9)         |                 |         |
|       |             |            |           |                     |                 |         |
| f1^r^ | —           | —          | —         | appx. as b1^r^      |                 | /4/3    |
|       |             |            |           |                     |                 |         |
|       |             |            |           |                     |                 |         |
| f2^r^ | v. 3, pt. 1 | /7/4,      | —         | *complex*           | dr.             | /4/3    |
|       |             | 7/5/3      |           |                     |                 |         |
|       |             |            |           |                     |                 |         |
| f2^r^ | ch. 1, 2,   | /3/,       | 4/2/1.5   | *complex*           | r.              | /4/3    |
|       | etc.        | 4/3/2      |           |                     |                 |         |
|       |             |            |           |                     |                 |         |
| g1^v^ | —           | —          | —         | appx. as b1^r^      |                 | /4/3    |
|       |             |            |           |                     |                 |         |
| h2^r^ | v. 3, pt. 2 | /7/4,      | —         | *complex*           | dr.             | /4/3    |
|       |             | 7/5/3      |           |                     |                 |         |
|       |             |            |           |                     |                 |         |
| h2^r^ | ch. 1, 2,   | /3/,       | 4/2/1.5   | *complex*           | r.              | /4/3    |
|       | etc.        | 4/3/2      |           |                     |                 |         |
|       |             |            |           |                     |                 |         |
| h2^v^ | —           | —          | —         | appx. as b1^r^      |                 | /4/3    |
|       |             |            |           |                     |                 |         |
| i2^r^ | al. indx.   | 15/11/7,   | 4/2/1.5   | 21 ll. 2 col.,      | p. dr.          | —       |
|       |             | /7/4,      |           | 21\|179\|39 ×       |                 |         |
|       |             | /5/,       |           | 9\|62\|3\|62\|50    |                 |         |
|       |             | 6/4/3[^8], |           | (21\|182\|34 ×      |                 |         |
|       |             | /3/        |           | 9\|62\|3\|62\|50)   |                 |         |
|       |             |            |           |                     |                 |         |
| i2^v^ | —           | —          | —         | appx. as b1^r^      |                 | /4/3    |
|       |             |            |           |                     |                 |         |
| n1^r^ | v. t.p.     | *as t.p.*  | *as t.p.* | 7\|193\|39 ×        | p.^r^           | —       |
|       |             |            |           | 19\|120\|52         |                 |         |
|       |             |            |           | (7\|197\|35 ×       |                 |         |
|       |             |            |           | 19\|120\|52)        |                 |         |
|       |             |            |           |                     |                 |         |
|       |             |            |           |                     |                 |         |
| n2^r^ | v. ded.     | 12,        |           |                     |                 |         |
|       |             | 16/11/7,   |           | 8\|180\|46 ×        | p.^r^           | —       |
|       |             | 9/11/5,    |           | 25\|110\|54         |                 |         |
|       |             | 7/5/3,     |           |                     |                 |         |
|       |             | 6/4/2      |           |                     |                 |         |
|       |             |            |           |                     |                 |         |
: General contents and volume title

\egroup

[^9]: This line in blackletter, so the capitals descend.  The
    measurements here are thus (face=capital)/(ascender of h and l)/(x
    height).

[^8]: The argument is measured here, as is ‘The End of the INDEX’ (m2^r^)

[^7]: The headings for v. 1 ch. 5; v. 2 ch. 2; v. 3 pt. 1 ch. 2, 5;
    v. 3 pt. 2 ch. 3, 4, all occur on a new page, but wouldn’t have
    fit on the preceding with enough entries to make them clear.

[^6]: The rules between columns vary by a mm in either direction, this
    one is actually 189 mm; the columns and widths also vary by mm,
    but by gathering, not by heading.  See next table.

[^5]: “VOL. I.”

[^3]: This chapter heading begins not on a new page, but the same page
    after a rule.  This is the notation being used throughout.

\bgroup\ttxx\setupinterlinespace[1ex]

|              |            | Heads       | Text        | Layout               | Break     | Running | cont.  |
|--------------|------------|-------------|-------------|----------------------|-----------|---------|--------|
|              |            |             |             |                      |           |         |        |
| 1 (B1^r^)    | v. 1 head. | 11/9/7[^9], | 4/3/2,      | 6 ll. (text),        | p.^r^ dr. | 6//3    | math,  |
|              |            | /6/5,       | 3/2/1[^11]  | 22\|171\|45 ×        |           |         | Latin  |
|              |            | 5,          |             | 18\|115\|56          |           |         |        |
|              |            | 8/5/3       |             | (5\|197\|36 ×        |           |         |        |
|              |            | /4/2,       |             | 18\|135\|36)         |           |         |        |
|              |            |             |             |                      |           |         |        |
| 2 (B1^v^)    | –          | –           | 4/3/2       | 45 ll.,              | –         | 6//3    |        |
|              |            |             | (81)        | 18\|183\|37 ×        |           |         |        |
|              |            |             |             | 55\|116\|13          |           |         |        |
|              |            |             |             | (6\|197\|34 ×        |           |         |        |
|              |            |             |             | 55\|116\|13)         |           |         |        |
|              |            |             |             |                      |           |         |        |
| 120 (Q4^v^)  | ch. 2      | 6/4/2       | 4/3/2       | 30 ll. (text),       | r.        | 6//3    | –      |
|              |            |             | (81),       | 13\|183\|41 ×        |           |         |        |
|              |            |             | 3/2/1       | 59\|116\|13          |           |         |        |
|              |            |             |             | (7\|195\|36 ×        |           |         |        |
|              |            |             |             | 39\|136\|13)         |           |         |        |
|              |            |             |             |                      |           |         |        |
| 125 (R3^r^)  | –          | –           | 4/3/2       | 46 ll.,              | —         | 6//3    |        |
|              |            |             | (81),       | 13\|187\|37 ×        |           |         |        |
|              |            |             | 3/2/1       | 15\|120\|55          |           |         |        |
|              |            |             |             | (16\|198\|34 ×       |           |         |        |
|              |            |             |             | 15\|141\|33)         |           |         |        |
|              |            |             |             |                      |           |         |        |
| 128 (R4^v^)  | ch. 3      | /4/         | 4/3/2       | 38 ll. (text),       | r.        | 6//3    | Latin, |
|              |            |             | (81),       | 21\|180\|36 ×        |           |         | math,  |
|              |            |             | 3/2/1       | 44\|120\|18          |           |         | tbl.   |
|              |            |             |             | (7\|199\|32 ×        |           |         |        |
|              |            |             |             | 31\|140\|18)         |           |         |        |
|              |            |             |             |                      |           |         |        |
| 129 (S1^r^)  | —          | —           | 4/3/2       | 45 ll.,              | —         | 6//3    |        |
|              |            |             | (81),       | 13\|183\|42 ×        |           |         |        |
|              |            |             | 3/2/1       | 18\|116\|56          | —         |         |        |
|              |            |             |             | (6\|193\|39 ×        |           |         |        |
|              |            |             |             | 18\|135\|37)         |           |         |        |
|              |            |             |             |                      |           |         |        |
| 216 (2E4^v^) | ch. 4      | /4/         | 4/3/2       | 37 ll.,              | r.        | 6//3    | Latin, |
|              |            |             | (81),       | 19\|182\|37 ×        |           |         | tbls., |
|              |            |             | 3/2/1       | 60\|116\|18          |           |         | astr.  |
|              |            |             |             | (6\|200\|32 ×        |           |         |        |
|              |            |             |             | 36\|137\|18)         |           |         |        |
|              |            |             |             |                      |           |         |        |
| 219 (2F2^r^) | —          | —           | 4/3/2       | 46 ll.,              | –         | 6//3    |        |
|              |            |             | (81),       | 15\|189\|35 ×        |           |         |        |
|              |            |             | 3/2/1       | 17\|116\|57          |           |         |        |
|              |            |             |             | (7\|199\|32 ×        |           |         |        |
|              |            |             |             | 15\|134\|38)         |           |         |        |
|              |            |             |             |                      |           |         |        |
| 545 (4A1^r^) | ch. 5      | 6/4/2       | 4/3/2       | 27 ll.,              | r.        | 6//3    | Latin, |
|              |            |             | (81)        | 21\|174.6\|42.0 ×    |           |         | math   |
|              |            |             |             | 18.0\|116.0\|55.4    |           |         |        |
|              |            |             |             | (8.0\|198.3\|32.0 ×  |           |         |        |
|              |            |             |             | 18.0\|135.0\|35.3)   |           |         |        |
|              |            |             |             |                      |           |         |        |
| 554 (4B1^v^) | –          | –           | 4.0/2.6/1.6 | 46 ll.,              | —         | 5.6//3  |        |
|              |            |             | (81)        | 15.0\|187.6\|36.0 ×  |           |         |        |
|              |            |             |             | 55.6\|116.0\|17.0    |           |         |        |
|              |            |             |             | (7.0\|199.0\|32.0  × |           |         |        |
|              |            |             |             | 36.0\|135.0\|17.0)   |           |         |        |
: Main text

[^11]: Marginal notes.

\egroup

### Skeleton formes

Measuring \$1r,v and \$4r,v, to calculate furniture, total measure,
furniture, total measure, furniture, from left to right looking at the
forme with the first leaf in the lower right for the outer form, with
the first leaf in the lower left for the inner forme.  Measures for
all four pages.  In cases where shoulder notes are missing, I estimate
using the printers’ measure and the apparent width of notes in that
forme.

B(o) 38.0 \| 135.3 \| 23.9 \| 136.0 \| 36.0;
B(i) ~34.0 \| ~136.3 \| 25.6 \| 136.3 \| 37.0;
B measure 116.0

C(o) ~39.7 \| ~133.3 \| 27.6 \| 133.3 \| 39.3;
C(i) 35.0 \| 136.3 \| 29.0 \| 139.3 \| 35.0;
C measure 117.0 (C1^v^ 117.3)

D(o) 42.0 \| 131.0 \| 26.0 \| ~131.6 \| ~41.0;
D(i) 33.0 \| 140.3 \| 26.0 \| 131.6 \| 18.3;
D measure 117.6 (D4^v^ 117.0)
*Measures D(o,i)1,5 probably over because of the short notes.  Measures D(o,i)2,4 probably underestimates.*

E(o) 36.0 \| 135.0 \| 26.0 \| 135.0 \| 36.0;
E(i) ~34.3 \| ~136.0 \| 27.3 \| 136.0 \| 38.0;
E measure 115.3 (E4^v^ 115.0);
*Distinctive squashed foot of right parentheses D3^r^ (21) matching E3^r^ (29), suggest D(o) and E(o) same forme.*

F(o) ~38.0 \| ~135.3 \| 26.0 \| 136.0 \| 38.3
F(i) 37.3 \| 135.0 \| 24.3 \| 135.3 \| 40.3
F measure 116.3 (F1^r^ 117, F4^r^ 116.0)

G(o) ~42.6 \| ~131.4 \| 26.3 \| 131.0 \| 43.0
G(i) 42.0 \| 131.0 \| 26.0 \| ~131.0 \| ~43.0
G measure 117.0 (G1^r^ 166.6)

4A(o) 36.3 \| 135.6 \| 28.0 \| 135.3 \| 35.3
4A(i) ~36.0 \| ~136.3 \| 27.0 \| 135.6 \| 36.3
4A measure 116.3(i) 116.0(o)

4B(o) 39.0 \| 134.6 \| 28.6 \| 135.0 \| 35
4B(i) 36.0 \| 135.0 \| 29.0 \| 132.0 \| 41.0
4B measure 115.0(o) 115.6(i) (4B1^v^ 116.0, 4B4^r^ 115.3)
*Measures 4B(o,i)1,5 probably over because of the short notes.  Measures 4B(o,i)2,4 probably underestimates.*

4C(o) ~39.3 \| ~133.6 \| 28.0 \| 133.6 \| 35.0
4C(i) ~35.3 \| ~133.6 \| 28.0 \| 132.6 \| 40.3
4C measure 116 (4C1^v^ 117.0)
*Measures 4C(o,i)1,5 probably over because of the short notes.  Measures 4C(o,i)2,4 probably underestimates.*

4D(o) 37.0 \| 135.0 \| 29.0 \| 134.0 \| 38.6
4D(i) 38.3 \| 136.0 \| 27.0 \| 132.0 \| 39.0
4D measure 116.0
*Measures 4D(o,i)1,5 probably over because of the short notes.  Measures 4D(o,i)2,4 probably underestimates.*

4E(o) 37.3 \| 135.6 \| 29.0 \| 145.3 \| 26.0
4E(i) ~36.7 \| ~135.3 \| 29.0 \| 135.3 \| 39.0
4E measure 116.0

4F(o) 38.0 \| 136.0 \| 28.0 \| 134.0 \| 36.3
4F(i) 35.0 \| 135.0 \| 28.0 \| 136.0 \| 39.0
4F measure 116.0 (4F4^v^ 117)

4G(o) ~37.0 \| ~136.0 \| 26.0 \| 136.0 \| 37.0
4G(i) 39.0 \| 137.0 \| 28.0 \| 136.0 \| 35.0
4G measure 117.0 (4G1^v^ 118.0)

4H(o) 36.0 \| 136.0 \| 27.0 \| ~136.0 \| ~38.3
4H(i) 36.6 \| 136.0 \| 27.0 \| ~136.0 \| ~37.0
4H measure 117.0

4I(o) ~38.0 \| ~135.0 \| 28.0 \| 135.0 \| 38.0
4I(i) 38.6 \| 135.6 \| 28.0 \| 135.6 \| 36.4
4I measure 117.0

4K(o) ~36.6 \| ~136.3 \| 27.0 \| ~135.7 \| ~35.6
4K(i) ~35.6 \| 136.3 \| 27.0 \| 136.0 \| 37.0
4K measure 117 (4K1^r^ 116.3, 4K4^r^ 116.6)

4L(o) 35.0 \| 136.0 \| 28.0 \| 136.0 \| 37.0
4L(i) 37.0 \| 136.6 \| 29.0 \| 137.0 \| 34.0
4L measure 117.0

4M(o) 40.0 \| 136.0 \| 28.0 \| 135.6 \| 34.0
4M(i) 33.0 \| 135.6 \| 30.0 \| 135.0 \| 39.0
4M measure 117.0 and 116.3

4N(o) 39.0 \| 136.0 \| 31.0 \| 135.0 \| 37.0
4N(i) ~34.6 \| ~135.0 \| 31.0 \| 135.0 \| 41.0
4N measure 117

4O(o) 39.0 \| 135.3 \| 30.0 \| 136.0 \| 35.0
4O(i) 36.0 \| 136.0 \| 29.3 \| 132.3 \| 42.0
4O measure 116.0 (4O1^r^ 115.0,  4O1^v^ 116.3)

4P(o) bis 38.0 \| 135.6 \| 31.0 \| 136.3 \| 35.0
4P(i) 36.0 \| 137.0 \| 30.3 \| 131.3 \| 43.0
4P measure 117

4P(o) 38.0 \| 135.6 \| 28.6 \| 135.3 \| 35.0
4P(i) 39.0 \| 135.0 \| 26.0 \| 131.0 \| 42.3
4P measure 117.0 (4P1^v^ 116.0)

### Squashed foot tracing

*Distinctive squashed foot of right parentheses D3^r^ (21) matching
E3^r^ (29), F1^r^ (33), G2^r^ (43), H1^r^(49), I1^r^ (57), K1^r^ (65),
L1^r^ (73), M1^r^ (81), N1^r^ (89), O1^r^ (97), P1^r^ (105), R2^r^
(123), T2^r^ (139), X2^r^ (155), Z1^r^ (169), 2B4^r^ (191), 2D4^r^
(207), 2F1^r^ (217), 2I2^r^ (243), 2L2^r^ (259), 2N1^r^ (273), 2O1^r^
(281), 2P2^r^ (291), 2Q3^r^ (301), 2R2^r^ (307), 2S2^r^ (315), 2T3^r^
(325), 2U1^r^ (329), 2Y2^r^ (347), 3A1^r^ (361), 3B2^r^ (371), 3D1^v^ (386), 3E1^v^ (394), 3F1^v^ (402), 3G1^v^ (410), 3I4^v^ (432), 3K4^v^ (440), 3L4^v^ (448), 3M4^v^ (544), 4P4^r^ (663), 4Q3^v^ (670), 4S3^v^ (686), 4Y4^v^ (704)

*Third leaf:* D(o), E(o);

*First leaf:* F(o), G(i), H(o), I(o), K(o), L(o), M(o), N(o), O(o), P(o);

*More formes created*

*First leaf, four formes:* R(i), T(i), X(i);

*First leaf, four formes:* Z(o);

*Third leaf, four formes:* 2B4(i), 2D4(i);

*First leaf, four formes:* 2F1(o);

*First leaf, four formes:* 2I(i), 2L(i);

*First leaf, four formes:* 2N(o);

*First leaf, two formes:* 2O(o), 2P(i);

*Second leaf, two formes:* 2Q(o); 

*First leaf, two formes:* 2R(i), 2S(i);

*Second leaf, two formes:* *2T(o);

*First leaf, two formes:* 2U(o);

*First leaf, four formes:* 2Y(i);

*First leaf, two formes:* 3A(o), 3B(i);

*Fourth leaf, two formes:* 3D(i), 3E(i), 3F(i), 3G(i);

*Fourth leaf, two formes:* 3I(o), 3K(o), 3L(o), 3M(o);

*Big gap, shared printing?:* catch word on 4N4^v^ (648) in italics as
matches the typographical style of 4A–4N, but the table on 4O1^r^
(649) is roman with names in italics.  Additionally, item XII refers
correctly back to observations in chapter 4, section 45, but not by
page number, so presumably the astronomy section had been numbered.
These inter-chapter references seem to only occur in this section,
back to astronomy.  The item, however, is the article immediately
preceding it in the *Transactions*.

*Fourth leaf, two formes:* 4P(i);

*Third leaf, four formes:* 4Q(i), 4S(i);

*Fourth leaf, four formes:* 4Y(o),

### Thoughts on order of printing

B–C lack the bent parentheses and have the standard style, but use the
section mark within an abridged text is absent through the rest to
refer to the first sub-chapter heading, on N4^v^ (96) for example, the
section mark is used to reference that heading, which lacks the mark
itself, looks like similar stock to 4A-4N, higher quality.

4A–4N seems like one unit, one design, with evidence of gaps in the work or shared printing at either end.  Typographically, it matches  D–P and refers back to references in 2E ff.

4P–4X have larger sized headers, but also the bent parentheses that appears in D–3M, post printed specimen?

D-P match the style of 4A-4N, but also have the bent parentheses, D1^r^ must be printed after C4^v^ (TODO, check?) as it’s the middle of a text.

Q–3M have the bent parentheses, but introduce a new headline style (at
R4^r^) that differs from both the specimen and previous, Q1^r^ after P4^v^? (math, yes)

Two candidates come to mind for the specimen: B-C would be typical, even just B; 4A-4N seems much longer… what about printing B-C then 4A-4N, then D-Q, then R–3M and 4O–4X last?

B-C and 4A-4N (18 sheets) are the only sections lacking the bent
parentheses and both begin on first-leaf rectos, the only two in the
volume.

D-Q follows C and matches typographically, 4O-4X enlarges the subheadings to match the specimen, and R-3M takes on a different style

B-C -> D-Q -> Q-3M

4A -> 3M (content)

D -> 4P-4X (bent parentheses)

4A-4N -> (or different printer) D otherwise we would have seen the bent parentheses

B looks like Thomas Warren’s printing style


### Paper stocks

Kinds of paper and measurements:

Watermark ‘8’ in corner of sheet.

B: {3 \| 17 \| 25 \| 23 \| 24 \| 24 \| 26 \| 25 \| 26 \| 23 \| 24} {18
\| 23 \| 26 \| 24 \| 26 \| 23 \| 26 \| 26 \| 23 \| 14 \| 10}

C: {2 \| 18 \| 23 \| 25 \| 23 \| 25 \| 25 \| 27 \| 23 \| 24 \| 24} {14
\| 25 \| 23 \| 26 \| 23 \| 26 \| 23 \| 25 \| 26 \| 15 \| 13}

D: {11 \| 14 \| 24 \| 23 \| 26 \| 25 \| 25 \| 25 \| 26 \| 26 \| 14}  {7
\| 21 \| 23 \| 25 \| 26 \| 25 \| 23 \| 26 \| 30 \| 21 \| 12}

E: {2 \| 5 \| 25 \| 24 \| 23 \| 23 \| 26 \| 24 \| 24 \| 26 \| 24 \|
13} {27 \| 23 \| 24 \| 25 \| 26 \| 24 \| 23 \| 24 \| 25 \| 16 \| 2}  *heavier, map-style thickness*

F: {9 \| 16 \| 24 \| 24 \| 25 \| 24 \| 25 \| 26 \| 22 \| 25 \| 19} {19
\| 25 \| 24 \| 25 \| 24 \| 26 \| 22 \| 24 \| 26 \| 15 \| 8}

G: {3 \| 17 \| 25 \| 23 \| 24 \| 25 \| 24 \| 24 \| 26 \| 24 \| 24} {16
\| 25 \| 24 \| 24 \| 25 \| 23 \| 26 \| 25 \| 24 \| 13 \| 14}

K watermark, K4, lower right: {2 \| 15 [ 3 \| 8 ] 6 \| [*the bottom of this watermark is damaged*]

X watermark, X2, lower right: {11 \| 0 [ 11 ] 4 \|

4A: {2 \| 17 \| 26 \| 23 \| 24 \| 23 \| 26 \| 25 \| 25 \| 24 \| 24}
{17 \| 23 \| 25 \| 24 \| 27 \| 23 \| 26 \| 24 \| 24 \| 14 \| 12}
Watermark 4A2, lower right: {9 [5 \| 8 ] 5 \|

4B: {15 \| 14 \| 24 \| 24 \| 23 \| 27 \| 26 \| 24 \| 25 \| 25 \| 12}
{27 \| 22 \| 23 \| 27 \| 23 \| 25 \| 24 \| 26 \| 26 \| 9 \| 7}
Watermark 4B4, lower right: {17 [ 1 \| 11 ] 4 |\

4C: {6 \| 17 \| 27 \| 23 \| 25 \| 23 \| 24 \| 25 \| 25 \| 23 \| 21} {18
\| 26 \| 26 \| 23 \| 25 \| 24 \| 25 \| 22 \| 23 \| 13 \| 14}
Watermark 4C2, lower right: {15 [ 4 \| 8 ] 5 \| 

4D: {13 \| 15 \| 24 \| 24 \| 23 \| 28 \| 24 \| 26 \| 24 \| 25} {13 \|
24 \| 23 \| 24 \| 26 \| 24 \| 24 \| 26 \| 24 \| 26 \| 17 \| 1}
Watermark 4D4, lower right: {14 [ 1 \| 11 ] 3 \|

*B, D, G, I, K, M, N, O, P, V, 2D, 2H, 2N, 2P, 4A, 4C, 4E, 4M, 4P, 4Q? are the same stock:* numeral ‘8’ with chainline close to middle

*C, E, F, H, L, Q, S, T, X, Y, Z, 2A, 2C, 2O, 4D, 2E, 2G, 2I, 2K, 2M, 4B, 4F, 4G, 4H, 4I, 4K, 4N, 4O, 4R, 4S, 4T, 4U are the same stock:* numeral ‘8’ tight to bottom chainline

*X is damaged of the tight to bottom chainline

*d, g, h, 2F, 3H, 3K, 4L*: initials ‘BS \| L’ all within a circle

*3G:* foot in circle

*3M:* footmark numeral 8, but totally new paper stock, chainline across the middle, but the numeral is one chainline up

*C, F match*

*2B, 2L lacks wm*

images of 4P4 vs 4A2 vs K4 -> K shows damage to the watermark! M3 V2 2D4

Vol. 1:

4A-4I no back mark, thin paper, nice rattle
4K back mark
4N, 4P, 4Q, 4R, 4S rust spots near backmark
4O browner

B-I,N–Q rust spots or back marks

a–m rough paper, dirty press work

*Proposal:* 12 \| 24 \| 23 \| 24 \| 24 \| 25 \| 21 \| 25 \|
25 \| 23 \| 12+.


Based on UkLWK copy, I think v.2 and v.3 were begun at the same time,
while some of the stock used for v.1 remained, but the plates were
printed then too(?)

## Description of next two volumes

\BiblHangingIndentsStart

**Uk 740.h.6:** Volume four in modern institutional quarter deep brown
(Centroid 56) morocco over strong orange yellow (Centroid 68) cloth
boards with bind crown typical of British Library institutional
bindings, binder’s stamp ‘B.M. 1971’; seven panel spine, panel 2 with
‘PHILOSOPHICAL \| TRANSACTIONS. \| 1700 – 1720 \| ABRIDG’D’, panel 4
‘VOL. 4 \| JONES’, smaller panel seven ‘LONDON \| 1731’; black
hexagonal British Museum ownership mark associated with Hans Sloane
‘MVEVM \| BRITAN \| NICVM’ on t.p. verso and last leaf verso, strong
reddish brown (Centroid 40) stamp of ‘W: Musgrave.’ on t.p. verso;
canceled shelf mark in pen “9 K k” on t.p. along with 8 in pencil,
imprimatur facing t.p. “K. Academies [etc?]” in pencil with check mark
below, imprimatur recto blank with penciled “740. h.6.” and “42” both
in upper left, p. 1 (B1^r^) 2227×179; verso of last leaf pencil
notation ‘Collated & Perfect \| J. R. \| 1783–.’; lower paste down
“PRESERVATION SERVICE” note.

**Uk 740.h.7:** Volume five in modern institutional quarter deep
yellow green (Centroid 118) pigskin over deep yellowish green
(Centroid 132) cloth, raised band spine with five panels, panel 1 with
two armored arms holding a ring, panel 2 ‘PHILOSOPHICAL \|
TRANSACTIONS. \| 1700–20. \| ABRIDG’D’, panel 3 ‘VOL. 5 \| JONES’,
panel 5 ‘LONDON. 1731’, modern pale yellow (Centroid 89) endpapers;
black hexagonal British Museum ownership mark associated with Hans
Sloane ‘MVSEVM \| BRITAN \| NICVM’ on t.p. verso and last leaf verso,
strong reddish brown (Centroid 40) stamp of ‘W: Musgrave.’ on
t.p. verson, penciled shelf marks: “9 K K” on t.p. upper right, number
“9” on t.p. lower right, “740.h.7” and “740.H.#7” on free endpaper
facing t.p., penciled ‘Collated & Perfect \| J.R. \| 1733.’ and ‘2’ on
verso of last leaf, pasted note of “RECORD OF TREATMENT, EXTRACTION
ETC.” for deacidification with magnisium bi-carbonate on second lower,
free endpaper; page 1 (B1^r^) 229×182.

THE \| PHILOSOPHICAL \| TRANSACTIONS \| *(From the Year* 1700 *to the Year* 1720.*)* \| ABRIDG’D, \| AND \| Diſpos’d under [General Heads.]{.smallcaps} \| [*rule*] \| In [Two Volumes]{.smallcaps} \| [*rule*] \| By *HENRY \swashJ{}ONES,* M. A. and \| Fellow of *King*’s *College* in *CAMBRIDGE*. \| [*rule*] \| [Vol. IV.]{.smallcaps} Containing \| Part I. The MATHEMATICAL Papers. \| Part II. The PHYSIOLOGICAL Papers. \| [*rule*] \| The SECOND EDITION. \| [*rule*] \| *LONDON:* \| Printed for J. and J. [Knapton,]{.smallcaps} D. [Midwinter]{.smallcaps} and A. \| [Ward,]{.smallcaps} A. [Bettesworth]{.smallcaps} and C. [Hitch,]{.smallcaps} W. [Innys,]{.smallcaps} \| F. [Fayram]{.smallcaps} and T. [Hatchett,]{.smallcaps} J. [Osborn]{.smallcaps} and T. \| [Longman,]{.smallcaps} J. [Pemberton,]{.smallcaps} C. [Rivington,]{.smallcaps} F. \| [Clay,]{.smallcaps} J. [Batley,]{.smallcaps} and R. [Hett.]{.smallcaps}  1731.

\BiblHangingIndentsStop

### Contents

The categories are somewhat similar to the previous

1. Mathematical papers
    1. Geometry, arithmetic, algebra, logarithmotechny
    2. Optics
    3. Astronomy
    4. Mechanics, acoustics
    5. Hydrostatics, hydraulics
    6. Geography, navigation
    7. Music

2. Physiological papers
    1. Physiology, meteorology, pneumatics
    2. Hydrology
    3. Mineralogy
    4. Magnetics
    5. Agriculture, botany

3. Zoology, &c. anatomy, physic, chymistry
    1. Zoology, and the anatomy of animals
    2. Anatomy, diseases
       1. General and skin
       2. Head
       3. Neck, thorax, heart
       4. Abdomen
       5. Humours and general affections of the body
       6. Bones, joints, and muscles
       7. Pharmacy, chymistry

4. Philology, miscellanies
     1. Manuscripts, printing
     2. Chronology, history, antiquities
     3. Travels, voyages
     4. Miscellaneous

## An alternate for the next two volumes

\BiblHangingIndentsStart

**UkLoRS LM 192a:** modern cloth bound by Maltby of Oxford with Royal Society bookplate… [continue]

THE \| PHILOSOPHICAL \| TRANSACTIONS \| From the Year M DCC. \| (Where Mr. *LOWTHORP* ends) \| To the Year M DCC XX. \| ABRIDG’D, \| AND \| Diſpos’d under General [Heads]{.smallcaps} \| [*rule*] \| In Two VOLUMES. \| [*rule*] \| *By* [Benj. Motte.]{.smallcaps} \| [*rule*] \| *LONDON:* \| Printed for R. [Wilkin,]{.smallcaps} R. [Robinson,]{.smallcaps}, S. [Ballard,]{.smallcaps} \| W. and J. [Innys,]{.smallcaps} and J. [Osborn.]{.smallcaps}  M DCC XXI.

**Facing t.p.:** [*rule*] \| I^2^ Have inſpeed the Epitome of the
*Phi-* \| *loſophical Tranſaions*, nce the Year \| 1700. made by
Mr. *Benjamin \swashM{}otte* ; and \| do believe that it is done by
him with due \| Care and Judgment. \| Witneſs my Hand, \| [*next two
lines in the same line as third:*] *July* 26, \| 1720. \|
*Edm. Halley*, Secr. Reg. Soc. \| [*rule*]

**Vol. 2:** [*As v.1 with:*] […] \| Diſpos’d under General [Heads.]{.smallcaps} \| [*rule*] \| *By* [Benj. Motte.]{.smallcaps} \| [*rule*] \| VOL. II. Containing \| Part III. The [Physiological]{.smallcaps} Papers. \| Part IV. The [Philosogical]{.smallcaps} Papers. \| [*rule*] \| *LONDON:* \| […]

\BiblHangingIndentsStop

### Contents

1. Mathematicks
   1. Geometry, algebra, logarithmotechny, arithmetick
   2. Opticks
   3. Astronomy
   4. Mechanicks, aucousticks
   5. Hydrostaticks, hydraulicks
   6. Geography, navigation and musick

2. Anatomical and medical
   1. Course of anatomy
   2. Head
   3. Neck, thorax
   4. Abdomen
   5. Veins, arteries and blood
   6. Bones, joynts, and muscles
   7. Monsters, logævity
   8. General affections of the body, and remarkable cases and observations not reducible to the foregoing heads
   9. Pharmacy, chymistry

3. Physiological
   1. Physiology, meteorology, pneumaticks
   2. Hydrology
   3. Mineralogy
   4. Magneticks
   5. Botany, agriculture
   6. Zoology

4. Philological and miscellaneous
   1. Chronology, history, antiquities
   2. Voyages and travels
   3. Miscellaneous


## Hutton’s 1809 Abridgment

The Contents Classed under General Heads (v.1, pp. xiii–xix)

1. Natural Philosophy—Acoustics, Astronomy, Hydraulics, Hydrostatics, Hydrology, Magnetics, Meteorology, Optics, Pneumatics.
2. Miscellanies—Agriculture, Antiquities, Architecture, Grammar, History, Music, Painting, Perspective, Sculpture, Travels, Voyages.
3. Anatomy, Physiology, Surgery, Medicine, Pharmacy, Chemistry, &c.
4. Natural History—Botany, Mineralogy, Zoology, &c.
5. Chronology, Geography, Mathematics, Mechanics, Navigation, &c.
6. Books, of which an Account is given in this Volume.
7. Biographical Notes of the Following Authors in this Volume.

v.2 varies the order

v.3

I. Mathematics
    1. Arithmetic, Political Arithmetic, Numbers of Persons, Annuities.
    2. Algebra, Analysis, Fluxions.
    3. Geometry.

II. Mechanical Philosophy
    1. Dynamics.
    2. Statics.
    3. Astronomy—Navigation.
    4. Projectiles.
    5. Mechanics.
    6. Hydrostatics.
    7. Hydraulics.
    8. Pneumatics.
    9. Acoustics.
    10. Optics.
    11. Magnetism.

III. Natural History
    1. Zoology.
    2. Botany.
    3. Mineralogy.
    4. Geography and Topography.
    5. Hydrology.

IV. Chemical Philosophy
    1. Chemistry.
    2. Meteorology.
    3. Geology.

V. Phisiology.
    1. Anatomy.
    2. Physiology of Animals.
    3. Physiology of Plants.
    4. Medicine.
    5. Surgery.

VI. The Arts.
    1. Mechanical.
    2. Chemical.
    3. The Fine Arts.
    4. Antiquities.

VII. Education, Literary Characters, Moral Philosophy

VIII. Bibliography; or, Account of Books.

IX. Biography; or, Account of Authors.

v.4 same broad classes, with these variations

I. [same]

II. Mechanical Philosophy
    1. Astronomy.
    2. Projectiles.
    3. Hydraulics.
    4. Pneumatics.
    5. Acoustics, Music.
    6. Optics.
    7. Magnetism.

III.-IX. [same]

v. 18 same broad classes as v.3, with revised subdivisions of the chapters thus

I. Mathematics
    1. Arithmetic, Annuities, Political Arithmetic. [*lacks Number of Persons*]
    2. Algebra, Analysis, Fluxions, Series.  [*Series added*]
    3. Geometry, Trigonometry, Surveying. [*Trigonometry and Surveying added*]

II. Mechanical Philosophy
    1. [*as above*]
    2. [*as above*]
    3. Astronomy, Chronolgy, Navigation. [*adds chronology*]
    4. Projectiles and Gunnery. [*adds Gunnery*]
    5. Hydraulics.
    6. Pneumatics.
    7. Optics.
    8. Electricity, Magnetism, Thermometry.

III. Natural History
    1. [*as above*]
    2. Mineralogy, Fossilogy, &c.
    3. Geography.

IV. Chemical Philosophy.
    1. [*as above*]
    2. Meteorology.
    3. Geology

V. Physiology.
    1. [*as above*]
    2. [*as above, with typo: ‘Animls’*]
    3. [*as above*]
    4. Medicine and Surgery. [*added Surgery*]

VI. The Arts.
    1. Mechanical Arts. [*as above with ‘Arts’*]
    3. Antiquities.

VII. Biography.

These headings seem to vary from volume to volume, adding sub topics
as the process goes along, but retaining the basic structure.  It
seems that there’s a master classification for the whole thing, but
that we only add the headings that pertain.  Some sub-headings will be
combined this way or that way depending on the number of articles.

## Oldenburg’s more natural method

From No. 22 (405–407)

I. A Natural History of all Countries and Places, is the foundation for solid Philosophy
    [places, rivers, weather, earth, insects, creatures, monsters, plants, medicine]

II. Singularities of Nature severely examin’d.

III. Arts, or Aids for the discovery or use of things Natural.
    [mathematics, painting, time, trades, transfusion, using plants]
